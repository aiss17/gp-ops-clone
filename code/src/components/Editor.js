import React, { Component, useEffect, useState } from "react";
import { GetCookie } from "src/services/Functions";

const Editor = ({ fileType, title, url, userData }) => {
  const [key, setKey] = useState("");
  useEffect(() => {
    console.log("File type, Title, dan url => ", title);
    editorConfig();
    setKey(getRandomString(15));

    return () => {};
  }, []);

  const getRandomString = (length) => {
    var randomChars =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var result = "";
    for (var i = 0; i < length; i++) {
      result += randomChars.charAt(
        Math.floor(Math.random() * randomChars.length)
      );
    }
    return result;
  };

  // constructor() {
  //     super();
  //     this.state = {
  //         key: sessionStorage.getItem("key"),
  //         url: sessionStorage.getItem("url"),
  //         callbackUrl: sessionStorage.getItem("callbackUrl"),
  //         mode: sessionStorage.getItem("mode"),
  //         title: sessionStorage.getItem("title"),
  //     };
  // }

  const editorConfig = () => {
    let config = {
      type: "desktop",
      documentType: "text",
      height: "100%",
      token:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.t-IDcSemACt8x4iTMCda8Yhe3iZaWbvV5XKSTbuAn0M",
      width: "100%",
      plugins: {
        autostart: ["asc.{da8a2138-868a-46a5-b9d0-1455a0c58d30}"],
        pluginsData: [
          "https://sertifikat.bki.co.id/plugin-ogs/config.json",
          "https://sertifikat.bki.co.id/plugin-rico/config.json"
        ]
      },
      document: {
        fileType: fileType,
        key: getRandomString(15),
        title: title,
        url: url,
        userdata: userData
        // url: "https://api.bki.co.id/api-sertifikat/upload_data/template_sertifikat/3196331629766871.docx",
      },
      editorConfig: {
        // mode: "edit",
        callbackUrl:
          `${process.env.REACT_APP_BASURL_TRX}project/report/save` +
          "?projectid=" +
          userData +
          "&key=" +
          GetCookie("key"),
        customization: {
          //   autosave: true,
          forcesave: true
          //   goback: {
          //     url: "https://eoffice.bki.co.id"
          //   }
        }
      }
    };
    console.log(JSON.stringify(config));
    new window.DocsAPI.DocEditor("placeholder", config);
  };

  return <div id="placeholder"></div>;
};

export default Editor;
