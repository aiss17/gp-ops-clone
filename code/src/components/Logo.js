import PropTypes from "prop-types";
import { Link as RouterLink } from "react-router-dom";
// material
import { Box } from "@mui/material";

// ----------------------------------------------------------------------

Logo.propTypes = {
  sx: PropTypes.object,
};

export default function Logo({ sx }) {
  return (
    <RouterLink to="app">
      <Box
        component="img"
        src="/static/GPOS_color.png"
        sx={{ width: 198, height: 60, ...sx }}
        style={{ textAlign: "center", marginLeft: 10, marginBottom: 5 }}
      />
    </RouterLink>
  );
}
