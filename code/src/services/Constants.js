import { GetApp } from "@mui/icons-material";

const ENDPOINT = {
  // Proflie
  GetCompanyProfile: "company/profile",
  GetCompanyByID: "company/profile/",
  UpdateCompanyProfile: "company/profile/update/",
  InsertCompany: "company/profile",

  // Document
  GetAllDocument: "documents/data",
  ViewOrDownload: "documents/fetch",
  MultiUploadDocument: "documents/multi",
  MetadataUpload: "documents/update",
  UploadRevise: "documents/revise",
  DeleteParrent: "documents/delete/parent",
  DeleteRevision: "documents/delete/revision",

  // Order
  GetAllOrder: "main/order",
  GetOrderID: "main/order/",
  UpdateOrder: "main/order/update/",

  // Project
  GetAllProject: "project/data",
  InsertProject: "project/data",
  UpdateProject: "project/data/update/",

  // Project Team
  GetAllTeams: "project/team",
  InsertTeams: "project/team",

  // Client
  GetAllClient: "project/client",
  DeleteClient: "project/client/delete",
  GetClientMyGp: "access/allclient",
  InsertClient: "access/register",

  // Project Visit
  GetAllVisit: "project/visit",
  InsertVisit: "project/visit",
  GetVisitByID: "project/visit/",
  DeleteVisit: "project/visit/delete/",

  // Assessment
  GetAllAssessment: "assessment/checklist",
  InsertComment: "assessment/comment",
  LinkFile: "assessment/checklist/link",
  Update: "assessment/checklist/update",

  // Masterdata Checklist
  GetAllCriteria: "assessment/criteria",
  GetAllSubCriteria: "assessment/subcriteria",
  UpdateCriteria: "assessment/criteria/update",
  UpdateSubCriteria: "assessment/subcriteria/update",
  GetAllMasterChecklist: "assessment/master",
  UpdateMaster: "assessment/master/update",

  // Technical
  GetAllTechnicalQuery: "project/query",
  ReplyTechnicalQuery: "project/query/update",

  // Meeting
  GetAllMeeting: "project/meeting",

  //GetAlluser
  GetAllusers: "access/alluser",
  DeluserIdproj: "project/team/delete",

  // Whiteboard
  GetAllWhiteboard: "project/whiteboard",

  //Utilities
  GetAllProvince: "utility/province",
  GetAllCountry: "utility/country",

  //Sync
  SyncData: "/main/sync",

  // Log Activity
  GetLogActivity: "project/log",

  // Get Assessment
  GetSelfAssessment: "assessment/self",
  GetCalculationCompany: "assessment/self/result/",
  GetSelfAssessmentCompany: "assessment/self/result",
  DeleteSelfAssessment: "assessment/self/delete",
  GetConsolidation: "assessment/consolidation",
  GetNCOFI: "assessment/nc",
  GenerateFileReport: "project/report",
  GetFileReport: "project/report/fetch",
  CallbackReport: "project/report/save",
  ReleaseNC: "assessment/releasenc",
  GenerateToPDF: "project/report/pdf",

  // Certificate
  GetAllCertificate: "main/certificate",
  InsertCertificate: "main/certificate",
  GetFileCertificate: "main/certificate/fetch"
};

export { ENDPOINT };
