import Api from "apisauce";
// console.log("monyettt", process.env.REACT_APP_NOT_SECRET_CODE);
const create = (baseuri) => {
  const api = Api.create({
    baseURL:
      baseuri == "auth"
        ? process.env.REACT_APP_BASEURL_AUTH
        : process.env.REACT_APP_BASURL_TRX,
    timeout: 30000
  });

  // inisialisasi
  const GET = api.get;
  const POST = api.post;
  const DELETE = api.delete;
  const PUT = api.put;

  const LOGIN = (data) => POST("login", data);
  const REFRESH = (data) => POST("refresh", data);

  const GETALL = () => GET("main/order");

  return {
    GET,
    POST,
    DELETE,
    PUT,

    LOGIN,
    REFRESH,
    GETALL
  };
};

export default { create };
