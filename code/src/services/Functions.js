import { Base64 } from "js-base64";
import sha512 from "js-sha512";
import Api from "./Api";
import Cookie from "js-cookie";

const SetCookie = (cookiename, value) => {
  // console.log("Ini key nya => ", value);
  Cookie.set(cookiename, value, {
    expires: 1,
    secure: true,
    sameSite: "strict",
    path: "/"
  });
};

const GetCookie = (cookiename) => {
  return Cookie.get(cookiename);
};

const RemoveCookie = (cookiename) => {
  return Cookie.remove(cookiename);
};

const callApi = async (uri, payload, type) => {
  let response;
  const keys = GetCookie("key");

  if (type === "POST") {
    const changePayload = {
      ...payload,
      key: keys
    };

    response = await Api.create("Api").POST(uri, changePayload, {
      headers: {
        "Content-Type": "application/json"
        // "Access-Control-Request-Headers": "*"
      }
    });
  } else if (type === "UPLOAD") {
    payload.set("key", keys);
    response = await Api.create("Api").POST(uri, payload, {
      headers: {
        "Content-Type": "multipart/form-data"
        // "Access-Control-Request-Headers": "*"
      }
    });
  } else {
    response = await Api.create("Api").GET(
      uri,
      {
        ...payload,
        key: keys
      },
      {
        headers: {
          "Content-Type": "application/json"
          // "Access-Control-Request-Headers": "*"
        }
      }
    );
  }

  // console.log("Check di Call API => ", JSON.stringify(response.data));
  if (response.status === 200 || response.status === "200") {
    //console.log("Berhasil get API");
    const finalResponse = response.data;
    //console.log("data normal", finalResponse);
    return { finalResponse, keys };
  } else if (response.data.status === "refresh") {
    //console.log("refresh get API");
    let sapi;
    await refreshTokenApi_v2(uri, payload, type);
    //console.log("kucing garong",  GetCookie("key"))

    sapi = callApi(uri, payload, type);
    //console.log("data dari call refresh", finalResponse);

    await sapi.then(function (result) {
      sapi = result.finalResponse;
      return { sapi, keys };
    });

    var finalResponse = sapi;
    return { finalResponse, keys };
  } else {
    //console.log("error nih!");
    const finalResponse = "error";
    //console.log("error nih! =>", response);
    console.log(response.data.message);
  }
};

const refreshTokenApi = async (uri, payload, type) => {
  const keys = GetCookie("key");
  var changePayload = {};

  var base64_key = Base64.encode(keys);
  var sha512_key = sha512(base64_key);

  var payloadToken = {
    key: keys,
    refresh: sha512_key
  };

  const response = await Api.create("auth").REFRESH(payloadToken);
  // console.log("Response dari refresh token => ", JSON.stringify(response.data));
  if (response.data.status == "success") {
    let responses;
    const newKey = response.data.key;
    SetCookie("key", response.data.key);

    if (type === "POST") {
      changePayload = {
        ...payload,
        key: newKey
      };

      // responses = await Api.create("Api").POST(uri, changePayload);
      responses = await Api.create("Api").POST(uri, changePayload, {
        headers: {
          "Content-Type": "application/json"
          // "Access-Control-Request-Headers": "*"
        }
      });
    } else if (type === "UPLOAD") {
      responses = await Api.create("Api").POST(uri, payload, {
        headers: {
          "Content-Type": "multipart/form-data"
          // "Access-Control-Request-Headers": "*"
        }
      });
    } else {
      // responses = await Api.create("Api", keys).GET(uri, { key: newKey });
      responses = await Api.create("Api").GET(
        uri,
        {
          ...payload,
          key: newKey
        },
        {
          headers: {
            "Content-Type": "application/json"
            // "Access-Control-Request-Headers": "*"
          }
        }
      );
    }

    if (responses.status == 200 || responses.status == "200") {
      console.log("Berhasil get API");
      const finalResponse = responses.data;

      return { finalResponse, keys: newKey };
    } else {
      const finalResponse = "error";
      return { finalResponse, keys: newKey };
    }
  } else if (response.data.status == "invalid") {
    const finalResponse = "error";
    return { finalResponse };
  } else {
    const finalResponse = "error";
    return { finalResponse };
  }
};

const refreshTokenApi_v2 = async (uri, payload, type) => {
  const keys = GetCookie("key");
  var changePayload = {};

  var base64_key = Base64.encode(keys);
  var sha512_key = sha512(base64_key);
  //console.log("Minta Refresh :",keys );
  var payloadToken = {
    key: keys,
    refresh: sha512_key
  };

  const response = await Api.create("auth").REFRESH(payloadToken);
  // console.log("Response dari refresh token => ", JSON.stringify(response.data));
  if (response.data.status == "success") {
    let responses;
    const newKey = response.data.key;
    SetCookie("key", response.data.key);
    //console.log("ini Key refresh :", newKey);
  } else if (response.data.status == "invalid") {
    const finalResponse = "error";
    return { finalResponse };
  } else {
    const finalResponse = "error";
    return { finalResponse };
  }
};

export { refreshTokenApi, SetCookie, GetCookie, RemoveCookie, callApi };
