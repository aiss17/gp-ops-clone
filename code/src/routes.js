import { Navigate, useRoutes } from "react-router-dom";
// layouts
import DashboardLayout from "./layouts/dashboard";
import LogoOnlyLayout from "./layouts/LogoOnlyLayout";
//
import Login from "./pages/Login";
import Register from "./pages/Register";
import Products from "./pages/Products";
import Blog from "./pages/Blog";
import NotFound from "./pages/Page404";
import {
  Dashboard,
  Order,
  ProjectList,
  Assessment,
  Document,
  Meeting,
  TechnicalEnquiry,
  SetupTeam,
  SetupPlan,
  ReviewApprove,
  RemoteSurvey,
  Checklist,
  Customer,
  CustomerDetails,
  TabProject,
  DetailSurvey,
  Overview,
  Report,
  VisitDetails,
  Client,
  ClientDetail,
  AuthLoading,
  Certificate,
  SelfAssessmentManagement,
  SelfAssessmentDetail,
  PreviewCertificate,
} from "./pages";
import { useEffect, useState } from "react";
import Api from "./services/Api";
import { ENDPOINT } from "./services/Constants";
import { GetCookie } from "./services/Functions";

// ----------------------------------------------------------------------

export default function Router() {
  const [isLogin, setIsLogin] = useState(false);
  const keys = GetCookie("key");

  useEffect(() => {
    checkLogin();

    return () => {};
  }, []);

  const checkLogin = async () => {
    await Api.create("trx")
      .GET(
        ENDPOINT.GetAllOrder,
        { key: keys },
        {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Request-Headers": "*",
          },
        }
      )
      .then((res) => {
        // console.log("Test => ", JSON.stringify(res.data));

        if (res.data.status !== "refresh") {
          // console.log("haha");
          setIsLogin(true);
        } else {
          // console.log("haha");
          setIsLogin(true);
        }
      });
  };

  return useRoutes([
    {
      path: "/gp-ops",
      element: <DashboardLayout />,
      children: [
        { path: "app", element: <Dashboard /> },
        { path: "order", element: <Order /> },
        { path: "selfAssessment", element: <SelfAssessmentManagement /> },
        {
          path: "selfAssessment/selfassessmentdetail/:id",
          element: <SelfAssessmentDetail />,
        },
        // project
        { path: "project", element: <ProjectList /> },
        { path: "project/tabproject/:id", element: <TabProject /> },
        {
          path: "project/tabproject/:id/visitdetails/:id_visit",
          element: <VisitDetails />,
        },

        // Project
        // { path: "project/setup-team", element: <SetupTeam /> },
        // { path: "project/setup-plan", element: <SetupPlan /> },
        // { path: "project/document-management", element: <Document /> },
        // { path: "project/meeting-management", element: <Meeting /> },
        // { path: "project/technical-enquiry", element: <TechnicalEnquiry /> },
        // { path: "project/assessment-workspace", element: <Assessment /> },
        // { path: "project/review-approve", element: <ReviewApprove /> },
        // { path: "project/remote-survey", element: <RemoteSurvey /> },
        // Masterdata
        { path: "masterdata/checklist", element: <Checklist /> },
        { path: "masterdata/customer", element: <Customer /> },
        {
          path: "masterdata/customer/customerdetails/:id",
          element: <CustomerDetails />,
        },
        { path: "masterdata/client", element: <Client /> },
        {
          path: "masterdata/client/clientdetail/:id",
          element: <ClientDetail />,
        },
        { path: "certificate", element: <Certificate /> },
        { path: "view", element: <PreviewCertificate /> }
      ],
    },
    {
      path: "/",
      element: <LogoOnlyLayout />,
      children: [
        { path: "/", element: <AuthLoading /> },
        { path: "login", element: <Login /> },
        { path: "register", element: <Register /> },
        { path: "404", element: <NotFound /> },
        { path: "*", element: <Navigate to="/404" /> },
      ],
    },
    { path: "*", element: <Navigate to="/404" replace /> },
  ]);
}
