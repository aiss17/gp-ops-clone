import BKI from "./IDSurvey_Putih.png";
import PDF from "./doc.png";
import EXCEL from "./excel.png";
import WORD from "./word.png";
import PPT from "./ppt.png";
import JPEG from "./jpeg.png";
import JPG from "./jpg.png";
import PNG from "./png.png";
import BMP from "./bmp.png";
import underconstruction from "./under_construction.png";
import HISTORY from "./history.png";

export {
  BKI,
  PDF,
  EXCEL,
  PPT,
  WORD,
  BMP,
  JPEG,
  JPG,
  PNG,
  underconstruction,
  HISTORY
};
