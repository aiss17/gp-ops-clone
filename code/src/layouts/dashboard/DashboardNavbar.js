import PropTypes from "prop-types";
// material
import { alpha, styled } from "@mui/material/styles";
import {
  Box,
  Stack,
  AppBar,
  Toolbar,
  IconButton,
  Badge,
  Button,
} from "@mui/material";
// components
import Iconify from "../../components/Iconify";
//
// import Searchbar from "./Searchbar";
import AccountPopover from "./AccountPopover";
import LanguagePopover from "./LanguagePopover";
import NotificationsPopover from "./NotificationsPopover";
// api
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { useState } from "react";

// ----------------------------------------------------------------------

const DRAWER_WIDTH = 280;
const APPBAR_MOBILE = 64;
const APPBAR_DESKTOP = 65;

const RootStyle = styled(AppBar)(({ theme }) => ({
  boxShadow: "none",
  backdropFilter: "blur(6px)",
  WebkitBackdropFilter: "blur(6px)", // Fix on Mobile
  backgroundColor: alpha(theme.palette.background.default, 0.72),
  [theme.breakpoints.up("lg")]: {
    width: `calc(100% - ${DRAWER_WIDTH + 1}px)`,
  },
}));

const ToolbarStyle = styled(Toolbar)(({ theme }) => ({
  minHeight: APPBAR_MOBILE,
  [theme.breakpoints.up("lg")]: {
    minHeight: APPBAR_DESKTOP,
    padding: theme.spacing(0, 5),
  },
}));

// ----------------------------------------------------------------------

DashboardNavbar.propTypes = {
  onOpenSidebar: PropTypes.func,
};

export default function DashboardNavbar({ onOpenSidebar }) {
  // default set variabel input
  const [key, setKey] = useState("");

  const SyncData = async () => {
    setKey(GetCookie("key"));
    const response = await callApi(ENDPOINT.SyncData, null, "POST");

    window.location.reload();
    return false;
  };

  return (
    <RootStyle>
      {/* <ToolbarStyle style={{ backgroundColor: "#F0FFFA" }}> */}
      <ToolbarStyle style={{ backgroundColor: "#E8F3FF" }} sx={{ py: 0 }}>
        <IconButton
          onClick={onOpenSidebar}
          sx={{ mr: 1, color: "text.primary", display: { lg: "none" } }}
        >
          <Iconify icon="eva:menu-2-fill" />
        </IconButton>

        {/* <Searchbar /> */}
        <Box sx={{ flexGrow: 1 }} />

        <Stack direction="row" alignItems="center">
          {/* <LanguagePopover />
          <NotificationsPopover /> */}
          <Button
            onClick={function (event) {
              SyncData();
            }}
            size="medium"
            style={{
              backgroundColor: "#254A7C",
              color: "#ffffff",
              marginRight: 10,
              paddingRight: 20,
              paddingLeft: 20,
              borderRadius: 50,
            }}
            startIcon={<Iconify icon="fluent:arrow-sync-circle-24-filled" />}
          >
            Sync Data
          </Button>
          <AccountPopover />
        </Stack>
      </ToolbarStyle>
    </RootStyle>
  );
}
