// component
import Iconify from "../../components/Iconify";

// ----------------------------------------------------------------------

const getIcon = (name) => <Iconify icon={name} width={22} height={22} />;

const sidebarConfigAdmin = [
  {
    title: "dashboard project",
    path: "/gp-ops/app",
    icon: getIcon("ant-design:dashboard-outlined"),
  },
  {
    title: "management order",
    path: "/gp-ops/order",
    icon: getIcon("ant-design:form-outlined"),
  },
  {
    title: "self assessment",
    path: "/gp-ops/selfAssessment",
    icon: getIcon("akar-icons:person-check"),
  },
  {
    title: "project",
    path: "/gp-ops/project",
    icon: getIcon("eos-icons:project-outlined"),
  },
  {
    title: "masterdata",
    path: "/gp-ops/masterdata",
    icon: getIcon("carbon:data-base"),
    children: [
      {
        title: "Customer",
        path: "/gp-ops/masterdata/customer",
        icon: getIcon("eos-icons:project-outlined"),
        children: [],
      },
      {
        title: "Client",
        path: "/gp-ops/masterdata/client",
        icon: getIcon("eos-icons:project-outlined"),
        children: [],
      },
      {
        title: "Checklist",
        path: "/gp-ops/masterdata/checklist",
        icon: getIcon("eos-icons:project-outlined"),
        children: [],
      },
    ],
  },
  {
    title: "certificate",
    path: "/gp-ops/certificate",
    icon: getIcon("teenyicons:certificate-outline"),
  },
  // {
  //   title: 'login',
  //   path: '/login',
  //   icon: getIcon('eva:lock-fill')
  // },
  // {
  //   title: 'register',
  //   path: '/register',
  //   icon: getIcon('eva:person-add-fill')
  // },
  // {
  //   title: 'Not found',
  //   path: '/404',
  //   icon: getIcon('eva:alert-triangle-fill')
  // }
];

const sidebarConfig = [
  {
    title: "dashboard project",
    path: "/gp-ops/app",
    icon: getIcon("ant-design:dashboard-outlined"),
  },
  {
    title: "management order",
    path: "/gp-ops/order",
    icon: getIcon("ant-design:form-outlined"),
  },
  {
    title: "self assessment",
    path: "/gp-ops/selfAssessment",
    icon: getIcon("akar-icons:person-check"),
  },
  {
    title: "project",
    path: "/gp-ops/project",
    icon: getIcon("eos-icons:project-outlined"),
  },
  // {
  //   title: "masterdata",
  //   path: "/gp-ops/masterdata",
  //   icon: getIcon("carbon:data-base"),
  //   children: [
  //     {
  //       title: "Customer",
  //       path: "/gp-ops/masterdata/customer",
  //       icon: getIcon("eos-icons:project-outlined"),
  //       children: [],
  //     },
  //     {
  //       title: "Client",
  //       path: "/gp-ops/masterdata/client",
  //       icon: getIcon("eos-icons:project-outlined"),
  //       children: [],
  //     },
  //     {
  //       title: "Checklist",
  //       path: "/gp-ops/masterdata/checklist",
  //       icon: getIcon("eos-icons:project-outlined"),
  //       children: [],
  //     },
  //   ],
  // },
  {
    title: "certificate",
    path: "/gp-ops/certificate",
    icon: getIcon("teenyicons:certificate-outline"),
  },
  // {
  //   title: 'login',
  //   path: '/login',
  //   icon: getIcon('eva:lock-fill')
  // },
  // {
  //   title: 'register',
  //   path: '/register',
  //   icon: getIcon('eva:person-add-fill')
  // },
  // {
  //   title: 'Not found',
  //   path: '/404',
  //   icon: getIcon('eva:alert-triangle-fill')
  // }
];

export { sidebarConfig, sidebarConfigAdmin };
