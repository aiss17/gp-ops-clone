import { faker } from "@faker-js/faker";
import { sample } from "lodash";
// utils
import { mockImgAvatar } from "../utils/mockImages";

// ----------------------------------------------------------------------

const users = [...Array(50)].map((_, index) => ({
  id: faker.address.zipCode(),
  avatarUrl: mockImgAvatar(index + 1),
  name: faker.name.findName(),
  company: faker.company.companyName(),
  isVerified: faker.datatype.boolean(),
  status: sample(["active", "banned"]),
  role: sample([
    "Leader",
    "Hr Manager",
    "UI Designer",
    "UX Designer",
    "UI/UX Designer",
    "Project Manager",
    "Backend Developer",
    "Full Stack Designer",
    "Front End Developer",
    "Full Stack Developer"
  ])
}));

const ORDERS = [...Array(50)].map((_, index) => ({
  id: faker.datatype.uuid(),
  zipCode: faker.address.zipCode(),
  orderName: sample(["Assessment", "Surveillance", "Consultation"]),
  progress: sample(["Review", "Administrative", "Complain"]),
  startDate: faker.date.past(),
  endDate: faker.date.past(),
  email: faker.internet.email(),
  status: sample(["In Progress", "Awaiting", "Approve"])
}));

const DOCUMENTS = [...Array(50)].map((_, index) => ({
  id: faker.datatype.uuid(),
  name: faker.commerce.product(),
  type: sample(["A", "B", "C"]),
  category: sample(["A", "B", "C"]),
  startDate: faker.date.past()
}));

const MEETINGS = [...Array(50)].map((_, index) => ({
  id: faker.datatype.uuid(),
  name: faker.commerce.product(),
  note: "Lorem ipsum hahah",
  startDate: faker.date.past()
}));

const REPORTS = [...Array(50)].map((_, index) => ({
  id: faker.datatype.uuid(),
  name: faker.commerce.product(),
  file: sample([true, false]),
  startDate: faker.date.past()
}));

const TQ = [...Array(50)].map((_, index) => ({
  id: faker.datatype.uuid(),
  subject: sample([
    "Penuntasan Assessment",
    "Reschedule Permintaan Surveillance",
    "Persetujuan"
  ]),
  question: faker.commerce.product(),
  file: sample([true, false]),
  reply: sample([
    "",
    "Tidak diizinkan",
    "Silahkan dilanjutkan surveillance nya"
  ]),
  startDate: faker.date.past()
}));

const ASSESSMENTWORKSPACE = [...Array(50)].map((_, index) => ({
  id: faker.datatype.uuid(),
  requirement: sample([
    "Port di pelabuhan sebanyak 5 port",
    "Luas setiap portnya 50 M",
    "Terdapat penjual kaki lima disekitar port",
    "Terdapat security disekitar port"
  ]),
  point: sample(["", 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]),
  compliance: sample(["", "Yes", "No"]),
  startDate: faker.date.past()
}));

const DOCUMENTUPLOAD = [...Array(10)].map((_, index) => ({
  // name: sample(["SKCK PERUSAHAAN", "NPWP PERUSAHAAN", "SIUP PERUSAHAAN", "SKDP PERUSAHAAN", "TDP PERUSAHAAN", "AKTA PERUSAHAAN"]),
  // type: sample(["SKCK", "NPWP", "SIUP", "SKDP", "TDP", "AKTA"]),
  id: faker.datatype.uuid(),
  documentName: sample([
    "SKCK PERUSAHAAN",
    "NPWP PERUSAHAAN",
    "SIUP PERUSAHAAN",
    "SKDP PERUSAHAAN",
    "TDP PERUSAHAAN",
    "AKTA PERUSAHAAN"
  ]),
  type: sample(["SKCK", "NPWP", "SIUP", "SKDP", "TDP", "AKTA"])
}));

export {
  users,
  ORDERS,
  DOCUMENTS,
  MEETINGS,
  REPORTS,
  TQ,
  ASSESSMENTWORKSPACE,
  DOCUMENTUPLOAD
};
