import * as Yup from "yup";
import { useEffect, useState } from "react";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { useFormik, Form, FormikProvider } from "formik";
// material
import {
  Link,
  Stack,
  Checkbox,
  TextField,
  IconButton,
  InputAdornment,
  FormControlLabel,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
// component
import Iconify from "../../../components/Iconify";
// service
import Api from "../../../services/Api";
import { sha512 } from "js-sha512";
import { SetCookie } from "src/services/Functions";

// ----------------------------------------------------------------------

export default function LoginForm() {
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);
  const [buttonLogin, setbuttonLogin] = useState(false);

  const LoginSchema = Yup.object().shape({
    email: Yup.string().required("Email is required"),
    password: Yup.string().required("Password is required"),
  });

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
      remember: true,
      // isSubmitting: false
    },
    validationSchema: LoginSchema,
    onSubmit: () => {
      setbuttonLogin(true);
      const pass = sha512(values.password);

      Api.create("auth")
        .LOGIN({ email: values.email, sandi: pass })
        .then((response) => {
          console.log(response.data);
          if (response.data.status === "success") {
            SetCookie("key", response.data.key);
            SetCookie("id_user", response.data.id);
            SetCookie("name", response.data.nama);
            SetCookie("email", response.data.email);
            navigate("/gp-ops/app", { replace: true });
            // isSubmitting = false;
          } else if (response.data.status === "mismatch") {
            alert(response.data.message);
            setbuttonLogin(false);
            // isSubmitting = false;
          } else if (response.data.status === "locked") {
            alert(
              "You have input the wrong password 5 times, " +
                response.data.message +
                "ed!"
            );
            setbuttonLogin(false);
          } else if (response.data.status === "inactive") {
            alert(response.data.message);
            setbuttonLogin(false);
          }
        });
    },
  });

  const { errors, touched, values, isSubmitting, handleSubmit, getFieldProps } =
    formik;

  const handleShowPassword = () => {
    setShowPassword((show) => !show);
  };

  useEffect(() => {
    return () => {};
  }, []);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Stack spacing={3}>
          <TextField
            fullWidth
            autoComplete="username"
            type="text"
            label="Email address"
            {...getFieldProps("email")}
            error={Boolean(touched.email && errors.email)}
            helperText={touched.email && errors.email}
            value={formik.values.email}
          />

          <TextField
            fullWidth
            autoComplete="current-password"
            type={showPassword ? "text" : "password"}
            label="Password"
            {...getFieldProps("password")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton onClick={handleShowPassword} edge="end">
                    <Iconify
                      icon={showPassword ? "eva:eye-fill" : "eva:eye-off-fill"}
                    />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            error={Boolean(touched.password && errors.password)}
            helperText={touched.password && errors.password}
            value={formik.values.password}
          />
        </Stack>

        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          sx={{ my: 2 }}
        >
          <FormControlLabel
            control={
              <Checkbox
                {...getFieldProps("remember")}
                checked={values.remember}
                style={{ color: "#164477" }}
              />
            }
            label="Remember me"
            style={{ color: "#164477" }}
          />

          <Link
            component={RouterLink}
            variant="subtitle2"
            to="#"
            underline="hover"
            style={{ color: "#164477" }}
          >
            Forgot password?
          </Link>
        </Stack>

        <LoadingButton
          fullWidth
          size="large"
          type="submit"
          variant="contained"
          loading={buttonLogin}
          style={{ backgroundColor: "#164477", boxShadow: "none" }}
        >
          Login
        </LoadingButton>
      </Form>
    </FormikProvider>
  );
}
