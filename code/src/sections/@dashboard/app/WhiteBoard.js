import { faker } from "@faker-js/faker";
import PropTypes from "prop-types";
import { formatDistance } from "date-fns";
import { Link as RouterLink } from "react-router-dom";
// material
import {
  Box,
  Stack,
  Link,
  Card,
  Button,
  Divider,
  Typography,
  CardHeader,
  TextField,
  Grid
} from "@mui/material";
// utils
import { mockImgCover } from "../../../utils/mockImages";
//
import Scrollbar from "../../../components/Scrollbar";
import Iconify from "../../../components/Iconify";
import { useEffect, useState } from "react";
import { callApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";

// ----------------------------------------------------------------------

const NEWS = [...Array(5)].map((_, index) => {
  const setIndex = index + 1;
  return {
    title: faker.name.title(),
    description: faker.lorem.paragraphs(),
    image: mockImgCover(setIndex),
    postedAt: faker.date.soon()
  };
});

// ----------------------------------------------------------------------

NewsItem.propTypes = {
  news: PropTypes.object.isRequired
};

function NewsItem({ news }) {
  const { message, created_at } = news;

  return (
    <Stack direction="row" alignItems="center" spacing={2}>
      <Box sx={{ minWidth: 240 }}>
        <Typography variant="body2">{message}</Typography>
        <Typography variant="caption" sx={{ color: "text.secondary" }} noWrap>
          {formatDistance(new Date(news.created_at), new Date())}
        </Typography>
      </Box>
    </Stack>
  );
}

export default function WhiteBoard() {
  const [data, setData] = useState([]);
  const [message, setMessage] = useState("");

  useEffect(() => {
    GetAllWhiteboard();

    return () => {};
  }, []);

  const GetAllWhiteboard = async () => {
    const response = await callApi(ENDPOINT.GetAllWhiteboard, null, "GET");
    // setKey(GetCookie("key"));
    //console.log("dapat data order",response.finalResponse.data);
    setData(response.finalResponse.data);
  };

  const CommentChat = async () => {
    const payload = {
      message: message
    };

    const response = await callApi(ENDPOINT.GetAllWhiteboard, payload, "POST");
    // console.log("Haha => ", response.finalResponse);

    setMessage("");
    GetAllWhiteboard();
  };

  return (
    <Card>
      <CardHeader title="White Board" />

      <Scrollbar style={{ height: "350px", paddingRight: 15 }}>
        <Stack spacing={3} sx={{ p: 3, pr: 0 }}>
          {data !== [] &&
            data.map((news, index) => <NewsItem key={index} news={news} />)}
        </Stack>
      </Scrollbar>

      <Divider />

      <Box sx={{ p: 2, textAlign: "right" }}>
        <Grid container spacing={3}>
          <Grid item xs={10}>
            <TextField
              margin="normal"
              id="name"
              // label="Contract No"
              type="text"
              fullWidth
              variant="outlined"
              size="small"
              style={{ margin: 0 }}
              value={message}
              onChange={(e) => {
                setMessage(e.target.value);
              }}
            />
          </Grid>
          <Grid item xs={2}>
            <Button
              to="#"
              size="medium"
              color="inherit"
              component={RouterLink}
              variant="contained"
              style={{
                backgroundColor: "#248bf5",
                color: "#ffffff",
                marginLeft: "-15px",
                padding: "10px 8px 8px 10px"
              }}
              endIcon={<Iconify icon="eva:arrow-ios-forward-fill" />}
              onClick={() => CommentChat()}
            >
              Send
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
}
