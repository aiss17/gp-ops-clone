import React, { Component } from "react";

import { merge } from "lodash";
import GoogleMapReact from "google-map-react";
// material
import { Card, CardHeader, Box } from "@mui/material";
//
import { BaseOptionChart } from "../../../components/charts";

// import { MapContainer } from "react-leaflet/MapContainer";
// import { TileLayer } from "react-leaflet/TileLayer";
// import { useMap } from "react-leaflet/hooks";
import { MapContainer, TileLayer, useMap, Marker, Popup } from "react-leaflet";
// import "../../../theme/leaflet.css";

// ----------------------------------------------------------------------

const handleApiLoaded = (map, maps) => {
  // use map and maps objects
};

export default function IndonesiaMap() {
  return (
    <Card>
      <CardHeader title="Location" />
      <Box sx={{ p: 3, pb: 1 }} dir="ltr">
        {/* <GoogleMapReact
          bootstrapURLKeys={{ key: YOUR KEY HERE }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
        >
          <Component lat={59.955413} lng={30.337844} text="My Marker" />
        </GoogleMapReact> */}
        <MapContainer
          center={[-0.856852602015434, 117.830547058422]}
          zoom={5}
          scrollWheelZoom={false}
          style={{ height: 500 }}
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={[-6.126094956960548, 106.8923011406927]}>
            <Popup>ID Survey Head Office</Popup>
          </Marker>
        </MapContainer>
      </Box>
    </Card>
  );
}
