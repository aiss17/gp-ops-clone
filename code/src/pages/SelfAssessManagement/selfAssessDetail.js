import { forwardRef, useEffect, useRef, useState } from "react";
// import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import {
  Card,
  Stack,
  Typography,
  Grid,
  TextField,
  Button,
  Table,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  IconButton,
  Divider,
  Paper,
  TableHead,
  FormControl,
  Select
} from "@mui/material";
// pages
import { Assessment, Report, TechnicalEnquiry } from "../index";

// pages

import { callApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { useLocation, useParams } from "react-router-dom";

export default function BasicTabs() {
  const [data, setData] = useState([]);

  const [scores, setScores] = useState(null);
  const [done, setDone] = useState(null);
  const [total, setTotal] = useState(null);

  const params = useParams();
  const { state } = useLocation();

  useEffect(() => {
    console.log(params);
    GetSelfAssessment();

    return () => {};
  }, []);

  const GetSelfAssessment = async () => {
    const payload = {
      company: params.id
    };

    const response = await callApi(ENDPOINT.GetSelfAssessment, payload, "GET");

    console.log("response self assessment => ", response.finalResponse.data);
    setData(response.finalResponse.data);
    GetCalculationCompany();
  };

  const GetCalculationCompany = async () => {
    const response = await callApi(
      ENDPOINT.GetCalculationCompany + params.id,
      null,
      "GET"
    );

    console.log("response Calculation => ", response.finalResponse);
    setScores(response.finalResponse.data.score);
    setDone(response.finalResponse.data.done);
    setTotal(response.finalResponse.data.total);
  };

  return (
    <Box style={{ clear: "both", marginLeft: 20, marginRight: 20 }}>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Card style={{ marginLeft: 25, marginBottom: 20, padding: 20 }}>
            <Typography
              variant="h5"
              gutterBottom
              style={{ textAlign: "center" }}
            >
              NILAI SEMENTARA
            </Typography>
            <Typography
              variant="h2"
              gutterBottom
              style={{ textAlign: "center", color: "#00AF5B" }}
            >
              {scores}
            </Typography>
          </Card>
        </Grid>
        <Grid item xs={6}>
          <Card style={{ marginRight: 25, marginBottom: 20, padding: 20 }}>
            <Typography
              variant="h5"
              gutterBottom
              style={{ textAlign: "center" }}
            >
              JUMLAH PENGISIAN SUB KRITERIA
            </Typography>
            <Typography
              variant="h2"
              gutterBottom
              style={{ textAlign: "center", color: "#00AF5B" }}
            >
              {`${done} / ${total}`}
            </Typography>
          </Card>
        </Grid>
      </Grid>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={1}
      >
        <Typography variant="h4" gutterBottom>
          Self Assessment {state.companyName}
        </Typography>
      </Stack>

      {/* <hr /> */}

      <Card>
        <TableContainer component={Paper} style={{ borderRadius: 0 }}>
          <Table aria-label="collapsible table">
            <TableHead style={{ backgroundColor: "#164477" }}>
              <TableRow>
                {/* <TableCell /> */}
                <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                  Kode Kriteria
                </TableCell>
                <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                  Sub Kriteria
                </TableCell>
                <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                  Porsi
                </TableCell>
                <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                  Tolak Ukur
                </TableCell>
                <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                  Pemenuhan
                </TableCell>
                <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                  Nilai
                </TableCell>
                <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                  Persentase
                </TableCell>
                {/* <TableCell /> */}
              </TableRow>
            </TableHead>
            <TableBody>
              {/* {rows.map((row) => (
              <Row key={row.name} row={row} />
            ))} */}
              {data.map((row, index) => {
                return (
                  <TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
                    <TableCell component="th" scope="row" align="center">
                      {row.code}
                    </TableCell>
                    <TableCell>{row.sub_criteria}</TableCell>
                    <TableCell style={{ textAlign: "center" }}>
                      {row.portion}
                    </TableCell>
                    <TableCell>{row.master_name}</TableCell>
                    <TableCell style={{ textAlign: "center" }}>
                      {row.compliance === null ? "-" : row.compliance}
                    </TableCell>
                    <TableCell style={{ textAlign: "center" }}>
                      {row.value === null ? "-" : row.value}
                    </TableCell>
                    <TableCell style={{ textAlign: "center" }}>
                      {`${Number(row.percentage).toFixed(2)}%`}
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Card>
    </Box>
  );
}
