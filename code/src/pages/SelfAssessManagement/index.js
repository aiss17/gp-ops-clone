import { filter } from "lodash";
import { useEffect, useState } from "react";
import { Link as RouterLink, useNavigate } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  TableRow,
  TableBody,
  Typography,
  TableContainer,
  TablePagination
} from "@mui/material";
import moment from "moment";

import { styled } from "@mui/material/styles";
// components
import Page from "../../components/Page";
import Scrollbar from "../../components/Scrollbar";
import Iconify from "../../components/Iconify";
import SearchNotFound from "../../components/SearchNotFound";
import { UserListToolbar } from "../../sections/@dashboard/user";
// table
import TableHead from "@mui/material/TableHead";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
// api
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { LoadingData } from "..";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff"
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.company.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Order() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("company");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [loading, setLoading] = useState(false);

  // default set variabel input
  const [key, setKey] = useState("");
  const [data, setData] = useState([]);
  const [id, setID] = useState(null);
  const navigate = useNavigate();

  // const [val,setVal]=useState({})

  useEffect(() => {
    // console.log(data);
    GetSelfAssessmentCompany();
    return () => {};
  }, []);

  const GetSelfAssessmentCompany = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetSelfAssessmentCompany,
      null,
      "GET"
    );
    setKey(GetCookie("key"));
    setData(response.finalResponse.data);
    setLoading(false);
  };

  const Deleteparent = async (id) => {
    const payload = {
      company: id
    };

    const response = await callApi(
      ENDPOINT.DeleteSelfAssessment,
      payload,
      "POST"
    );

    GetSelfAssessmentCompany();
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  return (
    <Page title="Self Assessment | Green Port Ops">
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Self Assesment Management
        </Typography>
      </Stack>

      <Card>
        <UserListToolbar
          numSelected={selected.length}
          filterName={filterName}
          onFilterName={handleFilterByName}
        />

        <Scrollbar>
          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <StyledTableCell>Company ID</StyledTableCell>
                  <StyledTableCell>Company</StyledTableCell>
                  <StyledTableCell>Current Score</StyledTableCell>
                  <StyledTableCell>Fulfillment</StyledTableCell>
                  <StyledTableCell style={{ textAlign: "center" }}>
                    Action
                  </StyledTableCell>
                </TableRow>
              </TableHead>
              {loading ? (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                      <LoadingData />
                    </TableCell>
                  </TableRow>
                </TableBody>
              ) : (
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      const { company_id, company, score, done, total } = row;

                      return (
                        <TableRow>
                          <TableCell align="left">{company_id}</TableCell>
                          <TableCell align="left">{company}</TableCell>
                          <TableCell align="left">{score}</TableCell>
                          <TableCell align="left">{`${done} / ${total}`}</TableCell>
                          <TableCell align="center">
                            {GetCookie("name") === "Admin Ops" && (
                              <Button
                                variant="contained"
                                // component={RouterLink}
                                // to={"#"}
                                startIcon={<Iconify icon="bx:detail" />}
                                style={{
                                  backgroundColor: "red",
                                  boxShadow: "none"
                                  // "&:hover": { color: "red" },
                                }}
                                onClick={() => {
                                  if (
                                    window.confirm(
                                      "Are you sure you want to reset ?"
                                    )
                                  ) {
                                    Deleteparent(company_id);
                                    console.log("success");
                                  } else {
                                    // cancel
                                    console.log("cancel");
                                  }
                                }}
                              >
                                Reset
                              </Button>
                            )}
                            &nbsp;
                            <Button
                              variant="contained"
                              component={RouterLink}
                              to={
                                window.location.pathname +
                                `/selfassessmentdetail/${company_id}`
                              }
                              state={{ companyName: company }}
                              startIcon={<Iconify icon="bx:detail" />}
                              style={{
                                backgroundColor: "#254A7C",
                                boxShadow: "none"
                                // "&:hover": { color: "red" },
                              }}
                              // onClick={() => {
                              //   navigate(
                              //     window.location.pathname +
                              //       `/selfassessmentdetail/${company_id}`,
                              //     { state: { companyName: company } }
                              //   );
                              // }}
                            >
                              View Detail
                            </Button>
                          </TableCell>
                        </TableRow>
                      );
                    })}

                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}

                  {isUserNotFound && (
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  )}
                </TableBody>
              )}
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Page>
  );
}
