// material
import { Box, Grid, Container, Typography } from "@mui/material";
import IndonesiaMap from "src/sections/@dashboard/app/IndonesiaMap";
// components
import Page from "../../components/Page";
import {
  AppTasks,
  CloseOrder,
  TotalClient,
  TotalProject,
  AppNewsUpdate,
  TechnicalEnq,
  OnReviewOrder,
  AppOrderTimeline,
  AppCurrentVisits,
  AppWebsiteVisits,
  AppTrafficBySite,
  ServiceType,
  AppConversionRates,
  VisitGraph_v2,
  WhiteBoard,
  PokjaTeam,
} from "../../sections/@dashboard/app";

// ----------------------------------------------------------------------

export default function DashboardApp() {
  return (
    <Page title="Dashboard | Green Port Ops">
      <Box sx={{ pb: 2 }}>
        <Typography variant="h4">Hi, Welcome back</Typography>
      </Box>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6} md={3}>
          <OnReviewOrder />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <CloseOrder />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <TotalProject />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <TotalClient />
        </Grid>

        <Grid item xs={12} md={6} lg={12}>
          <IndonesiaMap />
          {/* <AppWebsiteVisits /> */}
        </Grid>

        <Grid item xs={12} md={6} lg={7}>
          <VisitGraph_v2 />
          {/* <AppCurrentVisits /> */}
        </Grid>
        <Grid item xs={12} md={6} lg={5}>
          <ServiceType />
        </Grid>

        <Grid item xs={12} md={6} lg={4}>
          <PokjaTeam />
          {/* <AppConversionRates /> */}
        </Grid>

        <Grid item xs={12} md={6} lg={8}>
          {/* <WhiteBoard /> */}
          <WhiteBoard />
        </Grid>
      </Grid>
    </Page>
  );
}
