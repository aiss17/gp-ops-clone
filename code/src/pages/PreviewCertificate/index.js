import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  // TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid
} from "@mui/material";
import Page from "src/components/Page";
import PDFViewer from "pdf-viewer-reactjs";
import { callApi, GetCookie } from "src/services/Functions";
import {
  useLocation,
  useParams,
  Link as RouterLink,
  useNavigate
} from "react-router-dom";
import { ENDPOINT } from "src/services/Constants";
import { LoadingData } from "src/pages";
import { useEffect, useState } from "react";
import PDF from "../../assets/text.pdf";
import Iconify from "../../components/Iconify";

export default function UC() {
  const [urlReport, setUrlReport] = useState(null);
  const [visibleReport, setVisibleReport] = useState(false);
  const [loading, setLoading] = useState(false);
  const { id } = useParams();

  const { state } = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    console.log(state.id);
    GetCertifById();

    return () => {};
  }, []);

  const GetCertifById = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetAllCertificate + `/${state.id}`,
      null,
      "GET"
    );

    console.log(
      "Check response get docs => ",
      response.finalResponse.data[0].docs
    );
    if (response.finalResponse.data[0].docs.length > 0) {
      GetFileReport(response.finalResponse.data[0].docs[0].path);
    } else {
      setLoading(false);
    }
  };

  const GetFileReport = async (path) => {
    console.log("berhasil nih => ", path);
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetFileCertificate,
      { path, act: "view" },
      "GET"
    );

    console.log("Check response get file => ", response.finalResponse);
    if (response.finalResponse !== null) {
      if (response.finalResponse.status !== "empty") {
        console.log("berhasil nih");
        setUrlReport(
          `${
            process.env.REACT_APP_BASURL_TRX
          }main/certificate/fetch?path=${path}&key=${GetCookie("key")}&act=view`
        );

        setVisibleReport(false);

        setTimeout(() => {
          setLoading(false);
          setVisibleReport(true);
        }, 500);
      } else {
        console.log("gagal nih");
        setLoading(false);
        setVisibleReport(false);
      }
    } else {
      setLoading(false);
    }
  };

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Page title="Project Management | My Green Port">
      <Card
        style={{
          padding: 15,
          // maxHeight: "55%",
          height: "100%",
          overflow: "auto",
          textAlign: "center",
          boxShadow: "none"
        }}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          mb={2}
          // mt={2}
        >
          <Typography variant="h4" gutterBottom></Typography>
          <Button
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:arrow-ios-back-outline" />}
            onClick={() => {
              navigate(-1);
              // console.log(window.location.pathname);
            }}
            style={{ backgroundColor: "#D6D6D6", color: "black" }}
          >
            Back
          </Button>
        </Stack>
        {visibleReport ? (
          <PDFViewer
            navbarOnTop={true}
            document={{
              url: urlReport
            }}
          />
        ) : (
          // <div>{urlReport}</div>
          <div>
            <p>There is no Report here!</p>
          </div>
        )}
        {/* {visibleReport ? (
          <embed
            src={`${urlReport}#toolbar=0`}
            width="500px"
            height="705vh"
            style={{ pointerEvents: "none" }}
            type="application/pdf"
          />
        ) : (
          <div>
            <p>There is no Report here!</p>
          </div>
        )} */}
      </Card>
    </Page>
  );
}
