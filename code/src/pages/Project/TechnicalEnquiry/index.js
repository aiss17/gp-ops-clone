import { filter } from "lodash";
import { sentenceCase } from "change-case";
import { useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid,
  ImageList,
  ImageListItem,
  ImageListItemBar,
} from "@mui/material";
import moment from "moment";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

// components
import Page from "../../../components/Page";
import Label from "../../../components/Label";
import Scrollbar from "../../../components/Scrollbar";
import Iconify from "../../../components/Iconify";
import SearchNotFound from "../../../components/SearchNotFound";
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
  UserMoreMenuTE,
} from "../../../sections/@dashboard/user";
//
import { TQ } from "../../../_mocks_/user";

// ----------------------------------------------------------------------

import TableHead from "@mui/material/TableHead";
import Paper from "@mui/material/Paper";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import { callApi, GetCookie } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { BMP, EXCEL, JPEG, JPG, PDF, PNG, PPT, WORD } from "src/assets/images";
import { LoadingData } from "src/pages";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user.question.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function TechnicalEnquiry(props) {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("question");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [open, setOpen] = useState(false);
  const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [data, setData] = useState([]);
  const [idTech, setIdTech] = useState(null);
  const [loading, setLoading] = useState(false);

  const [question, setQuestion] = useState("");
  const [answer, setAnswer] = useState("");
  const [subject, setSubject] = useState("");
  const [docs, setDocs] = useState([]);

  const [fileAttachment, setFileAttachment] = useState(null);

  // default set variabel input
  const [key, setKey] = useState("");

  useEffect(() => {
    console.log("ini props nya => ", JSON.stringify(props));
    GetAllTechnicalQuery();

    return () => {};
  }, []);

  const GetAllTechnicalQuery = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetAllTechnicalQuery,
      { project: props.id_project },
      "GET"
    );
    setData(response.finalResponse.data);
    setLoading(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setDisable(false);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.question);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    console.log(selectedIndex);
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const onFileChangeAttachment = (event) => {
    setFileAttachment(event.target.files[0]);
  };

  const Reply = async () => {
    const formData = new FormData();

    formData.append("project_id", props.id_project);
    formData.append("key", GetCookie("key"));
    formData.append("answer", answer);
    formData.append("docs", fileAttachment);

    const response = await callApi(
      ENDPOINT.ReplyTechnicalQuery + `/${idTech}`,
      formData,
      "UPLOAD"
    );
    setQuestion("");
    setAnswer("");
    setSubject("");
    setFileAttachment(null);
    setIdTech(null);

    handleClose();
    GetAllTechnicalQuery();
  };

  const GetTechnicalById = async (item) => {
    setKey(GetCookie("key"));

    const response = await callApi(
      ENDPOINT.GetAllTechnicalQuery + `/${item}`,
      { project: props.id_project },
      "GET"
    );

    setSubject(response.finalResponse.data[0].subject);
    setQuestion(response.finalResponse.data[0].question);
    setAnswer(response.finalResponse.data[0].answer);
    setDocs(response.finalResponse.data[0].docs);
  };

  const isUserNotFound = filteredUsers.length === 0;

  const options = ["Assessment", "Surveillance"];

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Technical Enquiry
        </Typography>
      </Stack>

      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="md"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Reply Question
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={3}>
            {/* <Grid item xs={12}>
              <Typography variant="h6" gutterBottom>
                Subject
              </Typography>
              <TextField
                autoFocus
                margin="normal"
                id="name"
                value={subject}
                type="text"
                fullWidth
                variant="outlined"
                disabled={disable}
              />
            </Grid> */}
            <Grid item xs={12}>
              <Typography variant="h6" gutterBottom>
                Subject
              </Typography>
              <TextField
                autoFocus
                margin="none"
                id="name"
                value={subject}
                type="text"
                fullWidth
                variant="outlined"
                disabled={disable}
                style={{
                  marginTop: "-10px",
                  marginBottom: 10,
                  backgroundColor: "#eeeeee",
                }}
                size="small"
              />
              <Typography variant="h6" gutterBottom>
                Question
              </Typography>
              <TextField
                autoFocus
                margin="none"
                id="name"
                value={question}
                type="text"
                fullWidth
                variant="outlined"
                disabled={disable}
                style={{
                  marginTop: "-10px",
                  marginBottom: 10,
                  backgroundColor: "#eeeeee",
                }}
                size="small"
              />

              <hr style={{ margin: "5px" }} />
              <Typography variant="h6" gutterBottom>
                Answer
              </Typography>
              <TextField
                required
                name="port_address"
                margin="none"
                id="outlined-multiline-static"
                multiline
                rows={4}
                fullWidth
                value={answer}
                onChange={(e) => {
                  setAnswer(e.target.value);
                }}
              />
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <div style={{ marginTop: 5 }}>
              <label
                htmlFor="upload-photo"
                style={{ color: "#667480", fontSize: 17 }}
              >
                Upload Documents
              </label>
              <br />
              <input
                type="file"
                style={{ color: "#667480", marginTop: 10 }}
                onChange={(e) => onFileChangeAttachment(e)}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6" gutterBottom>
              Attachment
            </Typography>
            <ImageList sx={{ width: "auto", height: "auto" }} cols={4}>
              {docs.map((item) => (
                <ImageListItem
                  key={item.id}
                  style={{
                    justifyContent: "center",
                    border: "0.5px solid",
                    margin: 5,
                    cursor: "pointer",
                    maxHeight: 200,
                    maxWidth: 200,
                  }}
                  onClick={() =>
                    window.open(
                      `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=download&key=${key}`
                    )
                  }
                >
                  <img
                    style={{
                      maxHeight: 150,
                      maxWidth: 150,
                      justifyContent: "center",
                      alignSelf: "center",
                    }}
                    src={
                      item.filetype === "pdf"
                        ? PDF
                        : item.filetype === "doc" || item.filetype === "docx"
                        ? WORD
                        : item.filetype === "xls" || item.filetype === "xlsx"
                        ? EXCEL
                        : item.filetype === "ppt" || item.filetype === "pptx"
                        ? PPT
                        : item.filetype === "jpg"
                        ? `${
                            process.env.REACT_APP_BASURL_TRX
                          }documents/fetch?path=${
                            item.path
                          }&act=view&key=${GetCookie("key")}`
                        : item.filetype === "jpeg"
                        ? `${
                            process.env.REACT_APP_BASURL_TRX
                          }documents/fetch?path=${
                            item.path
                          }&act=view&key=${GetCookie("key")}`
                        : item.filetype === "png"
                        ? `${
                            process.env.REACT_APP_BASURL_TRX
                          }documents/fetch?path=${
                            item.path
                          }&act=view&key=${GetCookie("key")}`
                        : `${
                            process.env.REACT_APP_BASURL_TRX
                          }documents/fetch?path=${
                            item.path
                          }&act=view&key=${GetCookie("key")}`
                    }
                    // srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=4 4x`}
                    // alt={item.doc_title}
                    loading="lazy"
                  />
                  <ImageListItemBar
                    subtitle={
                      item.doc_title
                        ? item.doc_title + `.${item.filetype}`
                        : "Title unknown"
                    }
                  />
                </ImageListItem>
              ))}
            </ImageList>
          </Grid>

          {/* {!disable && (
            <Grid item xs={6}>
              <div style={{ marginTop: 5 }}>
                <label
                  htmlFor="upload-photo"
                  style={{ color: "#667480", fontSize: 17 }}
                >
                  Upload Documents
                </label>
                <br />
                <input
                  type="file"
                  style={{ color: "#667480", marginTop: 10 }}
                />
              </div>
              <TextField
                required
                autoFocus
                name="upload-photo"
                type="file"
                margin="normal"
                label="Upload Documents"
                fullWidth
                variant="outlined"
              />
            </Grid>
          )} */}
        </DialogContent>
        <DialogActions>
          <Button
            onClick={Reply}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="bi:reply" />}
            style={{
              backgroundColor: "#254A7C",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            Reply
          </Button>
        </DialogActions>
      </BootstrapDialog>

      <Card>
        <UserListToolbar
          numSelected={selected.length}
          filterName={filterName}
          onFilterName={handleFilterByName}
        />

        <Scrollbar>
          <TableContainer component={Paper} style={{ borderRadius: 0 }}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell
                    style={{
                      textAlign: "center",
                    }}
                  >
                    No.
                  </StyledTableCell>
                  <StyledTableCell>Subject</StyledTableCell>
                  <StyledTableCell>Question</StyledTableCell>
                  <StyledTableCell>Latest Answer</StyledTableCell>
                  <StyledTableCell>Created Date</StyledTableCell>
                  <StyledTableCell>Action</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    //   const { id, name, role, status, company, avatarUrl, isVerified } = row;
                    //   const isItemSelected = selected.indexOf(name) !== -1;
                    const { subject, question, answer, note, startDate, id } =
                      row;
                    const isItemSelected = selected.indexOf(subject) !== -1;
                    return (
                      <TableRow>
                        <TableCell
                          style={{
                            textAlign: "center",
                          }}
                        >
                          {index + 1}
                        </TableCell>
                        <TableCell align="center">{subject}</TableCell>
                        <TableCell align="left">{question}</TableCell>
                        <TableCell align="left">{answer}</TableCell>
                        {/* <TableCell align="left">{note}</TableCell> */}
                        <TableCell align="left">
                          {moment(startDate).format("LL")}
                        </TableCell>
                        {/* <TableCell align="left">{status}</TableCell> */}
                        <TableCell align="right">
                          <UserMoreMenuTE
                            onClickView={() => {
                              setOpen(true);
                              setDisable(true);
                              GetTechnicalById(id);
                              setIdTech(id);
                            }}
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Page>
  );
}
