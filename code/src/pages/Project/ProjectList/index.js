import { filter } from "lodash";
import { sentenceCase } from "change-case";
import { useEffect, useState } from "react";
import { Link, Link as RouterLink, useNavigate } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  // TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid,
  Input,
  InputAdornment,
  FormControl,
  InputLabel,
  OutlinedInput
} from "@mui/material";
import moment from "moment";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

// components
import Page from "../../../components/Page";
import Label from "../../../components/Label";
import Scrollbar from "../../../components/Scrollbar";
import Iconify from "../../../components/Iconify";
import SearchNotFound from "../../../components/SearchNotFound";
import {
  MoreMenu,
  UserListHead,
  UserListToolbar,
  UserMoreMenu
} from "../../../sections/@dashboard/user";
//
import { DOCUMENTS } from "../../../_mocks_/user";
// table
import TableHead from "@mui/material/TableHead";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";

import filepdf from "../../../assets/PDF.pdf";

// api
import {
  callApi,
  GetCookie,
  refreshTokenApi,
  SetCookie
} from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { LoadingData } from "src/pages";

// ----------------------------------------------------------------------

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff"
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

const TABLE_HEAD = [
  { id: "no", label: "No. ", alignRight: false },
  { id: "name", label: "Document Name", alignRight: false },
  { id: "type", label: "Document Type", alignRight: false },
  { id: "category", label: "Document Category", alignRight: false },
  { id: "file", label: "File", alignRight: false },
  { id: "startDate", label: "Created Date", alignRight: false },
  { id: "" }
];

// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user.project_name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function ProjectList() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("project_name");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [open, setOpen] = useState(false);
  // const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [data, setData] = useState([]);
  const [detailuser, setListuser] = useState([]);
  const [user_id, setuseridsingle] = useState("");
  const [nama_user, setNamauser] = useState("");
  const [detaiOrder, setdetailOrder] = useState([]);
  const [order_number, setOrderNo] = useState("");
  const [order_loc, setOrderLoc] = useState("");
  const [loading, setLoading] = useState(false);

  // default set variabel input
  const [key, setKey] = useState("");

  // api
  const [orderID, setOrderID] = useState("");
  const [projectName, setProjectName] = useState("");
  const [projectValue, setProjectValue] = useState("");
  const [contractNo, setContractNo] = useState("");
  const [startDate, setStartDate] = useState(null);
  const [dueDate, setDueDate] = useState(null);
  const [description, setDescription] = useState("");

  useEffect(() => {
    GetAllProject();
    return () => {};
  }, []);

  const GetAllProject = async () => {
    setLoading(true);

    const response = await callApi(ENDPOINT.GetAllProject, null, "GET");
    setKey(GetCookie("key"));
    //console.log("dapat data project",response.finalResponse.data);
    setData(response.finalResponse.data);

    setLoading(false);
  };

  const GetAllUsers = async () => {
    const payload = {
      app: process.env.REACT_APP_TEAM
    };

    const response = await callApi(ENDPOINT.GetAllusers, payload, "GET");
    //console.log("sapi",response);
    setListuser(response.finalResponse.data);
  };

  const GetAllOrder = async () => {
    const response = await callApi(ENDPOINT.GetAllOrder, null, "GET");
    setKey(GetCookie("key"));
    setdetailOrder(response.finalResponse.data);
  };

  const InsertProject = async () => {
    // const formData = new FormData();
    setKey(GetCookie("key"));

    // formData.append("order_id", orderID);
    // formData.append("name", projectName);
    // formData.append("value", projectValue);
    // formData.append("contract_number", contractNo);
    // formData.append("start", startDate);
    // formData.append("due", dueDate);
    // formData.append("description", description);

    const payload = {
      order_id: order_number,
      project_name: projectName,
      value: projectValue,
      contract_number: contractNo,
      start: startDate,
      due: dueDate,
      description: description,
      pm: user_id,
      pm_name: nama_user
    };

    const response = await callApi(ENDPOINT.InsertProject, payload, "POST");

    //setOrderID("");
    setProjectName("");
    setProjectValue("");
    setContractNo("");
    setStartDate(null);
    setDueDate(null);
    setDescription("");

    GetAllProject();
  };

  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setDisable(false);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.project_name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    console.log(selectedIndex);
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const options = ["Assessment", "Surveillance"];

  const top100Films = [
    { title: "The Shawshank Redemption", year: 1994 },
    { title: "The Godfather", year: 1972 },
    { title: "The Godfather: Part II", year: 1974 },
    { title: "The Dark Knight", year: 2008 },
    { title: "12 Angry Men", year: 1957 },
    { title: "Schindler's List", year: 1993 },
    { title: "Pulp Fiction", year: 1994 }
  ];

  const navigate = useNavigate();

  const ListMoreButton = [
    {
      onclick: () => {
        navigate(window.location.pathname + "/tabproject");
        // alert(window.location.pathname + "/tabproject");
      },
      icon: "carbon:task-view",
      text: "Detail Project"
    }
  ];

  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={1}
      >
        <Typography variant="h4" gutterBottom>
          Project List Management
        </Typography>
        <Button
          variant="contained"
          component={RouterLink}
          to="#"
          startIcon={<Iconify icon="eva:plus-fill" />}
          // onClick={handleClickOpen}
          onClick={function (event) {
            GetAllUsers();
            GetAllOrder();
            handleClickOpen();
          }}
          style={{
            backgroundColor: "#254A7C",
            boxShadow: "none"
            // "&:hover": { color: "red" },
          }}
        >
          New Project
        </Button>
      </Stack>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="lg"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          New Project
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500]
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <h4 style={{ marginBottom: "-15px" }}>Order ID</h4>
              <Autocomplete
                id="controllable-states-demo"
                // options={order_list}
                // getOptionLabel={(option) => option.order_number}
                // onChange={(event, value) => {
                //   setOrderID(value.id);
                // }}
                options={detaiOrder}
                getOptionLabel={
                  (option) => `${option.order_number}`
                  // `${option.order_number} - ${option.location[{ city }]}`
                }
                onChange={(e, val) => {
                  console.log("Test value => ", val.id);
                  //setCriteriaId(val.id);
                  setOrderNo(val.id);
                  //console.log("orderid",orderID)
                  // setOrderLoc(val.location[{ city }]);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    required
                    fullWidth
                    size="small"
                    placeholder="-- Select Order --"
                  />
                )}
                style={{ marginBottom: 10 }}
              />
              <h4 style={{ marginBottom: "-15px" }}>Project Manager</h4>
              <Autocomplete
                id="controllable-states-demo"
                options={detailuser}
                getOptionLabel={(option) => `${option.nama} - ${option.divisi}`}
                onChange={(e, val) => {
                  console.log("Test value => ", val.nama);
                  //setCriteriaId(val.id);
                  setuseridsingle(val.id_user);
                  setNamauser(val.nama);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    required
                    fullWidth
                    size="small"
                    placeholder="-- Select PM --"
                  />
                )}
                style={{ marginBottom: 10 }}
                placeholder="Order Type"
              />
              <h4 style={{ marginBottom: "-15px" }}>Project Name</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                onChange={(e) => setProjectName(e.target.value)}
              />
              <h4 style={{ marginBottom: "-15px" }}>Project Value</h4>
              {/* <FormControl fullWidth sx={{ m: 1 }}>
          <InputLabel htmlFor="outlined-adornment-amount">Amount</InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            // value={values.amount}
            onChange={handleChange('amount')}
            startAdornment={<InputAdornment position="start">$</InputAdornment>}
            label="Amount"
          />
        </FormControl> */}
              <TextField
                margin="normal"
                id="value"
                type="number"
                fullWidth
                variant="outlined"
                size="small"
                startAdornment={
                  <InputAdornment position="start">Rp</InputAdornment>
                }
                onChange={(e) => setProjectValue(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>Contract No.</h4>
              <TextField
                margin="normal"
                id="contract"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                onChange={(e) => setContractNo(e.target.value)}
              />
            </Grid>
            <Grid item xs={6}>
              <h4 style={{ marginBottom: "-15px" }}>Start Date</h4>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  disabled={disable}
                  // label="Requested Date"
                  value={startDate}
                  onChange={(newValue) => {
                    setStartDate(newValue);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      margin="normal"
                      required
                      fullWidth
                      size="small"
                    />
                  )}
                />
              </LocalizationProvider>
              <h4 style={{ marginBottom: "-15px" }}>Due Date</h4>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  disabled={disable}
                  // label="Requested Date"
                  value={dueDate}
                  onChange={(newValue) => {
                    setDueDate(newValue);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      margin="normal"
                      required
                      fullWidth
                      size="small"
                    />
                  )}
                />
              </LocalizationProvider>
              <h4 style={{ marginBottom: "-15px" }}>Description</h4>
              <TextField
                margin="normal"
                id="contract"
                type="text"
                multiline
                rows={5}
                fullWidth
                variant="outlined"
                size="small"
                onChange={(e) => setDescription(e.target.value)}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={function (event) {
              handleClose();
              InsertProject();
            }}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
              backgroundColor: "#254A7C",
              boxShadow: "none"
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>
      <Card>
        <UserListToolbar
          numSelected={selected.length}
          filterName={filterName}
          onFilterName={handleFilterByName}
        />

        <Scrollbar>
          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <StyledTableCell>Order ID</StyledTableCell>
                  <StyledTableCell>Order No.</StyledTableCell>
                  <StyledTableCell>Project Name</StyledTableCell>
                  <StyledTableCell>Project Value</StyledTableCell>
                  <StyledTableCell>Contract No.</StyledTableCell>
                  <StyledTableCell>Start Date</StyledTableCell>
                  <StyledTableCell>Due Date</StyledTableCell>
                  <StyledTableCell>Description</StyledTableCell>
                  <StyledTableCell>Action</StyledTableCell>
                </TableRow>
              </TableHead>
              {loading ? (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={9} sx={{ py: 3 }}>
                      <LoadingData />
                    </TableCell>
                  </TableRow>
                </TableBody>
              ) : (
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      //   const { id, name, role, status, company, avatarUrl, isVerified } = row;
                      //   const isItemSelected = selected.indexOf(name) !== -1;
                      const {
                        id,
                        order_id,
                        order_number,
                        project_name,
                        value,
                        contract_number,
                        start,
                        due,
                        description
                      } = row;
                      const isItemSelected = selected.indexOf(orderID) !== -1;

                      return (
                        <TableRow>
                          <TableCell align="left">{order_id}</TableCell>
                          <TableCell align="left">{order_number}</TableCell>
                          <TableCell align="left">{project_name}</TableCell>
                          <TableCell align="left">{value}</TableCell>
                          <TableCell align="left">{contract_number}</TableCell>
                          <TableCell align="left">
                            {moment(start).format("LL")}
                          </TableCell>
                          <TableCell align="left">
                            {moment(due).format("LL")}
                          </TableCell>
                          <TableCell align="left">{description}</TableCell>
                          <TableCell align="right">
                            <Button
                              variant="contained"
                              component={RouterLink}
                              to={
                                window.location.pathname + `/tabproject/${id}`
                              }
                              startIcon={<Iconify icon="bx:detail" />}
                              style={{
                                backgroundColor: "#254A7C",
                                boxShadow: "none"
                                // "&:hover": { color: "red" },
                              }}
                            >
                              Detail
                            </Button>
                            {/* <Link to={window.location.pathname + `/tabproject/${id}`}>handleChange</Link> */}
                          </TableCell>
                        </TableRow>
                      );
                    })}

                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}

                  {isUserNotFound && (
                    <TableRow>
                      <TableCell align="center" colSpan={9} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  )}
                </TableBody>
              )}
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Page>
  );
}
