import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import {
  Link,
  Link as RouterLink,
  useNavigate,
  useParams
} from "react-router-dom";
// material
import {
  Card,
  Stack,
  Button,
  Typography,
  Grid,
  CardHeader,
  CardContent,
  Divider
} from "@mui/material";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";

// components
import Page from "../../../components/Page";
import Scrollbar from "../../../components/Scrollbar";
import Iconify from "../../../components/Iconify";
//
import { DOCUMENTS } from "../../../_mocks_/user";
import { formatDistance } from "date-fns";
import faker from "@faker-js/faker";
import { mockImgCover } from "src/utils/mockImages";
import {
  PresentationEvidenceGraph,
  ReviewEvidenceGraph,
  VisitGraph,
  VisitGraph_v2
} from "../../../sections/@dashboard/app";
import { callApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import moment from "moment";
import { HISTORY } from "src/assets/images";
import { LoadingData } from "src/pages";

// const steps = [
//   { type: "Preparation", date: "24-12-2022" },
//   { type: "Document Review", date: "24-12-2022" },
//   { type: "Assessment", date: "24-12-2022" }
// ];

// ----------------------------------------------------------------------

// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

export default function Overview(props) {
  const [open, setOpen] = useState(false);
  const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [disable, setDisable] = useState(false);
  const [activeStep, setActiveStep] = useState(0);
  const [skipped, setSkipped] = useState(new Set());
  const [steps, setSteps] = useState([]);
  const [date, setdate] = useState("2020-06-06");
  const [logActivity, setLogActivity] = useState([]);
  const [loading, setLoading] = useState(false);

  // Informasi Pelabuhan
  const [clientName, setClientName] = useState("");
  const [namaPelabuhan, setNamaPelabuhan] = useState("");
  const [alamat, setAlamat] = useState("");
  const [pemilik, setPemilik] = useState("");
  const [operator, setOperator] = useState("");
  const [equipment, setEquipment] = useState("");

  const params = useParams();

  const isStepOptional = (step) => {
    return step === 1;
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  useEffect(() => {
    setLoading(true);
    GetInfoProject();
    console.log(DOCUMENTS);
    console.log("ini props nya => ", JSON.stringify(props.id_project));

    return () => {};
  }, []);

  const GetInfoProject = async () => {
    const response = await callApi(
      ENDPOINT.GetAllProject + `/${props.id_project}`,
      null,
      "GET"
    );

    setClientName(response.finalResponse.data[0].applicant);
    setNamaPelabuhan(response.finalResponse.data[0].port_name);
    setAlamat(response.finalResponse.data[0].address);
    setPemilik(response.finalResponse.data[0].owner_name);
    setOperator(response.finalResponse.data[0].operator);
    setEquipment(response.finalResponse.data[0].equipment);

    setSteps([
      { type: "Start", date: response.finalResponse.data[0].start },
      { type: "Preparation", date: response.finalResponse.data[0].preparation },
      {
        type: "Document Review",
        date: response.finalResponse.data[0].doc_review
      },
      { type: "Assessment", date: response.finalResponse.data[0].assessment },
      { type: "Due", date: response.finalResponse.data[0].due }
    ]);
    GetLogActivity();
  };

  const GetLogActivity = async () => {
    let array = [];
    const response = await callApi(
      ENDPOINT.GetLogActivity,
      { project: params.id, category: "view" },
      "GET"
    );

    console.log(response.finalResponse.data);

    if (response.finalResponse.data.length > 0) {
      if (response.finalResponse.data.length > 4) {
        for (let i = 0; i < 4; i++) {
          array.push(response.finalResponse.data[i]);
        }

        setLogActivity(array);
      } else {
        for (let i = 0; i < response.finalResponse.data.length; i++) {
          array.push(response.finalResponse.data[i]);
        }

        setLogActivity(array);
      }
    }

    setLoading(false);
    console.log("Log Activity => ", array);
  };

  const options = ["Assessment", "Surveillance"];

  const NEWS = [...Array(4)].map((_, index) => {
    const setIndex = index + 1;
    return {
      title: faker.name.title(),
      description: faker.lorem.paragraphs(),
      image: mockImgCover(setIndex),
      postedAt: faker.date.soon()
    };
  });

  NewsItem.propTypes = {
    news: PropTypes.object.isRequired
  };

  function NewsItem({ news }) {
    const { subject, details, created_at } = news;

    return (
      <Stack direction="row" alignItems="center" spacing={2}>
        <Box
          component="img"
          alt={subject}
          src={HISTORY}
          sx={{ width: 48, height: 48, borderRadius: 1 }}
        />
        <Box sx={{ minWidth: 240 }}>
          <Link to="#" color="inherit" underline="hover" component={RouterLink}>
            <Typography variant="subtitle2" noWrap>
              {subject}
            </Typography>
          </Link>
          <Typography variant="body2" sx={{ color: "text.secondary" }} noWrap>
            {details}
          </Typography>
        </Box>
        <Typography
          variant="caption"
          sx={{ pr: 3, flexShrink: 0, color: "text.secondary" }}
        >
          {formatDistance(new Date(created_at), new Date())}
        </Typography>
      </Stack>
    );
  }

  const navigate = useNavigate();

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      {/* <Container> */}
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Overview
        </Typography>
      </Stack>
      <Card>
        <CardHeader title="Timeline" style={{ textAlign: "center" }} />
        <Box
          sx={{
            width: "85%",
            margin: 10
          }}
        >
          <Stepper activeStep={activeStep}>
            {steps.map((label, index) => {
              const stepProps = {};
              const labelProps = {};

              // Set Date
              var date = new Date(label.date);
              var now = new Date();
              // if (isStepOptional(index)) {
              labelProps.optional = (
                <Typography variant="caption">
                  {moment(label.date).format("DD MMM YYYY")}
                </Typography>
              );
              // }

              // if (laterDate.isAfter(currentDate)) {
              // console.log(laterDate);
              // }
              return (
                <Step key={label} completed={now > date}>
                  <StepLabel {...labelProps}>{label.type}</StepLabel>
                </Step>
              );
            })}
          </Stepper>
        </Box>
      </Card>

      <Grid container spacing={5} style={{ marginTop: 1 }}>
        <Grid item xs={12} md={6} lg={6}>
          <Card>
            <CardHeader
              title="Informasi Pelabuhan"
              style={{ textAlign: "center" }}
            />
            <CardContent>
              <Typography style={{ marginTop: 10 }}>
                <b>Client:</b>
              </Typography>
              <Typography style={{ marginTop: 5 }}>
                {clientName === null ? "-" : clientName}
              </Typography>
              <Typography style={{ marginTop: 10 }}>
                <b>Nama Pelabuhan:</b>
              </Typography>
              <Typography style={{ marginTop: 5 }}>
                {namaPelabuhan === null ? "-" : namaPelabuhan}
              </Typography>
              <Typography style={{ marginTop: 10 }}>
                <b>Alamat:</b>
              </Typography>
              <Typography style={{ marginTop: 5 }}>
                {alamat === null ? "-" : alamat}
              </Typography>
              <Typography style={{ marginTop: 10 }}>
                <b>Pemilik:</b>
              </Typography>
              <Typography style={{ marginTop: 5 }}>
                {pemilik === null ? "-" : pemilik}
              </Typography>
              <Typography style={{ marginTop: 10 }}>
                <b>Operator:</b>
              </Typography>
              <Typography style={{ marginTop: 5 }}>
                {operator === null ? "-" : operator}
              </Typography>
              <Typography style={{ marginTop: 10 }}>
                <b>Equipment Bongkar Muat:</b>
              </Typography>
              <Typography style={{ marginTop: 5 }}>
                {equipment === null ? "-" : "1. " + equipment}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={6}>
          <Card>
            <CardHeader title="User Activity" style={{ textAlign: "center" }} />
            <CardContent>
              <Scrollbar>
                <Stack spacing={3} sx={{ p: 3, pr: 0 }}>
                  {logActivity.length > 0 ? (
                    logActivity.map((news) => (
                      <NewsItem key={news.subject} news={news} />
                    ))
                  ) : (
                    <div
                      style={{
                        padding: 10,
                        width: "100%",
                        textAlign: "center",
                        border: "1px solid #dddddd"
                      }}
                    >
                      No recent activity
                    </div>
                  )}
                </Stack>
              </Scrollbar>

              <Divider />

              <Box sx={{ p: 2, textAlign: "right" }}>
                <Button
                  to="#"
                  size="small"
                  color="inherit"
                  component={RouterLink}
                  endIcon={<Iconify icon="eva:arrow-ios-forward-fill" />}
                >
                  View all
                </Button>
              </Box>
            </CardContent>
          </Card>
        </Grid>
      </Grid>

      <Grid container spacing={3} style={{ marginTop: 5 }}>
        <Grid item xs={12} sm={6} md={4}>
          {/* <PresentationEvidenceGraph /> */}
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          {/* <ReviewEvidenceGraph /> */}
        </Grid>
        <Grid item xs={12}>
          <VisitGraph_v2 />
        </Grid>
      </Grid>
      {/* </Container> */}
    </Page>
  );
}
