import { filter } from "lodash";
import { sentenceCase } from "change-case";
import React, { useRef, useEffect, useState } from "react";
import { Link as RouterLink, useNavigate, useParams } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid,
} from "@mui/material";
import moment from "moment";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

// components
import Page from "../../../components/Page";
import Label from "../../../components/Label";
import Scrollbar from "../../../components/Scrollbar";
import Iconify from "../../../components/Iconify";
import SearchNotFound from "../../../components/SearchNotFound";
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
  UserMoreMenuVisit,
} from "../../../sections/@dashboard/user";
//
import { DOCUMENTS } from "../../../_mocks_/user";

import filepdf from "../../../assets/PDF.pdf";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";

// editor
import { Editor } from "@tinymce/tinymce-react";

// table
import TableHead from "@mui/material/TableHead";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
// api
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { id } from "date-fns/locale";
import { usePagination } from "@mui/lab";
import { LoadingData } from "src/pages";

// ----------------------------------------------------------------------
const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user.location.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Visit(props) {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("location");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [open, setOpen] = useState(false);
  const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [loading, setLoading] = useState(false);

  // default set variabel input
  const [key, setKey] = useState("");
  const [userid, setUserID] = useState(null);
  const [projectid, setProjectID] = useState(null);
  const [visitno, setVisitNo] = useState(null);

  // editor
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
    }
  };
  //api
  const [visitdate, setVisitdate] = useState(null);
  const [formatDate, setFormatDate] = useState(null);
  const [location, setLocation] = useState("");
  const [criteria, setCriteria] = useState([]);
  const [criteriaid, setCriteriaID] = useState("");
  const [criteriacode, setCriteriaCode] = useState([]);
  const [description, setDescription] = useState("");
  const [data, setData] = useState([]);
  const [fileupload, setFileupload] = useState([]);

  const ref = useRef();
  const params = useParams();

  // const [criteria, setCriteria] = useState(null);

  useEffect(() => {
    console.log("data nya nichhh => ", data);
    GetAllVisit();
    return () => {};
  }, []);

  const GetAllVisit = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetAllVisit,
      { project: params.id },
      "GET"
    );
    setKey(GetCookie("key"));
    setData(response.finalResponse.data);
    setLoading(false);
  };

  const GetAllCriteria = async () => {
    const response = await callApi(
      ENDPOINT.GetAllCriteria,
      { project: params.id },
      "GET"
    );
    // setKey(GetCookie("key"));
    setCriteriaCode(response.finalResponse.data);
  };

  const InsertVisit = async () => {
    var criteracode = "";
    for (let i = 0; i < criteria.length; i++) {
      // criteracode.push(criteria[i].code);
      criteracode += criteria[i].code + `-` + criteria[i].criteria_name + ",";
    }
    console.log("tanggal visit => ", formatDate);
    criteracode = criteracode.substring(0, criteracode.length - 1);
    // setKey(GetCookie("key"));

    const formData = new FormData();

    formData.append("key", key);
    formData.append("userid", GetCookie("id_user"));
    formData.append("project_id", props.id_project);
    formData.append("visit_num", 2);
    formData.append("visit_date", formatDate);
    formData.append("criteria", criteracode);
    formData.append("location", location);
    formData.append("description", description);

    for (let i = 0; i < fileupload.length; i++) {
      formData.append("docs[]", fileupload[i]);
    }

    const response = await callApi(ENDPOINT.GetAllVisit, formData, "UPLOAD");

    setVisitdate(null);
    setLocation("");
    setCriteria(null);
    setDescription("");
    setFileupload([]);

    GetAllVisit();
  };

  const DeleteVisit = async (id) => {
    const response = await callApi(
      ENDPOINT.DeleteVisit + id,
      { project: params.id },
      "POST"
    );
    GetAllVisit();
  };

  const onFileChange = (event) => {
    const imagesArray = [];
    for (let i = 0; i < event.target.files.length; i++) {
      imagesArray.push(event.target.files[i]);
    }
    setFileupload(imagesArray);
  };

  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setDisable(false);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.location);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    console.log(selectedIndex);
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const navigate = useNavigate();

  const DetailButton = [
    {
      onclick: () => {
        navigate(window.location.pathname + "/visitdetail");
        // alert(window.location.pathname + "/tabproject");
      },
      icon: "carbon:task-view",
      text: "Visit Detail",
    },
  ];

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          List of Visit
        </Typography>
        <Button
          variant="contained"
          component={RouterLink}
          to="#"
          startIcon={<Iconify icon="eva:plus-fill" />}
          // onClick={handleClickOpen}
          onClick={function (event) {
            handleClickOpen();
            GetAllCriteria();
          }}
          style={{ backgroundColor: "#254A7C", boxShadow: "none" }}
        >
          Add New Visit
        </Button>
      </Stack>

      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="md"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          New Visit
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <h4>Visit Date</h4>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  // label="Visit Date"
                  value={valueDate}
                  onChange={(newValue) => {
                    let text = newValue.toLocaleDateString("en-US");
                    setValueDate(text);
                    setVisitdate(text);
                    setFormatDate(moment(newValue).format("yyyy-MM-DD"));
                    // onChange={(newValue) => {
                    //   setValueDate(newValue);
                    // }}
                    console.log("sapi => ", moment(newValue).format("yyyy-MM-DD"));
                  }}
                  renderInput={(params) => (
                    <TextField {...params} required fullWidth />
                  )}
                />
              </LocalizationProvider>
            </Grid>
            <Grid item xs={6}>
              <h4>Location</h4>
              <TextField
                required
                id="name"
                // label="Location"
                type="text"
                fullWidth
                variant="outlined"
                onChange={(e) => setLocation(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <h4>Scope of Inspection</h4>
              <Editor
                apiKey={process.env.REACT_APP_API_KEY_TINYMCE}
                onInit={(evt, editor) => (editorRef.current = editor)}
                // initialValue="<p>Type Here...</p>"
                // value="<p>Type Here...</p>"
                init={{
                  height: 300,
                  menubar: false,
                  plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste code help wordcount",
                  ],
                  toolbar:
                    "undo redo | formatselect | " +
                    "bold italic backcolor | alignleft aligncenter " +
                    "alignright alignjustify | bullist numlist outdent indent | " +
                    "removeformat | help",
                  content_style:
                    "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                }}
                onEditorChange={(content, editor) =>
                  setDescription(editor.getContent())
                }

                // onChange={(e) => setDescription(e.target.value)}
              />
            </Grid>
            <Grid item xs={6}>
              <h4>Work Criteria</h4>
              <Autocomplete
                multiple
                id="checkboxes-tags-demo"
                options={criteriacode}
                getOptionLabel={(option) =>
                  `${option.code} - ${option.criteria_name}`
                }
                onChange={(e, val) => {
                  // console.log("Test value => ", val);
                  setCriteria(val);
                }}
                // options={top10Films}
                // getOptionLabel={(option) => option.title}
                // onChange={(event, value) => {
                //   setCriteria(value.id);
                // }}
                // getOptionLabel={(option) => option.title}
                renderOption={(props, option, { selected }) => (
                  <li {...props}>
                    <Checkbox
                      icon={icon}
                      checkedIcon={checkedIcon}
                      style={{ marginRight: 8 }}
                      checked={selected}
                    />
                    {option.code} -{option.criteria_name}
                  </li>
                )}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    // label="Select Work Criteria"
                    fullWidth
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <h4>Attachement</h4>
              <div style={{ marginTop: "-5px" }}>
                <input
                  type="file"
                  style={{ color: "#494949", marginTop: 10 }}
                  multiple
                  ref={ref}
                  onChange={(e) => onFileChange(e)}
                />
              </div>
            </Grid>
          </Grid>
          <hr style={{ margin: 5 }}></hr>
          <Grid item xs={12} style={{ float: "right" }}>
            <Button
              onClick={function (event) {
                handleClose();
                InsertVisit();
              }}
              variant="contained"
              component={RouterLink}
              to="#"
              startIcon={<Iconify icon="entypo:save" />}
              style={{
                backgroundColor: "#254A7C",
                boxShadow: "none",
                marginRight: 20,
                marginTop: 10,
                marginBottom: 10,
              }}
            >
              Save
            </Button>
          </Grid>
        </DialogContent>
      </BootstrapDialog>

      <Card>
        <UserListToolbar
          numSelected={selected.length}
          filterName={filterName}
          onFilterName={handleFilterByName}
        />

        <Scrollbar>
          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <StyledTableCell>Project ID</StyledTableCell>
                  {/* <StyledTableCell>Visit No.</StyledTableCell> */}
                  <StyledTableCell style={{ width: "250px" }}>
                    Criteria
                  </StyledTableCell>
                  <StyledTableCell>Location</StyledTableCell>
                  <StyledTableCell>Visit Date</StyledTableCell>
                  {/* <StyledTableCell>Description</StyledTableCell> */}
                  <StyledTableCell
                    style={{ width: "150px", textAlign: "center" }}
                  >
                    Action
                  </StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    //   const { id, name, role, status, company, avatarUrl, isVerified } = row;
                    //   const isItemSelected = selected.indexOf(name) !== -1;
                    const {
                      id,
                      project_id,
                      visit_num,
                      criteria,
                      location,
                      visit_date,
                      description,
                    } = row;

                    return (
                      <TableRow>
                        <TableCell align="left">{project_id}</TableCell>
                        {/* <TableCell align="left">{visit_num}</TableCell> */}
                        <TableCell align="left">
                          {criteria.toString()}
                        </TableCell>
                        <TableCell align="left">{location}</TableCell>
                        <TableCell align="left">
                          {moment(visit_date).format("LL")}
                          {/* {visit_date} */}
                        </TableCell>
                        {/* <TableCell align="left">{description}</TableCell> */}
                        <TableCell
                          style={{ width: "150px", textAlign: "center" }}
                        >
                          <Button
                            variant="contained"
                            component={RouterLink}
                            to={
                              window.location.pathname + `/visitdetails/${id}`
                            }
                            startIcon={<Iconify icon="bx:detail" />}
                            style={{
                              backgroundColor: "transparent",
                              boxShadow: "none",
                              color: "#254A7C",
                              textAlign: "center",
                              margin: "-20px",
                            }}
                            title="Detail Visit"
                          />
                          |
                          <Button
                            variant="contained"
                            onClick={function (event) {
                              DeleteVisit(id);
                            }}
                            startIcon={<Iconify icon="bxs:trash-alt" />}
                            style={{
                              backgroundColor: "transparent",
                              boxShadow: "none",
                              color: "red",
                              textAlign: "center",
                              margin: "-10px",
                            }}
                            title="Delete Visit"
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={8} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Page>
  );
}
