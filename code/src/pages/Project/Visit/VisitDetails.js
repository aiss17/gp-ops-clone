import { filter } from "lodash";
import { sentenceCase } from "change-case";
import React, { useRef, useEffect, useState } from "react";
import { Link as RouterLink, useNavigate, useParams } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid
} from "@mui/material";
import moment from "moment";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

// components
import Page from "../../../components/Page";
import Iconify from "../../../components/Iconify";

import filepdf from "../../../assets/PDF.pdf";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";

// api
import {
  callApi,
  GetCookie,
  refreshTokenApi,
  SetCookie
} from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { BMP, EXCEL, JPEG, JPG, PDF, PNG, PPT, WORD } from "src/assets/images";
import { Editor } from "@tinymce/tinymce-react";
import { LoadingData } from "src/pages";

// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function VisitDetails(props) {
  const { id, id_visit } = useParams();
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("name");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [open, setOpen] = useState(false);
  const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");

  // default set variabel input
  const [key, setKey] = useState("");
  const [data, setData] = useState([]);

  //api
  const [visit_date, setVisitdate] = useState(null);
  const [location, setLocation] = useState("");
  const [criteria, setCriteria] = useState([]);
  const [criteriaid, setCriteriaID] = useState("");
  const [criteriacode, setCriteriaCode] = useState([]);
  const [description, setDescription] = useState("");
  const [docs, setDocs] = useState([]);
  const [loading, setLoading] = useState(false);

  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
    }
  };

  useEffect(() => {
    console.log("ini state Hahahaha => " + id + " dan " + id_visit);
    GetVisitByID();
    // props.id_project;
    return () => {};
  }, []);

  const GetVisitByID = async () => {
    setLoading(true);
    // const payload = {
    //   app: process.env.REACT_APP_CLIENT,
    // };

    const response = await callApi(
      ENDPOINT.GetVisitByID + id_visit,
      { project: id },
      "GET"
    );
    setKey(GetCookie("key"));
    //console.log("dapat data order",response.finalResponse.data);
    setData(response.finalResponse.data);

    setLocation(response.finalResponse.data[0].location);
    setDescription(response.finalResponse.data[0].description);
    setCriteria(response.finalResponse.data[0].criteria);
    setDocs(response.finalResponse.data[0].docs);
    setVisitdate(
      moment(response.finalResponse.data[0].visit_date).format("MMM DD, yyyy")
    );
    setLoading(false);
  };

  const navigate = useNavigate();

  const itemData = [
    {
      img: "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e",
      title: "Breakfast",
      author: "@bkristastucchio"
    },
    {
      img: "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
      title: "Burger",
      author: "@rollelflex_graphy726"
    },
    {
      img: "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
      title: "Camera",
      author: "@helloimnik"
    },
    {
      img: "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c",
      title: "Coffee",
      author: "@nolanissac"
    },
    {
      img: "https://images.unsplash.com/photo-1533827432537-70133748f5c8",
      title: "Hats",
      author: "@hjrc33"
    },
    {
      img: "https://images.unsplash.com/photo-1558642452-9d2a7deb7f62",
      title: "Honey",
      author: "@arwinneil"
    },
    {
      img: "https://images.unsplash.com/photo-1516802273409-68526ee1bdd6",
      title: "Basketball",
      author: "@tjdragotta"
    },
    {
      img: "https://images.unsplash.com/photo-1518756131217-31eb79b20e8f",
      title: "Fern",
      author: "@katie_wasserman"
    },
    {
      img: "https://images.unsplash.com/photo-1597645587822-e99fa5d45d25",
      title: "Mushrooms",
      author: "@silverdalex"
    },
    {
      img: "https://images.unsplash.com/photo-1567306301408-9b74779a11af",
      title: "Tomato basil",
      author: "@shelleypauls"
    },
    {
      img: "https://images.unsplash.com/photo-1471357674240-e1a485acb3e1",
      title: "Sea star",
      author: "@peterlaster"
    },
    {
      img: "https://images.unsplash.com/photo-1589118949245-7d38baf380d6",
      title: "Bike",
      author: "@southside_customs"
    }
  ];

  const [editorState, setEditorState] = useState("");

  useEffect(() => {
    console.log(editorState);
  }, [editorState]);

  const onEditorStateChange = (editorState) => {
    // console.log(editorState)
    setEditorState(editorState);
  };

  const [text, setText] = useState("");

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Page title="Visit Details | My Green Port" style={{ margin: 10 }}>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Detail Visit
        </Typography>
        <Button
          component={RouterLink}
          to="#"
          startIcon={<Iconify icon="eva:arrow-ios-back-outline" />}
          onClick={() => {
            navigate(-1);
            // console.log(window.location.pathname);
          }}
          style={{ backgroundColor: "#D6D6D6", color: "black" }}
        >
          Back to List
        </Button>
      </Stack>
      <Card style={{ padding: 20 }}>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <h4>
              <strong>Visit Date</strong>
            </h4>
            <p>{visit_date}</p>
            {/* <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                disabled={disable}
                value="08/28/2020"
                onChange={(newValue) => {
                  setValueDate(newValue);
                }}
                renderInput={(params) => (
                  <TextField {...params} required fullWidth disabled />
                )}
              />
            </LocalizationProvider> */}
          </Grid>
          <Grid item xs={6}>
            <h4>
              <strong>Location</strong>
            </h4>
            <p>{location}</p>
            {/* <TextField
              required
              autoFocus
              id="name"
              type="text"
              fullWidth
              variant="outlined"
              disabled={disable}
              value="Jakarta"
            /> */}
          </Grid>
          <Grid item xs={12}>
            <h4>
              <strong>Scope of Inspection</strong>
            </h4>
            <div>
              {/* <span>{description}</span> */}
              <Editor
                apiKey={process.env.REACT_APP_API_KEY_TINYMCE}
                onInit={(evt, editor) => (editorRef.current = editor)}
                // initialValue="<p>Type Here...</p>"
                // value="<p>Type Here...</p>"
                init={{
                  height: 300,
                  menubar: false,
                  plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste code help wordcount",
                  ],
                  toolbar:
                    "undo redo | formatselect | " +
                    "bold italic backcolor | alignleft aligncenter " +
                    "alignright alignjustify | bullist numlist outdent indent | " +
                    "removeformat | help",
                  content_style:
                    "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                }}
                onEditorChange={(content, editor) =>
                  setDescription(editor.getContent())
                }
                value={description}
                disabled
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <h4>
              <strong>Work Criteria</strong>
            </h4>
            {/* <p>{criteria}</p> */}
            {criteria.map((value, index) => {
              return <p>- {value}</p>;
            })}
          </Grid>
          <Grid item xs={12}>
            <h4>
              <strong>Attachement</strong>
            </h4>
            <ImageList sx={{ width: "auto", height: 350 }} cols={5}>
              {docs.map((item) => (
                <ImageListItem
                  key={item.id}
                  style={{
                    justifyContent: "center",
                    border: "0.5px solid",
                    margin: 5,
                    cursor: "pointer",
                    maxHeight: 200,
                    maxWidth: 200
                  }}
                  onClick={() =>
                window.open(
                  `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=download&key=${key}`
                )
              }
                >
                  <img
                    style={{
                      maxHeight: 150,
                      maxWidth: 150,
                      justifyContent: "center",
                      alignSelf: "center"
                    }}
                    src={
                      item.filetype === "pdf"
                        ? PDF
                    : item.filetype === "doc" || item.filetype === "docx"
                    ? WORD
                    : item.filetype === "xls" || item.filetype === "xlsx"
                    ? EXCEL
                    : item.filetype === "ppt" || item.filetype === "pptx"
                    ? PPT
                    : item.filetype === "jpg"
                    ? `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=view&key=${key}`
                    : item.filetype === "jpeg"
                    ? `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=view&key=${key}`
                    : item.filetype === "png"
                    ? `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=view&key=${key}`
                    : `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=view&key=${key}`
                    }
                    // srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=4 4x`}
                    alt={item.doc_title}
                    loading="lazy"
                  />
                  <ImageListItemBar
                    subtitle={
                      item.doc_title
                        ? item.doc_title + `.${item.filetype}`
                        : "Title unknown"
                    }
                  />
                </ImageListItem>
              ))}
            </ImageList>
          </Grid>
        </Grid>
      </Card>
    </Page>
  );
}
