import { Card, List, ListItem } from "@mui/material";
import Page from "src/components/Page";
import PDFViewer from "pdf-viewer-reactjs";
import { underconstruction } from "../../../assets/images";
import { callApi, GetCookie } from "src/services/Functions";
import { useParams } from "react-router-dom";
import { ENDPOINT } from "src/services/Constants";
import { LoadingData } from "src/pages";
import { useEffect, useState } from "react";

export default function UC() {
  const [urlReport, setUrlReport] = useState(null);
  const [visibleReport, setVisibleReport] = useState(false);
  const [loading, setLoading] = useState(false);
  const { id } = useParams();

  useEffect(() => {
    GetFileReport();

    return () => {};
  }, []);

  const GetFileReport = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetFileReport,
      { project: id, extension: "pdf" },
      "GET"
    );

    console.log("Check response get file => ", response.finalResponse);
    if (response.finalResponse.status !== "empty") {
      setUrlReport(
        `${process.env.REACT_APP_BASURL_TRX}project/report/fetch?project=${id}&key=${GetCookie(
          "key"
        )}&extension=pdf`
      );

      setVisibleReport(false);

      setTimeout(() => {
        setLoading(false);
        setVisibleReport(true);
      }, 500);
    } else {
      setLoading(false);
      setVisibleReport(false);
    }
  };

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      <Card
        style={{
          padding: 15,
          // maxHeight: "55%",
          height: "100%",
          overflow: "auto",
          textAlign: "center",
          boxShadow: "none"
        }}
      >
        {visibleReport ? (
          <PDFViewer
            navbarOnTop={true}
            document={{
              url: urlReport
            }}
          />
        ) : (
          <div>
            <p>There is no Report here!</p>
          </div>
        )}
      </Card>
    </Page>
  );
}
