import Article from "@mui/icons-material/Article";
import CellTowerIcon from "@mui/icons-material/CellTower";
import FactCheckIcon from "@mui/icons-material/FactCheck";
import Dashboard from "@mui/icons-material/Dashboard";
import QuizIcon from "@mui/icons-material/Quiz";
import SummarizeIcon from "@mui/icons-material/Summarize";
import VideoCameraFrontIcon from "@mui/icons-material/VideoCameraFront";
import WorkIcon from "@mui/icons-material/Work";
import TopicIcon from "@mui/icons-material/Topic";

import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import * as React from "react";
import Tabs, { tabsClasses } from "@mui/material/Tabs";
import { useEffect, useState } from "react";
import { ENDPOINT } from "src/services/Constants";
// api
import {
  callApi,
  GetCookie,
  refreshTokenApi,
  SetCookie,
} from "src/services/Functions";
// pages
import {
  Assessment,
  Document,
  Meeting,
  Overview,
  // RemoteSurvey,
  RemoteUC,
  Report,
  TechnicalEnquiry,
  Administrative,
} from "../index";
import Visit from "../Visit";
import { useParams } from "react-router-dom";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const { id } = useParams();
  const [value, setValue] = React.useState(0);
  const [dataproject, setDataproject] = useState([]);

  React.useEffect(() => {
    console.log("ini state ewrfwer => ", id);
    GetInfoProject(id);
    return () => {};
  }, []);

  const GetInfoProject = async (id) => {
    const response = await callApi(
      ENDPOINT.GetAllProject + `/${id}`,
      null,
      "GET"
    );

    setDataproject(response.finalResponse.data[0]);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box style={{ clear: "both" }}>
      <Typography variant="h4" gutterBottom>
        Project : {dataproject.project_name}
      </Typography>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons
          aria-label="visible arrows tabs example"
          sx={{
            [`& .${tabsClasses.scrollButtons}`]: {
              "&.Mui-disabled": { opacity: 0.3 },
            },
          }}
        >
          <Tab
            icon={<Dashboard />}
            iconPosition={"start"}
            label="Overview"
            {...a11yProps(0)}
            style={{ color: "#254A7C" }}
          />
          <Tab
            icon={<Article />}
            iconPosition={"start"}
            label="Document"
            {...a11yProps(1)}
            style={{ color: "#254A7C" }}
          />
          <Tab
            icon={<VideoCameraFrontIcon />}
            iconPosition={"start"}
            label="Meeting"
            {...a11yProps(2)}
            style={{ color: "#254A7C" }}
          />
          <Tab
            icon={<WorkIcon />}
            iconPosition={"start"}
            label="Visit"
            {...a11yProps(3)}
            style={{ color: "#254A7C" }}
          />
          <Tab
            icon={<CellTowerIcon />}
            iconPosition={"start"}
            label="Remote Inspection"
            {...a11yProps(4)}
            style={{ color: "#254A7C" }}
          />
          <Tab
            icon={<FactCheckIcon />}
            iconPosition={"start"}
            label="Assessment Workspace"
            {...a11yProps(5)}
            style={{ color: "#254A7C" }}
          />
          <Tab
            icon={<QuizIcon />}
            iconPosition={"start"}
            label="Technical Enquiry"
            {...a11yProps(6)}
            style={{ color: "#254A7C" }}
          />
          <Tab
            icon={<SummarizeIcon />}
            iconPosition={"start"}
            label="Report"
            {...a11yProps(7)}
            style={{ color: "#254A7C" }}
          />
          <Tab
            icon={<TopicIcon />}
            iconPosition={"start"}
            label="Administrative"
            {...a11yProps(8)}
            style={{ color: "#254A7C" }}
          />
        </Tabs>
      </Box>

      <TabPanel value={value} index={0}>
        <Overview id_project={id} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Document id_project={id} />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Meeting id_project={id} />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Visit id_project={id} />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <RemoteUC id_project={id} />
      </TabPanel>
      <TabPanel value={value} index={5}>
        <Assessment id_project={id} />
      </TabPanel>
      <TabPanel value={value} index={6}>
        <TechnicalEnquiry id_project={id} />
      </TabPanel>
      <TabPanel value={value} index={7}>
        <Report id_project={id} />
      </TabPanel>
      <TabPanel value={value} index={8}>
        <Administrative id_project={id} />
      </TabPanel>
    </Box>
  );
}
