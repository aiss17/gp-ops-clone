import { forwardRef, useEffect, useRef, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
// material
import {
  Card,
  Stack,
  Button,
  Typography,
  Grid,
  Checkbox,
  Snackbar,
  FormControlLabel,
  Paper,
  List,
  ListItem,
  ListItemText
} from "@mui/material";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import PDFViewer from "pdf-viewer-reactjs";
import moment from "moment";
import {
  ChasingDots,
  Circle,
  CubeGrid,
  DoubleBounce,
  FadingCircle,
  FoldingCube,
  Pulse,
  RotatingPlane,
  ThreeBounce,
  WanderingCubes,
  Wave
} from "better-react-spinkit";
import MuiAlert from "@mui/material/Alert";

// components
import Page from "../../../components/Page";
import Iconify from "../../../components/Iconify";
//
import { DOCUMENTS } from "../../../_mocks_/user";

import filepdf from "../../../assets/PDF.pdf";
import { BMP, EXCEL, JPEG, JPG, PDF, PNG, PPT, WORD } from "src/assets/images";
import SearchNotFound from "src/components/SearchNotFound";
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { height } from "@mui/system";

import DeleteIcon from "@mui/icons-material/Delete";

// ----------------------------------------------------------------------

// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

export default function Document(props) {
  const [open, setOpen] = useState(false);
  const [disable, setDisable] = useState(false);
  const [visibleUpload, setVisibleUpload] = useState(false);
  const [alert, setAlert] = useState(false);
  const [visibleRevise, setvisibleRevise] = useState(false);
  const [loading, setLoading] = useState(false);
  const [dataDisable, setDataDisable] = useState(false);
  const [visibleSaveUpload, setVisibleSaveUpload] = useState(false);

  // state value
  const [fileShow, setFileShow] = useState("");
  const [fileType, setFileType] = useState("");
  const [fileUpload, setFileUpload] = useState([]);
  const [dataTemp, setDataTemp] = useState([]);
  const [documentTrue, setDocumentTrue] = useState([]);
  const [documentFalse, setDocumentFalse] = useState([]);
  const [key, setKey] = useState("");
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [id, setId] = useState(null);
  const [project_id, setProject_id] = useState(null);
  const [doc_title, setDoc_title] = useState("");
  const [issuer, setIssuer] = useState("");
  const [valid_until, setValid_until] = useState(null);
  const [created_at, setCreated_at] = useState(null);
  const [created_by, setCreated_by] = useState(null);
  const [size, setSize] = useState(null);
  const [file, setFile] = useState([]);
  const [dataSend, setDataSend] = useState([]);
  const [arrayIdDoc, setArrayIdDoc] = useState([]);
  const [numberRevision, setNumberRevision] = useState("");
  const [descRevise, setDescRevise] = useState("");
  const [fileRevise, setFileRevise] = useState(null);
  const [dataHistory, setDataHistory] = useState([]);
  const [source, setSource] = useState("");

  const ref = useRef();

  // const defaultLayoutPluginInstance = defaultLayoutPlugin();

  useEffect(() => {
    GetAllDocumentFalse();
    setTimeout(() => {
      GetAllDocumentTrue();
    }, 2000);
    console.log("ini props nya => ", JSON.stringify(props));

    return () => {};
  }, []);

  const handleClose = () => {
    setValid_until(null);
    setOpen(false);
    setDataDisable(false);
    setDisable(false);
  };

  const metadataMultipleUpload = async () => {
    const payload = {
      data: dataSend,
      project: props.id_project
    };

    const response = await callApi(ENDPOINT.MetadataUpload, payload, "POST");
    //setKey(response.keys);
    setKey(GetCookie("key"));
    setDataTemp([]);
    setDataSend([]);
    setFileUpload([]);
    setFile([]);
    setVisibleUpload(false);
    setVisibleSaveUpload(false);
    setDocumentFalse([]);
    setDocumentTrue([]);
    setArrayIdDoc([]);
    setValid_until(null);

    setTimeout(() => {
      GetAllDocumentFalse();
    }, 1000);
    setTimeout(() => {
      GetAllDocumentTrue();
    }, 2000);
  };

  const metadataSingleUpload = async () => {
    const formData = new FormData();

    formData.append("id", id);
    formData.append("doc_title", doc_title);
    formData.append("issuer", issuer);
    formData.append(
      "valid_until",
      valid_until === null || dataDisable
        ? null
        : moment(valid_until).format("YYYY-MM-DD")
    );
    formData.append("key", key);
    formData.append("project_id", props.id_project);

    const response = await callApi(ENDPOINT.MetadataUpload, formData, "UPLOAD");

    setId(null);
    setSource("");
    setKey(GetCookie("key"));
    setFileShow("");
    setFileType("");
    setDoc_title("");
    setIssuer("");
    setCreated_at(null);
    setDataHistory([]);
    setCreated_by(null);
    setSize(null);

    setVisibleUpload(false);
    setVisibleSaveUpload(false);
    setOpen(false);
    setDataDisable(false);
    setDisable(false);
  };

  const Deleteparent = async (id) => {
    const payload = {
      project: props.id_project
    };

    const response = await callApi(
      ENDPOINT.DeleteParrent + `/${id}`,
      payload,
      "POST"
    );

    setOpen(false);
    GetAllDocumentFalse();
    GetAllDocumentTrue();
  };

  const DeleteRevision = async (id) => {
    const payload = {
      project: props.id_project
    };

    const response = await callApi(
      ENDPOINT.DeleteRevision + `/${id}`,
      payload,
      "POST"
    );

    // setOpen(false);
    setOpen(false);
    GetAllDocumentFalse();
    GetAllDocumentTrue();
  };

  const options = ["Assessment", "Profile"];

  const onFileChange = (event) => {
    const imagesArray = [];
    for (let i = 0; i < event.target.files.length; i++) {
      imagesArray.push(event.target.files[i]);
    }
    setFile(imagesArray);
  };

  const onFileChangeRevise = (event) => {
    setFileRevise(event.target.files[0]);
  };

  const handleChange = (item) => {
    const index = dataTemp.indexOf(item.id);
    if (index > -1) {
      dataTemp.splice(index, 1);
      dataSend.splice(index, 1); // 2nd parameter means remove one item only
    } else {
      dataTemp.push(item.id);
      dataSend.push(item);
    }

    if (dataSend.length > 0) {
      setVisibleSaveUpload(true);
    } else {
      setVisibleSaveUpload(false);
    }
    console.log(dataSend);
  };

  const GetAllDocumentTrue = async () => {
    const response = await callApi(
      ENDPOINT.GetAllDocument,
      { complete: true, project: props.id_project },
      "GET"
    );

    setKey(GetCookie("key"));

    setDocumentTrue(
      response.finalResponse.data.sort((a, b) =>
        b.created_at.localeCompare(a.created_at)
      )
    );
  };

  const GetAllDocumentFalse = async () => {
    setLoading(true);
    let docFalse = [];
    let docId = [];
    const response = await callApi(
      ENDPOINT.GetAllDocument,
      { complete: false, project: props.id_project },
      "GET"
    );

    setKey(GetCookie("key"));
    for (let i = 0; i < response.finalResponse.data.length; i++) {
      let payload = {
        id: response.finalResponse.data[i].id,
        doc_title:
          response.finalResponse.data[i].doc_title === null
            ? ""
            : response.finalResponse.data[i].doc_title,
        fileName:
          response.finalResponse.data[i].ori_filename === null
            ? "-"
            : response.finalResponse.data[i].ori_filename,
        issuer:
          response.finalResponse.data[i].issuer === null
            ? ""
            : response.finalResponse.data[i].issuer,
        valid_until: response.finalResponse.data[i].valid_until,
        disabled: false,
        created_at: response.finalResponse.data[i].created_at,
        source: response.finalResponse.data[i].source
      };

      docFalse.push(payload);
    }

    setTimeout(() => {
      setDocumentFalse(
        docFalse.sort((a, b) => b.created_at.localeCompare(a.created_at))
      );
    }, 500);

    setLoading(false);
    setVisibleUpload(true);
  };

  const GetDocumentByID = async (item) => {
    const response = await callApi(
      ENDPOINT.GetAllDocument + `/${item.id}`,
      { project: props.id_project },
      "GET"
    );

    setId(item.id);
    setSource(item.source);
    setProject_id(item.project_id);
    //setKey(response.keys);
    console.log("sapi => ", GetCookie("key"));
    setKey(GetCookie("key"));

    console.log("sapi2 => ", key);
    setFileShow(response.finalResponse.data[0].path);
    setFileType(response.finalResponse.data[0].filetype);
    setDoc_title(response.finalResponse.data[0].doc_title);
    setIssuer(response.finalResponse.data[0].issuer);
    setCreated_at(
      moment(response.finalResponse.data[0].created_at).format("MMM DD, yyyy")
    );
    setDataHistory(response.finalResponse.data[0].revision);
    setCreated_by(response.finalResponse.data[0].created_by);
    setSize(response.finalResponse.data[0].size);

    if (response.finalResponse.data[0].valid_until !== null) {
      setValid_until(
        moment(response.finalResponse.data[0].valid_until).format()
      );
      setDataDisable(false);
    } else {
      setDataDisable(true);
    }

    setTimeout(() => {
      setOpen(true);
    }, 500);
  };

  const MultiUploadDocument = async () => {
    setLoading(true);
    const formData = new FormData();

    formData.append("project_id", props.id_project);
    for (let i = 0; i < file.length; i++) {
      formData.append("docs[]", file[i]);
    }
    formData.append("key", key);

    const response = await callApi(
      ENDPOINT.MultiUploadDocument,
      formData,
      "UPLOAD"
    );
    setVisibleUpload(true);
    setAlert(true);
    setFile([]);
    GetAllDocumentFalse();

    setLoading(false);
    ref.current.value = null;
  };

  const UploadRevise = async () => {
    const formData = new FormData();

    formData.append("doc_id", id);
    formData.append("project_id", project_id);
    formData.append("description", descRevise);
    formData.append("revision", numberRevision);
    formData.append("docs", fileRevise);
    formData.append("key", key);

    const response = await callApi(ENDPOINT.UploadRevise, formData, "UPLOAD");
    setDescRevise("");
    setvisibleRevise(false);
    setNumberRevision("");
    setAlert(true);
    setOpen(false);
    GetAllDocumentFalse();
    GetAllDocumentTrue();
  };

  const handleClickAlert = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setAlert(false);
  };

  const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="standard" {...props} />;
  });

  if (loading) {
    return (
      // <Page
      //   title="Project Management | My Green Port"
      //   style={{ margin: 10, justifyContent: "center", alignItems: "center" }}
      // >
      <div
        style={{
          width: "100%",
          height: "100%",
          // justifyContent: "center",
          top: "50%",
          left: "50%",
          textAlign: "center"
        }}
      >
        <ThreeBounce size={15} color="blue" />
      </div>
      // {/* </Page> */}
    );
  }
  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      {/* <Container> */}
      <Snackbar
        open={alert}
        autoHideDuration={1000}
        onClose={handleClickAlert}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          // variant="out"
          onClose={handleClickAlert}
          severity="success"
          sx={{ width: "100%" }}
        >
          Your document has been successfully uploaded!
        </Alert>
      </Snackbar>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Document Management
        </Typography>
      </Stack>

      <BootstrapDialog
        onClose={handleClose}
        // aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="xl"
        fullWidth
      >
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Attachment Details - {doc_title}
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500]
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent
          style={{ marginLeft: 15, marginRight: 15, overflow: "hidden" }}
          dividers
        >
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Card
                style={{
                  padding: 15,
                  // maxHeight: "55%",
                  height: "60%",
                  overflow: "auto",
                  textAlign: "center",
                  boxShadow: "none"
                }}
              >
                {fileShow && fileType === "pdf" && (
                  <PDFViewer
                    navbarOnTop={true}
                    document={{
                      url: `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${fileShow}&act=view&key=${key}`,
                    }}
                  />
                )}
                {fileShow &&
                  (fileType === "jpg" ||
                    fileType === "jpeg" ||
                    fileType === "png") && (
                    <img
                      src={`${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${fileShow}&act=view&key=${key}`}
                      loading="lazy"
                      style={{
                        justifyContent: "center",
                        alignSelf: "center"
                      }}
                    />
                  )}

                {fileShow &&
                  (fileType === "doc" ||
                    fileType === "docx" ||
                    fileType === "xls" ||
                    fileType === "xlsx" ||
                    fileType === "ppt" ||
                    fileType === "pptx") && (
                    <div
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        display: "flex",
                        height: "100%",
                        width: "100%"
                      }}
                    >
                      <img
                        src={
                          fileType === "doc" || fileType === "docx"
                            ? WORD
                            : fileType === "xls" || fileType === "xlsx"
                            ? EXCEL
                            : PPT
                        }
                        // alt={item.title}
                        loading="lazy"
                        // style="max-height: 100px; max-width: 100px;"
                        style={{
                          maxHeight: 300,
                          maxWidth: 300,
                          justifyContent: "center",
                          alignSelf: "center"
                        }}
                      />
                    </div>
                  )}
              </Card>
            </Grid>
            <Grid
              item
              xs={3}
              style={{
                border: "1",
                borderStyle: "none none none solid",
                borderColor: "#E5E8EB"
              }}
            >
              <Card
                style={{
                  padding: 5,
                  maxHeight: "78%",
                  overflow: "auto",
                  border: "none",
                  boxShadow: "none",
                  fontSize: 12
                }}
              >
                {/* <hr style={{ marginTop: 0, marginBottom: 20 }} /> */}
                <p>
                  <b>Uploaded on:</b>{" "}
                  <span style={{ color: "#637381" }}>{created_at}</span>
                </p>
                <p>
                  <b>Uploaded by:</b>{" "}
                  <span style={{ color: "#637381" }}>{created_by}</span>
                </p>
                <p>
                  <b>File name:</b>{" "}
                  <span style={{ color: "#637381" }}>{doc_title}</span>
                </p>
                <p>
                  <b>File type:</b>{" "}
                  <span style={{ color: "#637381" }}>{fileType}</span>
                </p>
                <p>
                  <b>File size:</b>{" "}
                  <span style={{ color: "#637381" }}>{size + " Bytes"}</span>
                </p>

                <hr style={{ marginTop: 20, marginBottom: 20 }} />

                <TextField
                  required
                  autoFocus
                  margin="normal"
                  id="document_name"
                  label="Document Name"
                  type="text"
                  fullWidth
                  variant="outlined"
                  disabled={source !== "ops"}
                  value={doc_title}
                  onChange={(e) => setDoc_title(e.target.value)}
                  size="small"
                />

                <TextField
                  required
                  autoFocus
                  margin="normal"
                  id="issuer"
                  label="Issuer"
                  type="text"
                  fullWidth
                  variant="outlined"
                  disabled={source !== "ops"}
                  value={issuer}
                  onChange={(e) => setIssuer(e.target.value)}
                  size="small"
                />
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DatePicker
                    inputFormat="yyyy/MM/dd"
                    disabled={dataDisable || source !== "ops"}
                    label="Valid Until"
                    value={valid_until}
                    onChange={(newValue) => {
                      setValid_until(newValue);
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        margin="normal"
                        required
                        fullWidth
                        // disabled={disable}
                        size="small"
                      />
                    )}
                  />
                </LocalizationProvider>
                <FormControlLabel
                  control={
                    <Checkbox
                      disabled={source !== "ops"}
                      size="small"
                      checked={dataDisable}
                      onChange={() => {
                        if (dataDisable) {
                          setDataDisable(false);
                        } else {
                          setDataDisable(true);
                        }
                      }}
                    />
                  }
                  label={
                    <p style={{ fontSize: 12 }}>
                      This document has no expiration date
                    </p>
                  }
                />

                <Autocomplete
                  disabled={source !== "ops"}
                  value={valueAuto}
                  onChange={(event, newValue) => {
                    setValueAuto(newValue);
                  }}
                  inputValue={inputValueAuto}
                  onInputChange={(event, newInputValue) => {
                    setInputValueAuto(newInputValue);
                  }}
                  id="controllable-states-demo"
                  options={options}
                  // sx={{ width: 300 }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Type"
                      margin="normal"
                      required
                      fullWidth
                    />
                  )}
                  size="small"
                />

                <hr style={{ marginTop: 20, marginBottom: 20 }} />

                {source === "ops" && (
                  <Button
                    variant={visibleRevise ? "outlined" : "contained"}
                    color={visibleRevise ? "error" : "primary"}
                    onClick={() => setvisibleRevise(!visibleRevise)}
                  >
                    {visibleRevise ? "Cancel" : "Update Document"}
                  </Button>
                )}

                {visibleRevise && (
                  <>
                    <p style={{ textAlign: "center", marginTop: 20 }}>
                      <b>FORM DOCUMENT REVISION</b>
                    </p>

                    <TextField
                      required
                      autoFocus
                      margin="normal"
                      id="description"
                      label="Description"
                      type="text"
                      fullWidth
                      variant="outlined"
                      disabled={disable}
                      value={descRevise}
                      onChange={(e) => setDescRevise(e.target.value)}
                    />

                    <div style={{ marginTop: 15 }}>
                      <label
                        htmlFor="upload-photo"
                        style={{ color: "#667480" }}
                      >
                        Revision Documents
                      </label>
                      <br />
                      <input
                        type="file"
                        style={{ color: "#667480", marginTop: 10 }}
                        onChange={(e) => onFileChangeRevise(e)}
                      />
                    </div>
                    <Button
                      style={{ marginTop: 30, width: "100%" }}
                      variant="contained"
                      onClick={UploadRevise}
                    >
                      Upload Revision
                    </Button>
                  </>
                )}

                <fieldset
                  style={{
                    border: "1px solid silver",
                    padding: "5px",
                    overflow: "auto",
                    maxHeight: "320px",
                    marginTop: 30
                  }}
                >
                  <legend style={{ color: "black", fontSize: 17 }}>
                    Revision History
                  </legend>
                  <div>
                    <Paper style={{ maxHeight: 220, overflow: "auto" }}>
                      <List
                        style={{
                          height: "70vh",
                          overflowY: "auto",
                          fontSize: "5px"
                        }}
                      >
                        {dataHistory.map((value) => {
                          return (
                            <ListItem
                              style={{
                                width: "100%",
                                color: "#1948A5"
                              }}
                            >
                              <ListItemText
                                primary={
                                  <Grid
                                    container
                                    spacing={1}
                                    // style={{ backgroundColor: "orange" }}
                                  >
                                    <Grid item xs={10}>
                                      <Typography
                                        onClick={() =>
                                          window.open(
                                            `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${value.path}&act=view&key=${key}`
                                          )
                                        }
                                        variant="h6"
                                        gutterBottom
                                        component="div"
                                        style={{
                                          textAlign: "left",
                                          color: "#00AF5B",
                                          fontSize: "14px",
                                          cursor: "pointer"
                                        }}
                                      >
                                        {`Revision ${value.revision}`}
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={2}>
                                      {source === "ops" && (
                                        <IconButton
                                          style={{
                                            marginTop: "-5px",
                                            cursor: "pointer"
                                          }}
                                          onClick={() => {
                                            if (
                                              window.confirm(
                                                "Are you sure you want to delete this revision?"
                                              )
                                            ) {
                                              DeleteRevision(value.id);
                                              console.log("success");
                                            } else {
                                              // cancel
                                              console.log("cancel");
                                            }
                                          }}
                                        >
                                          <Iconify
                                            icon="eva:trash-fill"
                                            width={15}
                                            height={15}
                                          />
                                        </IconButton>
                                      )}
                                    </Grid>
                                  </Grid>
                                }
                                secondary={
                                  <>
                                    <Typography
                                      variant="p"
                                      gutterBottom
                                      component="div"
                                      style={{
                                        textAlign: "left",
                                        color: "silver",
                                        fontSize: "12px",
                                        marginTop: -10
                                      }}
                                    >
                                      {value.description}
                                    </Typography>
                                    <Typography
                                      variant="p"
                                      gutterBottom
                                      component="div"
                                      style={{
                                        textAlign: "left",
                                        color: "silver",
                                        fontSize: "12px",
                                        marginTop: -10
                                      }}
                                    >
                                      {`Issued Date: ${moment(
                                        value.created_at
                                      ).format("DDMMMYY")}`}
                                    </Typography>
                                  </>
                                }
                                style={{
                                  color: "#1948A5"
                                }}
                              />
                            </ListItem>
                          );
                        })}
                      </List>
                    </Paper>
                  </div>
                </fieldset>
              </Card>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions style={{ justifyContent: "space-between" }}>
          {/* <Button autoFocus onClick={handleClose}>
              Save changes
            </Button> */}
          <div>
            <Button
              onClick={() =>
                window.open(
                  `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${fileShow}&act=download&key=${key}`
                )
              }
              variant="contained"
              color="info"
              component={RouterLink}
              to="#"
              startIcon={<Iconify icon="bi:download" />}
              style={{
                // backgroundColor: "#164477",
                boxShadow: "none",
                marginLeft: 20,
                marginTop: 10,
                marginBottom: 10
              }}
            >
              Download File
            </Button>

            {source === "ops" && (
              <Button
                onClick={() => {
                  if (
                    window.confirm(
                      "Are you sure you want to delete this document?"
                    )
                  ) {
                    Deleteparent(id);
                    console.log("success");
                  } else {
                    // cancel
                    console.log("cancel");
                  }
                }}
                variant="contained"
                color="error"
                component={RouterLink}
                to="#"
                startIcon={<Iconify icon="eva:trash-fill" />}
                style={{
                  // backgroundColor: "#164477",
                  boxShadow: "none",
                  marginLeft: 20,
                  marginTop: 10,
                  marginBottom: 10
                }}
              >
                Delete
              </Button>
            )}
          </div>

          {source === "ops" && (
            <Button
              onClick={metadataSingleUpload}
              variant="contained"
              component={RouterLink}
              to="#"
              startIcon={<Iconify icon="eva:save-fill" />}
              style={{
                // backgroundColor: "#164477",
                boxShadow: "none",
                marginRight: 20,
                marginTop: 10,
                marginBottom: 10
              }}
            >
              Save changes
            </Button>
          )}
        </DialogActions>
      </BootstrapDialog>

      <Card style={{ borderRadius: 5 }}>
        <div style={{ padding: 15 }}>
          <label
            htmlFor="upload-photo"
            style={{ color: "#667480", fontSize: "12px" }}
          >
            Upload Documents
          </label>
          <br />
          <input
            type="file"
            style={{ color: "#667480" }}
            multiple
            ref={ref}
            onChange={(e) => onFileChange(e)}
          />
          {/* </div> */}
          {/* <div> */}
          <Button
            disabled={file.length < 1}
            variant="contained"
            onClick={MultiUploadDocument}
          >
            Upload
          </Button>
          {/* </div> */}
        </div>
        <div
          style={{
            width: "100%",
            padding: 20,
            marginBottom: 20,
            height: "500px",
            overflow: "auto",
            fontSize: "12px"
          }}
        >
          <table
            style={{
              fontFamily: "arial, sans-serif",
              borderCollapse: "collapse",
              width: "100%"
            }}
          >
            <tr style={{ backgroundColor: "#254A7C" }}>
              <th style={{ border: "1px solid #dddddd", width: "2%" }} />
              <th
                style={{
                  border: "1px solid #dddddd",
                  textAlign: "left",
                  padding: "8px",
                  width: "10%",
                  color: "#ffffff"
                }}
              >
                Filename
              </th>
              <th
                style={{
                  border: "1px solid #dddddd",
                  textAlign: "left",
                  padding: "8px",
                  width: "20%",
                  color: "#ffffff"
                }}
              >
                Document Name
              </th>
              <th
                style={{
                  border: "1px solid #dddddd",
                  textAlign: "left",
                  padding: "8px",
                  width: "20%",
                  color: "#ffffff"
                }}
              >
                Issuer
              </th>
              <th
                style={{
                  border: "1px solid #dddddd",
                  textAlign: "left",
                  padding: "8px",
                  width: "20%",
                  color: "#ffffff"
                }}
              >
                Valid Until
              </th>
            </tr>
            {documentFalse !== [] &&
              documentFalse.map((item, index) => {
                const idDoc = documentFalse[index].id;
                return (
                  <tr>
                    <td
                      style={{
                        border: "1px solid #dddddd",
                        textAlign: "left",
                        padding: "8px"
                      }}
                    >
                      {documentFalse[index].doc_title !== "" &&
                        (documentFalse[index].disabled !== false ||
                          documentFalse[index].valid_until !== null) && (
                          <Checkbox
                            checked={dataTemp.find((x) => x.id === item.id)}
                            onChange={() => handleChange(item)}
                            size="small"
                          />
                        )}
                    </td>
                    <td
                      style={{
                        border: "1px solid #dddddd",
                        textAlign: "left",
                        padding: "25px 8px 8px 8px"
                      }}
                    >
                      {item.fileName === null || item.fileName === ""
                        ? "-"
                        : item.fileName}{" "}
                      {item.source === "ops" && (
                        <IconButton aria-label="delete" size="small">
                          <DeleteIcon
                            variant="text"
                            size="small"
                            startIcon={<Iconify icon="eva:trash-fill" />}
                            style={{
                              color: "red",
                              backgroundColor: "none",
                              boxShadow: "none"
                              // "&:hover": { color: "red" },
                            }}
                            onClick={() => {
                              // Deleteparent(item.id);
                              if (
                                window.confirm(
                                  "Are you sure you want to delete this document?"
                                )
                              ) {
                                Deleteparent(item.id);
                                console.log("success");
                              } else {
                                // cancel
                                console.log("cancel");
                              }
                            }}
                          />
                        </IconButton>
                      )}
                    </td>
                    <td
                      style={{
                        border: "1px solid #dddddd",
                        textAlign: "left",
                        padding: "8px"
                      }}
                    >
                      <TextField
                        placeholder="Document Name *"
                        required
                        autoFocus
                        margin="normal"
                        id="document_name"
                        // label="Document Name"
                        type="text"
                        fullWidth
                        variant="outlined"
                        disabled={disable}
                        value={documentFalse[index].doc_title}
                        onChange={(e) => {
                          const objIndex = documentFalse.findIndex(
                            (obj) => obj.id === idDoc
                          );

                          console.log(documentFalse[index].id + " || " + idDoc);

                          // make new object of updated object.
                          const updatedObj = {
                            ...documentFalse[objIndex],
                            doc_title: e.target.value
                          };

                          // make final new array of objects by combining updated object.
                          const updatedProjects = [
                            ...documentFalse.slice(0, objIndex),
                            updatedObj,
                            ...documentFalse.slice(objIndex + 1)
                          ];

                          setDocumentFalse(updatedProjects);
                        }}
                        size="small"
                        style={{ maxHeight: "10px" }}
                      />
                      {/* {(documentFalse[index].doc_title === "" ||
                        documentFalse[index].doc_title === null) && (
                        <p style={{ fontSize: 10, color: "red" }}>
                          * Document name is required{" "}
                        </p>
                      )} */}
                    </td>
                    <td
                      style={{
                        border: "1px solid #dddddd",
                        textAlign: "left",
                        padding: "8px"
                      }}
                    >
                      <TextField
                        placeholder="Issuer"
                        required
                        autoFocus
                        margin="normal"
                        id="issuer"
                        // label="Document Name"
                        type="text"
                        fullWidth
                        variant="outlined"
                        disabled={disable}
                        value={documentFalse[index].issuer}
                        onChange={(e) => {
                          const objIndex = documentFalse.findIndex(
                            (obj) => obj.id === idDoc
                          );

                          // make new object of updated object.
                          const updatedObj = {
                            ...documentFalse[objIndex],
                            issuer: e.target.value
                          };

                          // make final new array of objects by combining updated object.
                          const updatedProjects = [
                            ...documentFalse.slice(0, objIndex),
                            updatedObj,
                            ...documentFalse.slice(objIndex + 1)
                          ];

                          setDocumentFalse(updatedProjects);
                        }}
                        size="small"
                      />
                    </td>
                    <td
                      style={{
                        border: "1px solid #dddddd",
                        textAlign: "left",
                        padding: "8px"
                      }}
                    >
                      {/* {moment(item.valid_until).format("MM/DD/YYYY")} */}
                      <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DatePicker
                          disabled={documentFalse[index].disabled}
                          label="Valid Until"
                          value={documentFalse[index].valid_until}
                          onChange={(newValue) => {
                            //find the index of object from array that you want to update
                            const objIndex = documentFalse.findIndex(
                              (obj) => obj.id === idDoc
                            );

                            // make new object of updated object.
                            const updatedObj = {
                              ...documentFalse[objIndex],
                              valid_until: newValue
                            };

                            // make final new array of objects by combining updated object.
                            const updatedProjects = [
                              ...documentFalse.slice(0, objIndex),
                              updatedObj,
                              ...documentFalse.slice(objIndex + 1)
                            ];

                            setDocumentFalse(updatedProjects);
                          }}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              margin="normal"
                              required
                              fullWidth
                              disabled={disable}
                              size="small"
                            />
                          )}
                        />
                      </LocalizationProvider>
                      <FormControlLabel
                        control={
                          <Checkbox
                            size="small"
                            checked={documentFalse[index].disabled}
                            onChange={() => {
                              const objIndex = documentFalse.findIndex(
                                (obj) => obj.id === idDoc
                              );

                              // make new object of updated object.
                              const updatedObj = {
                                ...documentFalse[objIndex],
                                disabled: !documentFalse[index].disabled
                              };

                              // make final new array of objects by combining updated object.
                              const updatedProjects = [
                                ...documentFalse.slice(0, objIndex),
                                updatedObj,
                                ...documentFalse.slice(objIndex + 1)
                              ];

                              setDocumentFalse(updatedProjects);
                            }}
                          />
                        }
                        label={
                          <p style={{ fontSize: 10 }}>
                            This document has no expiration date
                          </p>
                        }
                      />
                      {/* {documentFalse[index].disabled === false &&
                        documentFalse[index].valid_until === null && (
                          <p style={{ fontSize: 10, color: "red" }}>
                            * Valid until is required{" "}
                          </p>
                        )} */}
                    </td>
                    {/* <td
                      style={{
                        border: "1px solid #dddddd",
                        padding: "8px",
                        textAlign: "center",
                      }}
                    ></td> */}
                  </tr>
                );
              })}
          </table>

          {/* {((!visibleUpload && documentFalse.length < 1) ||
            (!visibleUpload && documentFalse.length > 0)) && ( */}
          {documentFalse.length < 1 && (
            <div
              style={{
                width: "100%",
                padding: 10,
                textAlign: "center",
                border: "1px solid #dddddd"
              }}
            >
              There is no file
            </div>
          )}

          {/* <Button
            onClick={() => {
              console.log("before ", arrayIdDoc);
              GetAllDocumentFalse();
              arrayIdDoc.splice(0, arrayIdDoc.length);
              console.log("after ", arrayIdDoc);
            }}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            // disabled={!visibleSaveUpload}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10
            }}
          >
            Refresh
          </Button> */}
        </div>
        <Button
          onClick={() => {
            metadataMultipleUpload();
          }}
          variant="contained"
          component={RouterLink}
          to="#"
          startIcon={<Iconify icon="eva:save-fill" />}
          disabled={!visibleSaveUpload}
          style={{
            // backgroundColor: "#164477",
            boxShadow: "none",
            marginLeft: 20,
            marginTop: "-50px",
            marginBottom: "-20px"
          }}
        >
          Save changes
        </Button>
        <hr />
        <h3 style={{ marginLeft: "25px" }}>
          <strong>List of Documents</strong>
        </h3>
        <ImageList
          cols={8}
          rowHeight={120}
          style={{ maxHeight: "500px", overflow: "auto", margin: 20 }}
        >
          {documentTrue.map((item, index) => (
            <ImageListItem
              key={item.img}
              // onClick={() => {
              //   setOpen(true);
              //   setFileShow(item.path);
              //   setFileType(item.filetype);
              // }}
              style={{
                justifyContent: "center",
                border: "0.5px solid",
                margin: 5,
                cursor: "pointer"
              }}
              onClick={() => GetDocumentByID(item)}
            >
              <img
                // src={`${item.img}?w=248&fit=crop&auto=format`}
                // srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
                src={
                  item.filetype === "pdf"
                    ? PDF
                    : item.filetype === "doc" || item.filetype === "docx"
                    ? WORD
                    : item.filetype === "xls" || item.filetype === "xlsx"
                    ? EXCEL
                    : item.filetype === "ppt" || item.filetype === "pptx"
                    ? PPT
                    : item.filetype === "jpg"
                    ? `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=view&key=${key}`
                    : item.filetype === "jpeg"
                    ? `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=view&key=${key}`
                    : item.filetype === "png"
                    ? `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=view&key=${key}`
                    : `${process.env.REACT_APP_BASURL_TRX}documents/fetch?path=${item.path}&act=view&key=${key}`
                }
                alt={item.title}
                loading="lazy"
                style={{
                  maxHeight: 300,
                  maxWidth: 300,
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              />
              <ImageListItemBar
                subtitle={
                  item.doc_title
                    ? item.doc_title + `.${item.filetype}`
                    : "Title unknown"
                }
                // subtitle={item.doc_title ? "" : "Title unknown"}
              />
            </ImageListItem>
          ))}
        </ImageList>
      </Card>
      {/* </Container> */}
    </Page>
  );
}
