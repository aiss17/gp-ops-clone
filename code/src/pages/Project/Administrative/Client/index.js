import * as React from "react";
import { filter } from "lodash";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { Link as RouterLink } from "react-router-dom";
import { useEffect, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { styled } from "@mui/material/styles";
// Autocomplete
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
//Modal / Dialog
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

//Icon
import Iconify from "../../../../components/Iconify";

//Checkbox
import FormGroup from "@mui/material/FormGroup";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  // DialogTitle,
  IconButton,
  // DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  // DialogActions,
  Radio,
  // Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab,
} from "@mui/material";

// Table
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import TablePagination from "@mui/material/TablePagination";
// table
import TableCell, { tableCellClasses } from "@mui/material/TableCell";

import Scrollbar from "../../../../components/Scrollbar";
import SearchNotFound from "../../../../components/SearchNotFound";

// api
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { LoadingData } from "src/pages";

// ==========================================================================================================================================================

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#164477",
    color: "#ffffff",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const userList = [
  { user_id: 1, name: "Antonio Rudiger" },
  { user_id: 2, name: "Radja Nainggolan" },
  { user_id: 3, name: "Juan Cudrado" },
  { user_id: 4, name: "Christian Pulisic" },
  { user_id: 5, name: "Vicenzo Montella" },
  { user_id: 6, name: "El Sharawi" },
  { user_id: 7, name: "Dafor Suker" },
];
const userStatus = ["Owner", "Technical", "Administrator", "Assessor", "etc."];
const groupList = ["Group A", "Group B", "Group C", "Group D", "Group E"];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Client(props) {
  const [disable, setDisable] = useState(false);
  const [filterName, setFilterName] = useState("");
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("order_number");
  const [listClient, setListClient] = useState([]);
  const [listClientName, setListClientName] = useState([]);
  // default set variabel input
  const [key, setKey] = useState("");
  const [clientId, setClientId] = useState(null);
  const [clientName, setClientName] = useState("");
  const [status, setStatus] = useState("");
  const [group, setGroup] = useState("");
  const [loading, setLoading] = useState(false);

  //Dialog
  const [open, setOpen] = React.useState(false);

  // Autocomplete (ComboBox)
  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  // set variabel input
  const [userid, setUserID] = useState([]);
  const [projectid, setProjectid] = useState(props.id_project);
  const [data, setData] = useState([]);

  useEffect(() => {
    console.log(data);
    GetAllClient();
    return () => {};
  }, []);

  const GetAllClient = async () => {
    setLoading(true);
    const payload = {
      project: props.id_project,
    };

    const response = await callApi(ENDPOINT.GetAllClient, payload, "GET");
    setKey(GetCookie("key"));
    setData(response.finalResponse.data);
    setClientName("");
    setClientId(null);
    setStatus("");
    setGroup("");
    GetAllUsers();
  };

  const GetAllUsers = async () => {
    let clientNames = [];
    const payload = {
      app: process.env.REACT_APP_CLIENT,
      project_id: props.id_project,
    };

    const response = await callApi(ENDPOINT.GetClientMyGp, payload, "GET");
    //console.log("sapi",response);
    setListClient(response.finalResponse.data);
    if (response.finalResponse.data.length > 0) {
      for (let i = 0; i < response.finalResponse.data.length; i++) {
        clientNames.push(response.finalResponse.data[i].nama);
      }

      setListClientName(clientNames);
    }
    setLoading(false);
  };

  const DeleteClient = async (id) => {
    const payload = {
      project: props.id_project,
    };

    const response = await callApi(
      ENDPOINT.DeleteClient + `/${id}`,
      payload,
      "POST"
    );

    GetAllClient();
  };

  const AddClient = async () => {
    const payload = {
      project_id: props.id_project,
      client_id: clientId,
      client_name: clientName,
      group: group,
      status: status,
    };

    console.log("Payload Add Client => ", JSON.stringify(payload));

    const response = await callApi(ENDPOINT.GetAllClient, payload, "POST");

    GetAllClient();
  };

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClientName = (value) => {
    let obj = listClient.find((o) => o.nama === value);

    console.log(obj);
    setClientId(obj.id_user);
    setClientName(value);
  };

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Card>
      {/* Modal Update User */}
      <Box
        sx={{
          width: "auto",
          maxWidth: "100%",
          margin: 2,
        }}
      >
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <h5 style={{ marginTop: 15, textAlign: "right" }}>Client</h5>
          </Grid>
          <Grid item xs={7}>
            {/* <Autocomplete
              id="controllable-states-demo"
              options={listClient}
              getOptionLabel={(option) => option.nama}
              // value={clientName === "" ? "" : clientName}
              onChange={(event, value) => {
                console.log(value.user_id);
                setClientId(value.id_user);
                setClientName(value.nama);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  margin="normal"
                  required
                  size="small"
                  placeholder="-- Select Client --"
                  style={{ fontSize: 12, paddingTop: 20 }}
                />
              )}
              // onSelect={() => console.log(JSON.stringify(row))}
              style={{
                marginTop: "-30px",
                fontSize: 12
              }}
            /> */}
            <Autocomplete
              id="controllable-states-demo"
              options={listClientName}
              value={clientName}
              onChange={(e, val) => {
                console.log(val);
                handleClientName(val);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  margin="normal"
                  required
                  size="small"
                  placeholder="-- Select Client --"
                  style={{ fontSize: 12, paddingTop: 20 }}
                />
              )}
              // onSelect={() => console.log(JSON.stringify(row))}
              style={{
                marginTop: "-30px",
                fontSize: 12,
              }}
            />
          </Grid>
          <Grid item xs={3}>
            <h5 style={{ marginTop: "-15px", textAlign: "right" }}>Status</h5>
          </Grid>
          <Grid item xs={7}>
            <Autocomplete
              id="controllable-states-demo"
              options={userStatus}
              value={status}
              onChange={(e, val) => {
                console.log(val);
                setStatus(val);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  margin="normal"
                  required
                  size="small"
                  placeholder="-- Select Status --"
                  style={{ fontSize: 12, paddingTop: 10 }}
                  sixe="small"
                />
              )}
              // onSelect={() => console.log(JSON.stringify(row))}
              style={{
                marginTop: "-50px",
                fontSize: 12,
              }}
            />
          </Grid>
          <Grid item xs={3}>
            <h5 style={{ marginTop: "-5px", textAlign: "right" }}>
              Group Team
            </h5>
          </Grid>
          <Grid item xs={7}>
            <Autocomplete
              id="controllable-states-demo"
              options={groupList}
              value={group}
              onChange={(e, val) => {
                console.log(val);
                setGroup(val);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  // label="Type"
                  margin="normal"
                  // required
                  size="small"
                  placeholder="-- Select Group--"
                  style={{ fontSize: 12, paddingTop: 20 }}
                  sixe="small"
                />
              )}
              // onSelect={() => console.log(JSON.stringify(row))}
              style={{
                marginTop: "-50px",
                fontSize: 12,
              }}
            />
          </Grid>
        </Grid>
        <Grid item xs={10}>
          <Button
            onClick={() => {
              AddClient();
            }}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:plus-fill" />}
            style={{
              backgroundColor: "#248bf5",
              boxShadow: "none",
              marginTop: 10,
              marginBottom: 10,
              float: "right",
            }}
          >
            Add User
          </Button>
        </Grid>

        <br />
        <Paper sx={{ width: "100%", overflow: "hidden" }}>
          <hr style={{ marginTop: 5 }} />
          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <StyledTableCell style={{ textAlign: "center" }}>
                      User ID
                    </StyledTableCell>
                    <StyledTableCell style={{ textAlign: "center" }}>
                      Name
                    </StyledTableCell>
                    <StyledTableCell
                      style={{
                        backgroundColor: "#164477",
                        textAlign: "center",
                      }}
                    >
                      Action
                    </StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      //   const { id, name, role, status, company, avatarUrl, isVerified } = row;
                      //   const isItemSelected = selected.indexOf(name) !== -1;
                      const { id, client_id, client_name } = row;

                      return (
                        <TableRow>
                          <TableCell style={{ textAlign: "center" }}>
                            {client_id}
                          </TableCell>
                          <TableCell align="left">{client_name}</TableCell>
                          <TableCell
                            style={{
                              textAlign: "center",
                            }}
                          >
                            <Button
                              variant="contained"
                              onClick={() => {
                                if (
                                  window.confirm(
                                    "Are you sure you want to delete this Client?"
                                  )
                                ) {
                                  DeleteClient(id);
                                  console.log("success");
                                } else {
                                  // cancel
                                  console.log("cancel");
                                }
                              }}
                              startIcon={<Iconify icon="eva:trash-fill" />}
                              style={{
                                backgroundColor: "#254A7C",
                                boxShadow: "none",
                              }}
                            >
                              Delete
                            </Button>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isUserNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
        <hr style={{ marginTop: 5 }} />
      </Box>
    </Card>
  );
}
