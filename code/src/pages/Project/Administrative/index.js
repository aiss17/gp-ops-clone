import * as React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Article from "@mui/icons-material/Article";
import VideoCameraFrontIcon from "@mui/icons-material/VideoCameraFront";
import SummarizeIcon from "@mui/icons-material/Summarize";
import QuizIcon from "@mui/icons-material/Quiz";
import FactCheckIcon from "@mui/icons-material/FactCheck";

// pages
import { Assessment, Report, TechnicalEnquiry } from "../index";

// pages
import Administrative from "../Administrative/Administrative";
import TeamSetup from "../Administrative/TeamSetup";
import Client from "../Administrative/Client";
import PG from "../Assessment/PG";
import TP from "../Assessment/TP";
import KU from "../Assessment/KU";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`
  };
}

export default function BasicTabs(props) {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box style={{ clear: "both" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
          variant="scrollable"
          scrollButtons={false}
          style={{ marginTop: "-15px" }}
        >
          <Tab
            label="Administrative"
            {...a11yProps(0)}
            style={{ color: "#254A7C" }}
          />
          <Tab label="Team Setup" {...a11yProps(1)} />
          <Tab label="Client" {...a11yProps(2)} />
          {/* <Tab label="TP" {...a11yProps(3)} />
          <Tab label="KU" {...a11yProps(4)} /> */}
        </Tabs>
      </Box>
      <TabPanel
        value={value}
        index={0}
        style={{ marginLeft: "-25px", marginRight: "-25px" }}
      >
        <Administrative id_project={props.id_project} />
      </TabPanel>
      <TabPanel
        value={value}
        index={1}
        style={{ marginLeft: "-25px", marginRight: "-25px" }}
      >
        <TeamSetup id_project={props.id_project} />
      </TabPanel>
      <TabPanel
        value={value}
        index={2}
        style={{ marginLeft: "-25px", marginRight: "-25px" }}
      >
        <Client id_project={props.id_project} />
      </TabPanel>
      {/* <TabPanel value={value} index={3}>
        <TP />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <KU />
      </TabPanel> */}
    </Box>
  );
}
