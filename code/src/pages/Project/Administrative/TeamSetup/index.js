import * as React from "react";
import { filter } from "lodash";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { Link as RouterLink } from "react-router-dom";
import { useEffect, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { styled } from "@mui/material/styles";
// Autocomplete
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
//Modal / Dialog
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

//Icon
import Iconify from "../../../../components/Iconify";

//Checkbox
import FormGroup from "@mui/material/FormGroup";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  // DialogTitle,
  IconButton,
  // DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  // DialogActions,
  Radio,
  // Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab,
  Select,
  MenuItem,
  InputLabel,
} from "@mui/material";

// Table
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import TablePagination from "@mui/material/TablePagination";
// table
import TableCell, { tableCellClasses } from "@mui/material/TableCell";

import Scrollbar from "../../../../components/Scrollbar";
import SearchNotFound from "../../../../components/SearchNotFound";

// api
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { LoadingData } from "src/pages";

// ==========================================================================================================================================================

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#00AF5B",
    color: "#ffffff",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

//bootstrapDialog
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const columns = [
  { id: "photo", label: "Photo", minWidth: 80 },
  { id: "name", label: "Full Name", minWidth: 170 },
];

function createData(name, code, population, size) {
  const density = population / size;
  const photo = [
    <Box
      component="img"
      sx={{
        height: 50,
        width: 50,
        // maxHeight: { xs: 233, md: 167 },
        // maxWidth: { xs: 350, md: 250 },
        borderRadius: "50%",
      }}
      src="https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YXZhdGFyfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60"
    />,
  ];
  return { photo, name, code, population, size, density };
}

const userList = [
  { user_id: 1, name: "Antonio Rudiger" },
  { user_id: 2, name: "Radja Nainggolan" },
  { user_id: 3, name: "Juan Cudrado" },
  { user_id: 4, name: "Christian Pulisic" },
  { user_id: 5, name: "Vicenzo Montella" },
  { user_id: 6, name: "El Sharawi" },
  { user_id: 7, name: "Dafor Suker" },
];

// Team Role
const roles_type = [
  {
    id: 1,
    name: "Set as Project Leader",
  },
  {
    id: 2,
    name: "Set as Supervisio",
  },
  {
    id: 3,
    name: "Set as Manager",
  },
  {
    id: 4,
    name: "Set as Finance Officer",
  },
  {
    id: 5,
    name: "Set as Administrator",
  },
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user.order_number.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function TeamSetup(props) {
  const [disable, setDisable] = useState(false);
  const [filterName, setFilterName] = useState("");
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("order_number");

  // default set variabel input
  const [key, setKey] = useState("");

  // Autocomplete (ComboBox)
  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  // set variabel input
  const [userid, setUserID] = useState([]);
  const [dataproject, setDataproject] = useState("");

  const [projectid, setProjectid] = useState("");
  const [data, setData] = useState([]);
  const [dataalluser, setListuser] = useState([]);
  const [user_id, setuseridsingle] = useState("");
  const [nama_user, setNamauser] = useState("");
  const [roles, setRoles] = useState({});
  const [loading, setLoading] = useState(false);

  // setDataproject(response.finalResponse.data[0]);

  useEffect(() => {
    console.log(data);
    GetAllTeams();
    return () => {};
  }, []);

  const GetAllTeams = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetAllTeams,
      { project: props.id_project },
      "GET"
    );
    setKey(GetCookie("key"));
    console.log(response);
    setData(response.finalResponse.data);
    GetAllUsers();
  };

  const GetAllUsers = async () => {
    const payload = {
      app: process.env.REACT_APP_TEAM,
      project_id: props.id_project,
    };

    const response = await callApi(ENDPOINT.GetAllusers, payload, "GET");
    //console.log("sapi",response);
    setListuser(response.finalResponse.data);
    setLoading(false);
  };

  const DellprojectTeam = async (item) => {
    const response = await callApi(
      ENDPOINT.DeluserIdproj + `/${item}`,
      { project: props.id_project },
      "POST"
    );
    GetAllTeams();
  };

  const InsertTeams = async () => {
    setKey(GetCookie("key"));

    const payload = {
      key: key,
      user_id: user_id,
      project_id: props.id_project,
      user_name: nama_user,
    };
    // console.log(payload);

    const response = await callApi(ENDPOINT.GetAllTeams, payload, "POST");
    console.log("sapiii", response);
    setUserID("");
    GetAllTeams();
  };

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  //Dialog
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Card>
      {/* Modal Update User */}
      <BootstrapDialog
        // onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          // onClose={handleClose}
        >
          Permission Setting
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          {/* <h4>
            <strong>Position</strong>
          </h4>
          <FormControl>
            <RadioGroup
              row
              aria-labelledby="demo-radio-buttons-group-label"
              defaultValue="female"
              name="radio-buttons-group"
              size="small"
            >
              <FormControlLabel
                value="pos1"
                control={<Radio />}
                label="Position 1"
              />
              <FormControlLabel
                value="pos2"
                control={<Radio />}
                label="Position 2"
              />
              <FormControlLabel
                value="pos3"
                control={<Radio />}
                label="Position 3"
              />
              <FormControlLabel
                value="pos4"
                control={<Radio />}
                label="Position 4"
              />
              <FormControlLabel
                value="pos5"
                control={<Radio />}
                label="Position 5"
              />
              <FormControlLabel
                value="pos6"
                control={<Radio />}
                label="Position 6"
              />
              <FormControlLabel
                value="pos7"
                control={<Radio />}
                label="Position 7"
              />
              <FormControlLabel
                value="pos8"
                control={<Radio />}
                label="Position 8"
              />
              <FormControlLabel
                value="pos9"
                control={<Radio />}
                label="Position 9"
              />
              <FormControlLabel
                value="pos10"
                control={<Radio />}
                label="Position 10"
              />
              <FormControlLabel
                value="pos11"
                control={<Radio />}
                label="Position 11"
              />
              <FormControlLabel
                value="pos12"
                control={<Radio />}
                label="Position 12"
              />
              <FormControlLabel
                value="pos13"
                control={<Radio />}
                label="Position 13"
              />
              <FormControlLabel
                value="pos14"
                control={<Radio />}
                label="Position 14"
              />
              <FormControlLabel
                value="pos15"
                control={<Radio />}
                label="Position 15"
              />
            </RadioGroup>
          </FormControl>
          <hr />

          <h4>
            <strong>Dashboard</strong>
          </h4>
          <FormGroup row>
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 1"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 2"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 3"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 4"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 5"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 6"
            />
          </FormGroup>
          <hr /> */}
          <h4>
            <strong>Tab Menu</strong>
          </h4>
          <FormGroup row>
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 1"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 2"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 3"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 4"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 5"
            />
            <FormControlLabel
              // control={<Checkbox defaultChecked />}
              control={<Checkbox />}
              label="Dashboard 6"
            />
          </FormGroup>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            Save
          </Button>
        </DialogActions>
      </BootstrapDialog>
      <Box
        sx={{
          width: "auto",
          maxWidth: "100%",
          margin: 2,
        }}
      >
        <Grid container spacing={3}>
          {/* Bagian Kiri */}
          <Grid item xs={12}>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>User</span>
            <Autocomplete
              id="controllable-states-demo"
              options={dataalluser}
              getOptionLabel={(option) => `${option.nama} - ${option.divisi}`}
              onChange={(e, val) => {
                console.log("Test value => ", val.nama);
                //setCriteriaId(val.id);
                setuseridsingle(val.id_user);
                setNamauser(val.nama);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  // label="Aspek"
                  margin="normal"
                  required
                  fullWidth
                  size="small"
                  placeholder="--Select User--"
                />
              )}
              style={{ marginTop: "-15px", marginBottom: 10 }}
            />
          </Grid>
        </Grid>
        <Button
          onClick={function (event) {
            InsertTeams();
          }}
          variant="contained"
          component={RouterLink}
          to="#"
          startIcon={<Iconify icon="eva:plus-fill" />}
          style={{
            backgroundColor: "#164477",
            boxShadow: "none",
            // marginTop: 10,
            marginBottom: 10,
            float: "right",
          }}
        >
          Add
        </Button>

        <br />
        <Paper sx={{ width: "100%", overflow: "hidden" }}>
          <hr style={{ marginTop: 5 }} />
          <Scrollbar>
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <StyledTableCell
                      style={{
                        backgroundColor: "#164477",
                        textAlign: "center",
                      }}
                    >
                      Photo
                    </StyledTableCell>
                    <StyledTableCell style={{ backgroundColor: "#164477" }}>
                      User ID
                    </StyledTableCell>
                    <StyledTableCell style={{ backgroundColor: "#164477" }}>
                      Name
                    </StyledTableCell>
                    <StyledTableCell
                      style={{
                        backgroundColor: "#164477",
                        textAlign: "center",
                        width: "30%",
                      }}
                    >
                      Action
                    </StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      //   const { id, name, role, status, company, avatarUrl, isVerified } = row;
                      //   const isItemSelected = selected.indexOf(name) !== -1;
                      const { user_id, user_name, project_id, id } = row;

                      return (
                        <TableRow>
                          <TableCell align="left">
                            <Box
                              component="img"
                              sx={{
                                height: 50,
                                width: 50,
                                borderRadius: "50%",
                              }}
                              src="https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YXZhdGFyfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60"
                            />
                          </TableCell>
                          <TableCell align="left">{user_id}</TableCell>
                          <TableCell align="left">{user_name}</TableCell>
                          <TableCell
                            style={{
                              textAlign: "center",
                            }}
                          >
                            <Stack
                              direction="row"
                              style={{ textAlign: "center" }}
                            >
                              <Grid item xs={8}>
                                <p style={{ textAlign: "left" }}>
                                  <small>Select Roles</small>
                                </p>
                                <Select
                                  labelId="demo-simple-select-label"
                                  id="demo-simple-select"
                                  value={roles}
                                  onChange={(e) => {
                                    setRoles(e.target.value);
                                    // UpdateCriteria(e.target.value, row.id);
                                  }}
                                  style={{ width: "100%" }}
                                  size="small"
                                >
                                  {roles_type.map((item, index) => {
                                    return (
                                      <MenuItem value={item.id}>
                                        {item.name}
                                      </MenuItem>
                                    );
                                  })}
                                </Select>
                              </Grid>

                              <Grid item xs={2}>
                                <p style={{ textAlign: "left" }}>
                                  <small>&nbsp;</small>
                                </p>
                                <Button
                                  variant="contained"
                                  onClick={handleClickOpen}
                                  startIcon={
                                    <Iconify icon="icon-park-solid:setting-two" />
                                  }
                                  style={{
                                    backgroundColor: "transparent  ",
                                    boxShadow: "none",
                                    color: "#4C6A8E",
                                    marginTop: 5,
                                    textAlign: "center",
                                  }}
                                  title="Setting"
                                ></Button>
                              </Grid>
                              <Grid item xs={2}>
                                <p style={{ textAlign: "left" }}>
                                  <small>&nbsp;</small>
                                </p>
                                <Button
                                  variant="contained"
                                  // onClickView={() => {
                                  //   setOpen(true);
                                  // }}
                                  startIcon={
                                    <Iconify icon="icon-park-solid:close-one" />
                                  }
                                  style={{
                                    backgroundColor: "transparent  ",
                                    boxShadow: "none",
                                    color: "#b5271d",
                                    marginTop: 5,
                                    textAlign: "center",
                                  }}
                                  onClick={(e) => {
                                    //console.log("kucingGarong", row.id)
                                    DellprojectTeam(row.id);
                                  }}
                                  title="Delete Member"
                                ></Button>
                              </Grid>
                            </Stack>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isUserNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
        <hr style={{ marginTop: 5 }} />
      </Box>
    </Card>
  );
}
