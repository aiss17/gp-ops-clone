// import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { Link as RouterLink } from "react-router-dom";
import { forwardRef, useEffect, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { styled } from "@mui/material/styles";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import DatePicker from "@mui/lab/DatePicker";
import TextareaAutosize from "@mui/material/TextareaAutosize";
// Autocomplete
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import { ENDPOINT } from "src/services/Constants";
import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  DialogTitle,
  IconButton,
  DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  DialogActions,
  Radio,
  Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab,
  Select,
  MenuItem,
  Snackbar,
} from "@mui/material";
import Iconify from "../../../../components/Iconify";
import UserMoreMenuTE from "src/sections/@dashboard/user/UserMoreMenuTE";
import filepdf from "../../../../assets/PDF.pdf";

// api
import {
  callApi,
  GetCookie,
  refreshTokenApi,
  SetCookie,
} from "src/services/Functions";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
// import { Client } from "src/pages/Masterdata";

// alert
import MuiAlert from "@mui/material/Alert";
import { LoadingData } from "src/pages";
import moment from "moment";
// ----------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

export default function StickyHeadTable(props) {
  const [page, setPage] = useState(0);
  const [arrayIdDoc, setArrayIdDoc] = useState([]);

  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [open, setOpen] = useState(false);
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [fileUpload, setFileUpload] = useState([]);
  const [dataproject, setDataproject] = useState([]);

  const [key, setKey] = useState("");
  const [data, setData] = useState([]);

  //get Project Info
  const [projectname, setProjectname] = useState("");
  const [projectvalue, setProjectvalue] = useState(null);
  const [contract_number, setContractnumber] = useState("");
  const [startdate, setStart] = useState(null);
  const [duedate, setDue] = useState(null);
  const [preparation, setPreparation] = useState(null);
  const [doc_review, setDocreview] = useState(null);
  const [assessment, setAssessment] = useState(null);
  const [applicant, setApplicant] = useState("");
  const [owner, setOwner] = useState("");
  const [port_name, setPortname] = useState("");
  const [operator, setOperator] = useState("");
  const [address, setAddress] = useState("");
  const [equipment, setEquipment] = useState("");
  const [description, setDescription] = useState("");
  const [proj_status, setProjStatus] = useState("");
  const [proj_id, setProjID] = useState(null);
  const [loading, setLoading] = useState(false);
  const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");
  // const [status, setStatus] = useState("");

  const [valueDate, setValueDate] = useState(null);

  // get teams
  const [dataalluser, setListuser] = useState([]);
  const [user_id, setuseridsingle] = useState("");
  const [nama_user, setNamauser] = useState("");

  // ALERT
  const [alert, setAlert] = useState(false);
  const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="standard" {...props} />;
  });
  const handleClickAlert = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setAlert(false);
  };

  useEffect(() => {
    console.log("id project => ", JSON.stringify(props.id_project));
    GetInfoProject();
    // GetAllTeams();
    return () => {};
  }, []);

  const GetInfoProject = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetAllProject + `/${props.id_project}`,
      null,
      "GET"
    );

    //get project data
    setDataproject(response.finalResponse.data[0]);
    setProjectname(response.finalResponse.data[0].project_name);
    setProjectvalue(response.finalResponse.data[0].value);
    setContractnumber(response.finalResponse.data[0].contract_number);
    setStart(response.finalResponse.data[0].start);
    setDue(response.finalResponse.data[0].due);
    setPreparation(response.finalResponse.data[0].preparation);
    setDocreview(response.finalResponse.data[0].doc_review);
    setAssessment(response.finalResponse.data[0].assessment);
    setApplicant(response.finalResponse.data[0].applicant);
    setOwner(response.finalResponse.data[0].owner_name);
    setPortname(response.finalResponse.data[0].port_name);
    setOperator(response.finalResponse.data[0].operator);
    setAddress(response.finalResponse.data[0].address);
    setEquipment(response.finalResponse.data[0].equipment);
    setDescription(response.finalResponse.data[0].description);
    setProjStatus(response.finalResponse.data[0].status);
    setProjID(response.finalResponse.data[0].id);
    setLoading(false);
    setLatitude(response.finalResponse.data[0].lat);
    setLongitude(response.finalResponse.data[0].long);
  };

  const UpdateProject = async () => {
    const payload = {
      project_id: Number(props.id_project),
      project_name: projectname,
      value: projectvalue,
      contract_number: contract_number,
      start: startdate,
      due: duedate,
      preparation: preparation,
      doc_review: doc_review,
      assessment: assessment,
      applicant: applicant,
      owner_name: owner,
      port_name: port_name,
      operator: operator,
      address: address,
      equipment: equipment,
      description: description,
      status: proj_status,
      lat: latitude,
      long: longitude,
    };
    console.log(payload);

    const response = await callApi(
      ENDPOINT.UpdateProject + props.id_project,
      payload,
      "POST"
    );
    console.log(JSON.stringify(response));

    setAlert(true);
    GetInfoProject();
  };

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    setDisable(false);
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  // Autocomplete (ComboBox)
  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  // const status = ["Done", "Undone"];
  // status option
  const status_opt = [
    // {
    //   id: 0,
    //   name: "On Progress"
    // },
    {
      id: 1,
      name: "On Progress",
    },
    {
      id: 2,
      name: "Done",
    },
  ];

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Card>
      <Box
        sx={{
          width: "auto",
          maxWidth: "100%",
          margin: 2,
        }}
      >
        <Grid container spacing={3}>
          <Snackbar
            open={alert}
            autoHideDuration={1000}
            onClose={handleClickAlert}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <Alert
              // variant="out"
              onClose={handleClickAlert}
              severity="success"
              sx={{ width: "100%" }}
              style={{ marginTop: 50 }}
            >
              Your data has been successfully updated!
            </Alert>
          </Snackbar>
          {/* Bagian Kiri */}
          <Grid item xs={6}>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Project Name
            </span>
            <TextField
              required
              margin="normal"
              id="name"
              // label="Project Name"
              type="text"
              fullWidth
              variant="outlined"
              value={projectname}
              onChange={(e) => {
                setProjectname(e.target.value);
              }}
              placeholder={"Insert Project Name"}
              size="small"
              style={{ marginTop: 0, marginBottom: 20 }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Project Value
            </span>
            <TextField
              required
              margin="normal"
              id="name"
              // label="Project Value"
              type="text"
              fullWidth
              variant="outlined"
              placeholder={"Insert Project Value"}
              value={projectvalue}
              onChange={(e) => {
                setProjectvalue(e.target.value);
              }}
              size="small"
              style={{ marginTop: 0, marginBottom: 20 }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Contract No
            </span>
            <TextField
              required
              margin="normal"
              id="name"
              // label="Contract No"
              type="text"
              fullWidth
              variant="outlined"
              placeholder={"Insert Contract No"}
              value={contract_number}
              onChange={(e) => {
                setContractnumber(e.target.value);
              }}
              size="small"
              style={{ marginTop: 0, marginBottom: 20 }}
            />
            {/* <span style={{ fontSize: "14px", fontWeight: 700 }}>Teams</span>

            <Autocomplete
              id="controllable-states-demo"
              options={dataalluser}
              getOptionLabel={(option) => `${option.nama} - ${option.divisi}`}
              onChange={(e, val) => {
                console.log("Test value => ", val.nama);
                //setCriteriaId(val.id);
                setuseridsingle(val.id_user);
                setNamauser(val.nama);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  // label="Aspek"
                  margin="normal"
                  required
                  fullWidth
                  size="small"
                  placeholder="--Select User--"
                />
              )}
              style={{ marginTop: "-15px", marginBottom: 10 }}
            /> */}
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Start Date
            </span>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                value={startdate}
                onChange={(newValue) => {
                  let text = newValue.toLocaleDateString("en-US");
                  setValueDate(text);
                  setStart(text);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    fullWidth
                    size="small"
                    style={{ marginTop: 0, marginBottom: 20 }}
                  />
                )}
              />
            </LocalizationProvider>

            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Preparation Date
            </span>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                value={preparation}
                onChange={(newValue) => {
                  let text = newValue.toLocaleDateString("en-US");
                  setValueDate(text);
                  setPreparation(text);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    fullWidth
                    size="small"
                    style={{ marginTop: 0 }}
                  />
                )}
              />
            </LocalizationProvider>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Document Review Date
            </span>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                value={doc_review}
                onChange={(newValue) => {
                  // let text = newValue.toLocaleDateString("en-US");
                  let text = moment(newValue).format("MM/DD/yyyy");
                  setValueDate(text);
                  setDocreview(text);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    fullWidth
                    size="small"
                    style={{ marginTop: 0 }}
                  />
                )}
              />
            </LocalizationProvider>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Assessment Date
            </span>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                value={assessment}
                onChange={(newValue) => {
                  let text = newValue.toLocaleDateString("en-US");
                  setValueDate(text);
                  setAssessment(text);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    fullWidth
                    size="small"
                    style={{ marginTop: 0 }}
                  />
                )}
              />
            </LocalizationProvider>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Due Date</span>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                value={duedate}
                onChange={(newValue) => {
                  let text = newValue.toLocaleDateString("en-US");
                  setValueDate(text);
                  setDue(text);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    fullWidth
                    size="small"
                    style={{ marginTop: 0 }}
                  />
                )}
              />
            </LocalizationProvider>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Longitude</span>
            <TextField
              required
              margin="normal"
              id="name"
              // label="Contract No"
              type="text"
              fullWidth
              variant="outlined"
              placeholder={"Insert Longitude Location"}
              value={longitude}
              onChange={(e) => {
                setLongitude(e.target.value);
              }}
              size="small"
              style={{ marginTop: 0, marginBottom: 10 }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Latitude</span>
            <TextField
              required
              margin="normal"
              id="name"
              // label="Contract No"
              type="text"
              fullWidth
              variant="outlined"
              placeholder={"Insert Latitude Location"}
              value={latitude}
              onChange={(e) => {
                setLatitude(e.target.value);
              }}
              size="small"
              style={{ marginTop: 0, marginBottom: 20 }}
            />
          </Grid>
          {/* Bagian Kanan */}
          <Grid item xs={6}>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Applicant</span>
            <TextField
              required
              margin="normal"
              id="name"
              // label="Project Name"
              type="text"
              fullWidth
              variant="outlined"
              placeholder={"Insert Applicant"}
              value={applicant}
              size="small"
              style={{ marginTop: 0, marginBottom: 20 }}
              onChange={(e) => {
                setApplicant(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Pemilik</span>
            <TextField
              required
              margin="normal"
              id="name"
              // label="Project Name"
              type="text"
              fullWidth
              variant="outlined"
              placeholder={"Insert Applicant"}
              value={owner}
              size="small"
              style={{ marginTop: 0, marginBottom: 20 }}
              onChange={(e) => {
                setOwner(e.target.value);
              }}
            />

            <span style={{ fontSize: "14px", fontWeight: 700 }}>Operator</span>
            <TextField
              required
              margin="normal"
              id="name"
              // label="Contract No"
              type="text"
              fullWidth
              variant="outlined"
              placeholder="Nama Operator"
              size="small"
              style={{ marginTop: 0, marginBottom: 20 }}
              value={operator}
              onChange={(e) => {
                setOperator(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Nama Pelabuhan
            </span>
            <TextField
              required
              margin="normal"
              id="name"
              // label="Contract No"
              type="text"
              fullWidth
              variant="outlined"
              placeholder="Nama Pelabuhan"
              size="small"
              style={{ marginTop: 0, marginBottom: 20 }}
              value={port_name}
              onChange={(e) => {
                setPortname(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Alamat</span>
            <TextareaAutosize
              aria-label="minimum height"
              minRows={5}
              placeholder="Alamat Pelabuhan"
              style={{
                width: "100%",
                padding: 10,
                borderColor: "silver",
                borderRadius: 5,
                resize: "none",
                marginBottom: 7,
                fontSize: "16px",
              }}
              value={address}
              onChange={(e) => {
                setAddress(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Equipment Bongkar Muat
            </span>
            <TextareaAutosize
              aria-label="minimum height"
              minRows={5}
              placeholder="Deskripsi Equipment Bongkar Muat"
              value={equipment}
              style={{
                width: "100%",
                padding: 10,
                borderColor: "silver",
                borderRadius: 5,
                resize: "none",
                marginBottom: 0,
                fontSize: "16px",
              }}
              onChange={(e) => {
                setEquipment(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Description
            </span>
            <TextareaAutosize
              aria-label="minimum height"
              minRows={6}
              placeholder="Narative Description"
              value={description}
              style={{
                width: "100%",
                padding: 10,
                borderColor: "silver",
                borderRadius: 5,
                resize: "none",
                marginBottom: 14,
                fontSize: "16px",
              }}
              onChange={(e) => {
                setDescription(e.target.value);
              }}
            />
          </Grid>

          <Grid item xs={12}>
            <hr style={{ marginTop: 0, marginBottom: 0 }} />
          </Grid>
          <Grid item xs={12}>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Status</span>
            <FormControl
              sx={{ m: 1, width: "100%", marginBottom: 10 }}
              size="small"
            >
              <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                placeholder="-- Select Status --"
                value={proj_status}
                onChange={(e) => {
                  setProjStatus(e.target.value);
                }}
              >
                {status_opt.map((item, index) => {
                  return <MenuItem value={item.id}>{item.name}</MenuItem>;
                })}
              </Select>
            </FormControl>
            <Button
              variant="contained"
              style={{
                backgroundColor: "#248bf5",
                boxShadow: "none",
                float: "right",
                marginBottom: 10,
              }}
              onClick={function (event) {
                UpdateProject();
              }}
            >
              Update
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
}
