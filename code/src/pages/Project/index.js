import Assessment from "./Assessment";
import Document from "./Document";
import Meeting from "./Meeting";
import Report from "./Report";
import TechnicalEnquiry from "./TechnicalEnquiry";
import TabProject from "./TabProject";
import ProjectList from "./ProjectList";
import Overview from "./Overview";
import { RemoteUC, DetailSurvey, RemoteSurvey } from "./RemoteSurvey";
// import RemoteSurvey from "./RemoteSurvey";
import VisitDetails from "./Visit/VisitDetails";
import Administrative from "./Administrative";

export {
  Assessment,
  Document,
  Meeting,
  Report,
  TechnicalEnquiry,
  TabProject,
  ProjectList,
  Overview,
  RemoteSurvey,
  VisitDetails,
  DetailSurvey,
  Administrative,
  RemoteUC
};
