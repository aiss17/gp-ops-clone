import RemoteUC from "./RemoteUC";
import RemoteSurvey from "./RemoteSurveyList";
import DetailSurvey from "./RemoteDetails";

export { RemoteUC, DetailSurvey, RemoteSurvey };
