import { List, ListItem } from "@mui/material";
import Page from "src/components/Page";
import { underconstruction } from "../../../../assets/images";

export default function Report() {
  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      <List style={{ marginTop: `auto` }}>
        <ListItem>
          <img
            src={underconstruction}
            style={{ width: "50%", marginLeft: "25%" }}
            alt="logo"
          />
        </ListItem>
      </List>
    </Page>
  );
}
