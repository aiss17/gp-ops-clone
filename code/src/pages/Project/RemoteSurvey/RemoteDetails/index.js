import { filter } from "lodash";
import { sentenceCase } from "change-case";
import React, { useRef, useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid,
  ImageList,
  ImageListItem
} from "@mui/material";
import moment from "moment";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

// components
import Page from "../../../../components/Page";
import Label from "../../../../components/Label";
import Scrollbar from "../../../../components/Scrollbar";
import Iconify from "../../../../components/Iconify";
import SearchNotFound from "../../../../components/SearchNotFound";
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu
} from "../../../../sections/@dashboard/user";
//
import { MEETINGS } from "../../../../_mocks_/user";

//texteditor
import { Editor } from "@tinymce/tinymce-react";

// ----------------------------------------------------------------------
import TableHead from "@mui/material/TableHead";
import Paper from "@mui/material/Paper";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#00AF5B",
    color: "#ffffff"
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0
  }
}));

const TABLE_HEAD = [
  { id: "no", label: "No. ", alignRight: false },
  { id: "name", label: "Meeting Name", alignRight: false },
  // { id: "note", label: "Meeting Note", alignRight: false },
  { id: "startDate", label: "Meeting Date", alignRight: false },
  { id: "" }
];

// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

const itemData = [
  {
    img: "https://images.unsplash.com/photo-1551963831-b3b1ca40c98e",
    title: "Breakfast",
    author: "@bkristastucchio"
  },
  {
    img: "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
    title: "Burger",
    author: "@rollelflex_graphy726"
  },
  {
    img: "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
    title: "Camera",
    author: "@helloimnik"
  },
  {
    img: "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c",
    title: "Coffee",
    author: "@nolanissac"
  }
];

export default function RemoteDetails() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("name");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [open, setOpen] = useState(false);
  const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [image, setImage] = useState("");
  const [imageName, setImageName] = useState("");

  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
    }
  };

  useEffect(() => {
    console.log(MEETINGS);

    return () => {};
  }, []);

  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setDisable(false);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = MEETINGS.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    console.log(selectedIndex);
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - MEETINGS.length) : 0;

  const filteredUsers = applySortFilter(
    MEETINGS,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Remote Inspection Details
        </Typography>
      </Stack>

      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="md"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          {imageName}
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500]
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent
          style={{
            marginLeft: 15,
            marginRight: 15,
            alignSelf: "center"
          }}
          dividers
        >
          <img
            width={300}
            height={200}
            src={`${image}?w=248&fit=crop&auto=format`}
            srcSet={`${image}?w=248&fit=crop&auto=format&dpr=4 4x`}
            alt={imageName}
            loading="lazy"
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="ion:close" />}
            style={{
              backgroundColor: "red",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10
            }}
          >
            Close
          </Button>
        </DialogActions>
      </BootstrapDialog>

      <Card>
        <UserListToolbar
          numSelected={selected.length}
          filterName={filterName}
          onFilterName={handleFilterByName}
        />

        <Scrollbar>
          <TableContainer component={Paper} style={{ borderRadius: 0 }}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell
                    style={{
                      textAlign: "center"
                    }}
                  >
                    No.
                  </StyledTableCell>
                  <StyledTableCell>Survey Date</StyledTableCell>
                  <StyledTableCell>Assessor</StyledTableCell>
                  <StyledTableCell>Work Criteria</StyledTableCell>
                  <StyledTableCell>Evidence</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    //   const { id, name, role, status, company, avatarUrl, isVerified } = row;
                    //   const isItemSelected = selected.indexOf(name) !== -1;
                    const { name, startDate } = row;
                    const isItemSelected = selected.indexOf(name) !== -1;
                    return (
                      <TableRow>
                        <TableCell
                          style={{
                            textAlign: "center"
                          }}
                        >
                          {index + 1}
                        </TableCell>
                        {/* <TableCell align="left">{note}</TableCell> */}
                        <TableCell align="left">
                          {moment(startDate).format("LL")}
                        </TableCell>
                        <TableCell align="center">{name}</TableCell>
                        <TableCell align="center">{name}</TableCell>
                        {/* <TableCell align="left">{status}</TableCell> */}
                        <TableCell align="right">
                          <ImageList sx={{ width: 150, height: 100 }} cols={3}>
                            {itemData.map((item) => (
                              <ImageListItem
                                key={item.img}
                                onClick={() => {
                                  setImage(item.img);
                                  setImageName(item.title);
                                  setOpen(true);
                                }}
                              >
                                <img
                                  src={`${item.img}?w=248&fit=crop&auto=format`}
                                  srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=4 4x`}
                                  alt={item.title}
                                  loading="lazy"
                                />
                              </ImageListItem>
                            ))}
                          </ImageList>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={MEETINGS.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Page>
  );
}
