import { filter } from "lodash";
import { sentenceCase } from "change-case";
import React, { useRef, useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid
} from "@mui/material";
import moment from "moment";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

// components
import Page from "../../../components/Page";
import Label from "../../../components/Label";
import Scrollbar from "../../../components/Scrollbar";
import Iconify from "../../../components/Iconify";
import SearchNotFound from "../../../components/SearchNotFound";
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
  UserMoreMenuMeeting
} from "../../../sections/@dashboard/user";
//
import { MEETINGS } from "../../../_mocks_/user";

//texteditor
import { Editor } from "@tinymce/tinymce-react";
// Editor.activeEditor.selection.getContent({ format: "text" });

// ----------------------------------------------------------------------
import TableHead from "@mui/material/TableHead";
import Paper from "@mui/material/Paper";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import { callApi, GetCookie } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { DateTimePicker } from "@mui/lab";
import { LoadingData } from "src/pages";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff"
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0
  }
}));

// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.agenda.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Meeting(props) {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("agenda");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [open, setOpen] = useState(false);
  const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  // New Meeting
  const [agenda, setAgenda] = useState("");
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [meetingNote, setMeetingNote] = useState("");

  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
    }
  };

  useEffect(() => {
    console.log("ini props nya => ", JSON.stringify(props));
    GetAllMeeting();

    return () => {};
  }, []);

  const GetAllMeeting = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetAllMeeting,
      { project: props.id_project },
      "GET"
    );

    console.log("Shark => ", response.finalResponse.data);

    setData(response.finalResponse.data);
    setLoading(false);
  };

  const PostMeeting = async () => {
    const payload = {
      project_id: props.id_project,
      start: startDate,
      finish: endDate,
      agenda: agenda,
      meeting_minutes: meetingNote
    };
    const response = await callApi(ENDPOINT.GetAllMeeting, payload, "POST");

    setOpen(false);
    setMeetingNote("");
    setStartDate(null);
    setEndDate(null);
    setAgenda("");

    GetAllMeeting();
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setDisable(false);

    setAgenda("");
    setStartDate(null);
    setEndDate(null);
    setMeetingNote("");
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Meeting Management
        </Typography>
        <Button
          variant="contained"
          component={RouterLink}
          to="#"
          startIcon={<Iconify icon="eva:plus-fill" />}
          onClick={handleClickOpen}
          style={{
            backgroundColor: "#254A7C",
            boxShadow: "none",
            color: "ffffff",
            "&:hover": { color: "#ffce00" }
          }}
        >
          New MOM
        </Button>
      </Stack>

      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          New MOM
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500]
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography variant="h6" gutterBottom>
                Meeting Agenda
              </Typography>
              <TextField
                required
                autoFocus
                margin="normal"
                id="name"
                // label="Meeting Name"
                type="text"
                disabled={disable}
                fullWidth
                variant="outlined"
                placeholder="Insert Meeting Agenda"
                value={agenda}
                onChange={(e) => setAgenda(e.target.value)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} style={{ marginTop: 5 }}>
            <Grid item xs={6}>
              <Typography variant="h6" gutterBottom>
                Start Date
              </Typography>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DateTimePicker
                  disabled={disable}
                  label="Meeting Date"
                  value={startDate}
                  onChange={(newValue) => {
                    setStartDate(newValue);
                  }}
                  renderInput={(params) => (
                    <TextField {...params} margin="normal" required fullWidth />
                  )}
                />
              </LocalizationProvider>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="h6" gutterBottom>
                Finish Date
              </Typography>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DateTimePicker
                  disabled={disable}
                  label="Meeting Date"
                  value={endDate}
                  onChange={(newValue) => {
                    setEndDate(newValue);
                  }}
                  renderInput={(params) => (
                    <TextField {...params} margin="normal" required fullWidth />
                  )}
                />
              </LocalizationProvider>
            </Grid>
          </Grid>
          <hr></hr>
          <Grid container spacing={5}>
            <Grid item xs={12}>
              <Typography variant="h6" gutterBottom>
                Meeting Note
              </Typography>
              <div>
                {disable ? (
                  <Editor
                    apiKey={process.env.REACT_APP_API_KEY_TINYMCE}
                    onInit={(evt, editor) => (editorRef.current = editor)}
                    // initialValue="<p>This is the initial content of the editor.</p>"
                    init={{
                      height: 300,
                      menubar: false,
                      plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table paste code help wordcount",
                      ],
                      toolbar:
                        "undo redo | formatselect | " +
                        "bold italic backcolor | alignleft aligncenter " +
                        "alignright alignjustify | bullist numlist outdent indent | " +
                        "removeformat | help",
                      content_style:
                        "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                    }}
                    onEditorChange={(content, editor) =>
                      // setMeetingNote(editor.getContent({ format: "text" }))
                      setMeetingNote(editor.getContent())
                    }
                    value={meetingNote}
                    disabled
                    // onKeyPress={(e) => console.log(e)}
                  />
                  // <TextField
                  //   required
                  //   autoFocus
                  //   margin="normal"
                  //   id="name"
                  //   // label="Meeting Name"
                  //   type="text"
                  //   disabled={disable}
                  //   fullWidth
                  //   multiline
                  //   rows={4}
                  //   variant="outlined"
                  //   placeholder="Insert Meeting Agenda"
                  //   value={meetingNote}
                  //   // onChange={(e) => setAgenda(e.target.value)}
                  // />
                ) : (
                  <Editor
                    apiKey={process.env.REACT_APP_API_KEY_TINYMCE}
                    onInit={(evt, editor) => (editorRef.current = editor)}
                    // initialValue="<p>This is the initial content of the editor.</p>"
                    init={{
                      height: 300,
                      menubar: false,
                      plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table paste code help wordcount"
                      ],
                      toolbar:
                        "undo redo | formatselect | " +
                        "bold italic backcolor | alignleft aligncenter " +
                        "alignright alignjustify | bullist numlist outdent indent | " +
                        "removeformat | help",
                      content_style:
                        "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }"
                    }}
                    onEditorChange={(content, editor) =>
                      // setMeetingNote(editor.getContent({ format: "text" }))
                      setMeetingNote(editor.getContent())
                    }
                    // onKeyPress={(e) => console.log(e)}
                  />
                )}
              </div>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={disable ? handleClose : PostMeeting}
            variant="contained"
            component={RouterLink}
            // disabled={disable}
            to="#"
            startIcon={
              <Iconify icon={disable ? "ion:close" : "eva:save-fill"} />
            }
            style={{
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10
            }}
            color={disable ? "error" : "info"}
          >
            {disable ? "close" : "Save Changes"}
          </Button>
        </DialogActions>
      </BootstrapDialog>

      <Card>
        <UserListToolbar
          numSelected={selected.length}
          filterName={filterName}
          onFilterName={handleFilterByName}
        />

        <Scrollbar>
          <TableContainer component={Paper} style={{ borderRadius: 0 }}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell
                    style={{
                      textAlign: "center"
                    }}
                  >
                    No.
                  </StyledTableCell>
                  <StyledTableCell>Agenda</StyledTableCell>
                  <StyledTableCell>Created Date</StyledTableCell>
                  <StyledTableCell>Action</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    //   const { id, name, role, status, company, avatarUrl, isVerified } = row;
                    //   const isItemSelected = selected.indexOf(name) !== -1;
                    const {
                      agenda,
                      created_at,
                      start,
                      finish,
                      meeting_minutes
                    } = row;
                    const isItemSelected = selected.indexOf(agenda) !== -1;
                    return (
                      <TableRow>
                        <TableCell
                          style={{
                            textAlign: "center"
                          }}
                        >
                          {index + 1}
                        </TableCell>
                        <TableCell align="center">{agenda}</TableCell>
                        {/* <TableCell align="left">{note}</TableCell> */}
                        <TableCell align="left">
                          {moment(created_at).format("LL")}
                        </TableCell>
                        {/* <TableCell align="left">{status}</TableCell> */}
                        <TableCell align="right">
                          <UserMoreMenuMeeting
                            onClickView={() => {
                              setAgenda(agenda);
                              setStartDate(start);
                              setEndDate(finish);
                              setMeetingNote(meeting_minutes);

                              setOpen(true);
                              setDisable(true);
                            }}
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Page>
  );
}
