import { forwardRef, useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import Tabs, { tabsClasses } from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Article from "@mui/icons-material/Article";
import VideoCameraFrontIcon from "@mui/icons-material/VideoCameraFront";
import SummarizeIcon from "@mui/icons-material/Summarize";
import QuizIcon from "@mui/icons-material/Quiz";
import FactCheckIcon from "@mui/icons-material/FactCheck";

// pages
import Criteria from "./Criteria";
// import PG from "../Assessment/PG";
// import KP from "../Assessment/KP";
// import TP from "../Assessment/TP";
// import KU from "../Assessment/KU";
import { callApi, GetCookie } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { ConstructionOutlined } from "@mui/icons-material";
import { useParams } from "react-router-dom";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`
  };
}

export default function Assessment() {
  const [value, setValue] = useState(0);
  const [tab, setTab] = useState([]);
  const [data, setData] = useState(null);

  const { id } = useParams();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  useEffect(() => {
    console.log("Makan => ", id);
    GetAllAssessment();

    return () => {};
  }, []);

  const GetAllAssessment = async () => {
    // console.log(props.id_project);
    const response = await callApi(
      ENDPOINT.GetAllAssessment,
      { project: id },
      "GET"
    );

    setTab(Object.keys(response.finalResponse.data).map((key) => key));
    setData(response.finalResponse.data);

    console.log(
      "array object nya => ",
      Object.keys(response.finalResponse.data).map((key) => key)
    );
  };

  return (
    <Box style={{ clear: "both" }}>
      <Box
        sx={{
          borderBottom: 1,
          borderColor: "divider",
          maxWidth: { xs: 320, sm: 1100 }
        }}
      >
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="visible arrows tabs example"
          variant="scrollable"
          scrollButtons
          style={{ marginTop: "-15px", marginLeft: 25, marginRight: 25 }}
          sx={{
            [`& .${tabsClasses.scrollButtons}`]: {
              "&.Mui-disabled": { opacity: 0.3 }
            }
          }}
        >
          {tab.map((tabs, index) => {
            return <Tab label={tabs} {...a11yProps(index)} />;
          })}
          {/* <Tab label="KK" {...a11yProps(0)} />
          <Tab label="PG" {...a11yProps(1)} />
          <Tab label="KP" {...a11yProps(2)} />
          <Tab label="TP" {...a11yProps(3)} />
          <Tab label="KU" {...a11yProps(4)} /> */}
        </Tabs>
      </Box>

      {tab.map((item, index) => {
        return (
          <TabPanel
            value={value}
            index={index}
            style={{ marginLeft: "-25px", marginRight: "-25px" }}
          >
            <Criteria criteria={item} dataprops={data} id_project={id} />
          </TabPanel>
        );
      })}

      {tab.length < 1 && (
        <div
          style={{
            width: "100%",
            padding: 10,
            textAlign: "center",
            border: "1px solid #dddddd"
          }}
        >
          There is no file
        </div>
      )}

      {/* <TabPanel
        value={value}
        index={0}
        style={{ marginLeft: "-25px", marginRight: "-25px" }}
      >
        <KL criteria={"KK"} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <PG />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <KP />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <TP />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <KU />
      </TabPanel> */}
    </Box>
  );
}
