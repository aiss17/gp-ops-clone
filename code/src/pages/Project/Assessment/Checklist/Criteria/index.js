import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { Link as RouterLink, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { styled } from "@mui/material/styles";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import DatePicker from "@mui/lab/DatePicker";
import TextareaAutosize from "@mui/material/TextareaAutosize";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  DialogTitle,
  IconButton,
  DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  DialogActions,
  Radio,
  Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab,
  Autocomplete,
  Select,
  MenuItem,
} from "@mui/material";
import Iconify from "../../../../../components/Iconify";
import UserMoreMenuTE from "src/sections/@dashboard/user/UserMoreMenuTE";
// import filepdf from "../../../../assets/PDF.pdf";
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import moment from "moment";
import { LocalizationProvider } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import PDFViewer from "pdf-viewer-reactjs";
import {
  BMP,
  EXCEL,
  JPEG,
  JPG,
  PDF,
  PNG,
  PPT,
  WORD,
} from "../../../../../assets/images";
import { LoadingData } from "src/pages";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  chatSection: {
    width: "100%",
    height: "80vh",
  },
  headBG: {
    backgroundColor: "#e0e0e0",
  },
  borderRight500: {
    borderRight: "1px solid #e0e0e0",
  },
  messageArea: {
    height: "70vh",
    overflowY: "auto",
    fontSize: "5px",
  },
  imessage: {
    backgroundColor: "#fff",
    // border: "1px solid #e5e5ea",
    // borderRadius: "0.25rem",
    display: "flex",
    flexDirection: "column",
    fontSize: "12px",
    margin: "0 auto 1rem",
    maxWidth: "600px",
    padding: "0.5rem 1.5rem",
  },
  imessage_p: {
    borderRadius: "1.15rem",
    lineHeight: "1.25",
    maxWidth: "75%",
    padding: "0.5rem .875rem",
    position: "relative",
    wordWrap: "break-word",
  },

  from_me: {
    alignSelf: "flex-end",
    backgroundColor: "#B5EAC3",
    color: "#000000",
    margin: 5,
    padding: 10,
    borderRadius: 10,
  },

  from_them: {
    alignItems: "flex-start",
    backgroundColor: "#e5e5ea",
    color: "#3a3a3a",
    margin: 5,
    padding: 10,
    borderRadius: 10,
    maxWidth: "70%",
  },
});

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

export default function StickyHeadTable(props) {
  const [visibleUpload, setVisibleUpload] = useState(false);
  const [alert, setAlert] = useState(false);
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [disable, setDisable] = useState(false);
  const [openRow, setOpenRow] = useState(false);
  const [visibleSaveUpload, setVisibleSaveUpload] = useState(false);
  const [visibleRevise, setvisibleRevise] = useState(false);
  const [dataDisable, setDataDisable] = useState(false);
  const [openRevise, setOpenRevise] = useState(false);

  const [page, setPage] = React.useState(0);
  const [arrayIdDoc, setArrayIdDoc] = useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [valueRadio, setValueRadio] = useState("");
  const [fileUpload, setFileUpload] = useState([]);
  const [file, setFile] = useState([]);
  const [key, setKey] = useState("");
  const [data, setData] = useState([]);
  const [chat, setChat] = useState("");
  const [collapse, setCollapse] = useState([]);
  const [dataTemp, setDataTemp] = useState([]);
  const [dataSend, setDataSend] = useState([]);
  const [remark, setRemark] = useState("");
  const [idChecklist, setIdChecklist] = useState("");
  const [fileShow, setFileShow] = useState("");
  const [fileType, setFileType] = useState("");
  const [project_id, setProject_id] = useState(null);
  const [doc_title, setDoc_title] = useState("");
  const [issuer, setIssuer] = useState("");
  const [valid_until, setValid_until] = useState(null);
  const [created_at, setCreated_at] = useState(null);
  const [created_by, setCreated_by] = useState(null);
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [descRevise, setDescRevise] = useState("");
  const [fileRevise, setFileRevise] = useState(null);
  const [dataHistory, setDataHistory] = useState([]);
  const [valueAuto, setValueAuto] = useState("");
  const [size, setSize] = useState(null);
  const [id, setId] = useState("");
  const [aspect, setAspect] = useState("");
  const [criteria, setCriteria] = useState("");

  const classes = useStyles();
  const params = useParams();

  useEffect(() => {
    console.log("ini props nya loohhh => ", props.id_project);
    GetAllAssessment();
    return () => {};
  }, []);

  const metadataSingleUpload = async () => {
    const formData = new FormData();

    formData.append("id", id);
    formData.append("doc_title", doc_title);
    formData.append("issuer", issuer);
    formData.append(
      "valid_until",
      valid_until === null ? null : moment(valid_until).format("YYYY-MM-DD")
    );
    formData.append("key", key);
    formData.append("project_id", props.id_project);

    const response = await callApi(ENDPOINT.MetadataUpload, formData, "UPLOAD");
    setKey(response.keys);
    setDataTemp([]);
    setDataSend([]);
    setFileUpload([]);
    setFile([]);

    setVisibleUpload(false);
    setVisibleSaveUpload(false);
    setOpenRevise(false);
    setDisable(false);

    GetAllAssessment();
  };

  const GetDocumentByID = async (item) => {
    const response = await callApi(
      ENDPOINT.GetAllDocument + `/${item.id}`,
      { project: props.id_project },
      "GET"
    );
    console.log(response.finalResponse);
    setId(item.id);
    setProject_id(item.project_id);
    setKey(response.keys);
    setFileShow(response.finalResponse.data[0].path);
    setFileType(response.finalResponse.data[0].filetype);
    setDoc_title(response.finalResponse.data[0].doc_title);
    setIssuer(response.finalResponse.data[0].issuer);
    setCreated_at(
      moment(response.finalResponse.data[0].created_at).format("MMM DD, yyyy")
    );
    setDataHistory(response.finalResponse.data[0].revision);
    setCreated_by(response.finalResponse.data[0].created_by);
    setSize(response.finalResponse.data[0].size);

    if (response.finalResponse.data[0].valid_until !== null) {
      setValid_until(
        moment(response.finalResponse.data[0].valid_until).format()
      );
    } else {
      setDataDisable(true);
    }

    setTimeout(() => {
      setOpenRevise(true);
    }, 500);
  };

  const GetAllAssessment = async () => {
    setLoading(true);
    let detailProject;
    let array = [];

    const response = await callApi(
      ENDPOINT.GetAllAssessment + "?project=" + params.id,
      null,
      "GET"
    );

    setKey(response.keys);

    setData(response.finalResponse.data[props.criteria]);
    setAspect(response.finalResponse.data[props.criteria][0].aspect);
    setCriteria(response.finalResponse.data[props.criteria][0].criteria);
    setLoading(false);
  };

  const CommentChat = async (id) => {
    const payload = {
      item_id: id,
      comment: chat,
      from: "int",
    };

    const response = await callApi(ENDPOINT.InsertComment, payload, "POST");
    console.log("Haha => ", response.finalResponse);

    setChat("");
    GetAllAssessment();
  };

  const GetAssessmentById = async (id) => {
    let detailProject;
    const response = await callApi(
      ENDPOINT.GetAllAssessment + `/${id}`,
      { project: props.id_project },
      "GET"
    );
    setKey(response.keys);
    detailProject = response.finalResponse.data[0];

    return detailProject;
  };

  const UpdateRemark = async (id) => {
    const payload = {
      note: remark,
      project: props.id_project,
    };

    const response = await callApi(ENDPOINT.Update + `/${id}`, payload, "POST");

    setKey(response.keys);
    setRemark("");
    GetAllAssessment();
    setCollapse([]);
  };

  const UpdateChecklist = async (value, id) => {
    const payload = {
      compliance: value,
      project: props.id_project,
    };

    const response = await callApi(ENDPOINT.Update + `/${id}`, payload, "POST");

    setKey(response.keys);
    GetAllAssessment();
    setCollapse([]);
  };

  const UpdateCatatan = async (value, id) => {
    const payload = {
      assessor_note: value,
      project: props.id_project,
    };

    const response = await callApi(ENDPOINT.Update + `/${id}`, payload, "POST");

    setKey(response.keys);
    GetAllAssessment();
    setCollapse([]);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    console.log(fileUpload);
    setDataTemp([]);
    setDataSend([]);
    setFileUpload([]);
    setFile([]);
    setVisibleUpload(false);
    setOpen(false);
    setDisable(false);
    setArrayIdDoc([]);
    setVisibleSaveUpload(false);

    GetAllAssessment();
  };

  const handleCloseRevise = () => {
    setDataTemp([]);
    setDataSend([]);
    setFileUpload([]);
    setFile([]);
    setVisibleUpload(false);
    setOpenRevise(false);
    setDisable(false);
    setVisibleSaveUpload(false);

    GetAllAssessment();
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChange = (item) => {
    const index = dataTemp.indexOf(item.id);
    if (index > -1) {
      dataTemp.splice(index, 1);
      dataSend.splice(index, 1); // 2nd parameter means remove one item only
    } else {
      dataTemp.push(item.id);
      dataSend.push(item);
    }

    if (dataSend.length > 0) {
      setVisibleSaveUpload(true);
    } else {
      setVisibleSaveUpload(false);
    }
    console.log(dataSend);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleCollapse = (id) => {
    if (collapse.includes(id)) {
      setCollapse(collapse.filter((item) => item !== id));
    } else {
      setCollapse([...collapse, id]);
    }
  };

  const onFileChange = (event) => {
    console.log(event.target.files);
    const imagesArray = [];
    for (let i = 0; i < event.target.files.length; i++) {
      let payload = {
        id: null,
        doc_title: "",
        fileName: event.target.files[i].name,
        issuer: "",
        valid_until: null,
        disable: false,
      };
      fileUpload.push(payload);
      imagesArray.push(event.target.files[i]);
      console.log("fileUpload => ", fileUpload);
    }
    setFile(imagesArray);
  };

  const MultiUploadDocument = async () => {
    setLoading(true);
    const formData = new FormData();

    formData.append("project_id", props.id_project);
    for (let i = 0; i < file.length; i++) {
      formData.append("docs[]", file[i]);
    }
    formData.append("key", key);

    callApi(ENDPOINT.MultiUploadDocument, formData, "UPLOAD").then((res) => {
      arrayIdDoc.push(res.finalResponse.datas);
      console.log(res.finalResponse.datas);
      for (let i = 0; i < fileUpload.length; i++) {
        fileUpload[i].id = res.finalResponse.datas[i];
      }

      metadataMultipleUploadLink();
    });
  };

  const metadataMultipleUploadLink = async () => {
    console.log("test masuk");
    const payload = {
      data: fileUpload,
      itemid: idChecklist,
      projectid: props.id_project,
    };

    const response = await callApi(ENDPOINT.LinkFile, payload, "POST");

    console.log("array ", arrayIdDoc);

    setAlert(true);
    setLoading(false);
    // setKey(response.keys);
    // setDataTemp([]);
    // setDataSend([]);
    // setFileUpload([]);
    // setFile([]);
    // setArrayIdDoc([]);
    // setVisibleUpload(false);
    // setOpen(false);
    // setDisable(false);
    // setVisibleSaveUpload(false);

    // GetAllAssessment();
  };

  const metadataMultipleUpload = async () => {
    console.log("test masuk");
    const payload = {
      data: dataSend,
      project: props.id_project,
    };

    const response = await callApi(ENDPOINT.MetadataUpload, payload, "POST");
    setKey(response.keys);
    setDataTemp([]);
    setDataSend([]);
    setFileUpload([]);
    setFile([]);
    setArrayIdDoc([]);
    setVisibleUpload(false);
    setOpen(false);
    setDisable(false);
    setVisibleSaveUpload(false);

    GetAllAssessment();
  };

  const options = ["Assessment", "Profile"];

  const onFileChangeRevise = (event) => {
    setFileRevise(event.target.files[0]);
  };

  const UploadRevise = async () => {
    const formData = new FormData();

    formData.append("doc_id", id);
    formData.append("project_id", props.id_project);
    formData.append("description", descRevise);
    formData.append("docs", fileRevise);
    formData.append("key", key);

    const response = await callApi(ENDPOINT.UploadRevise, formData, "UPLOAD");
    setDescRevise("");
    setvisibleRevise(false);
    setAlert(true);
    setOpenRevise(false);

    GetAllAssessment();
  };

  const complience = ["Complete", "Enough", "Partial", "Lacking", "No", "N/A"];

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Card>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="xl"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Evidence
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <div style={{ padding: 15 }}>
            <label htmlFor="upload-photo" style={{ color: "#667480" }}>
              Upload Documents
            </label>
            <br />

            <input
              type="file"
              style={{ color: "#667480" }}
              multiple
              onChange={(e) => onFileChange(e)}
            />
            {/* </div> */}
            {/* <div> */}
            <Button
              variant="contained"
              onClick={() => {
                setVisibleUpload(true);
                MultiUploadDocument();
              }}
            >
              Upload
            </Button>
            {/* </div> */}
          </div>
          <div style={{ width: "100%", padding: 20, marginBottom: 20 }}>
            <table
              style={{
                fontFamily: "arial, sans-serif",
                borderCollapse: "collapse",
                width: "100%",
              }}
            >
              <tr>
                <th style={{ border: "1px solid #dddddd", width: "5%" }} />
                <th
                  style={{
                    border: "1px solid #dddddd",
                    textAlign: "left",
                    padding: "8px",
                    width: "20%",
                  }}
                >
                  Filename
                </th>
                <th
                  style={{
                    border: "1px solid #dddddd",
                    textAlign: "left",
                    padding: "8px",
                    width: "20%",
                  }}
                >
                  Document Name
                </th>
                <th
                  style={{
                    border: "1px solid #dddddd",
                    textAlign: "left",
                    padding: "8px",
                    width: "20%",
                  }}
                >
                  Issuer
                </th>
                <th
                  style={{
                    border: "1px solid #dddddd",
                    textAlign: "left",
                    padding: "8px",
                    width: "20%",
                  }}
                >
                  Valid Until
                </th>
              </tr>
              {visibleUpload &&
                fileUpload.length > 0 &&
                fileUpload.map((item, index) => {
                  const idDoc = fileUpload[index].id;
                  return (
                    <tr>
                      <td
                        style={{
                          border: "1px solid #dddddd",
                          textAlign: "left",
                          padding: "8px",
                        }}
                      >
                        {fileUpload[index].doc_title !== "" &&
                          (fileUpload[index].disable !== false ||
                            fileUpload[index].valid_until !== null) && (
                            <Checkbox
                              checked={dataTemp.find((x) => x.id === item.id)}
                              onChange={() => handleChange(item)}
                              size="small"
                            />
                          )}
                      </td>
                      <td
                        style={{
                          border: "1px solid #dddddd",
                          textAlign: "left",
                          padding: "8px",
                        }}
                      >
                        {item.fileName}
                      </td>
                      <td
                        style={{
                          border: "1px solid #dddddd",
                          textAlign: "left",
                          padding: "8px",
                        }}
                      >
                        <TextField
                          placeholder="Document Name"
                          required
                          autoFocus
                          margin="normal"
                          id="document_name"
                          // label="Document Name"
                          type="text"
                          fullWidth
                          variant="outlined"
                          disabled={disable}
                          value={fileUpload[index].doc_title}
                          onChange={(e) => {
                            const objIndex = fileUpload.findIndex(
                              (obj) => obj.id === idDoc
                            );

                            // make new object of updated object.
                            const updatedObj = {
                              ...fileUpload[objIndex],
                              doc_title: e.target.value,
                            };

                            // make final new array of objects by combining updated object.
                            const updatedProjects = [
                              ...fileUpload.slice(0, objIndex),
                              updatedObj,
                              ...fileUpload.slice(objIndex + 1),
                            ];

                            setFileUpload(updatedProjects);
                          }}
                          size="small"
                        />
                        {fileUpload[index].doc_title === "" && (
                          <p style={{ fontSize: 10, color: "red" }}>
                            * Document name is required{" "}
                          </p>
                        )}
                      </td>
                      <td
                        style={{
                          border: "1px solid #dddddd",
                          textAlign: "left",
                          padding: "8px",
                        }}
                      >
                        <TextField
                          placeholder="Issuer"
                          required
                          autoFocus
                          margin="normal"
                          id="issuer"
                          // label="Document Name"
                          type="text"
                          fullWidth
                          variant="outlined"
                          disabled={disable}
                          value={fileUpload[index].issuer}
                          onChange={(e) => {
                            const objIndex = fileUpload.findIndex(
                              (obj) => obj.id === idDoc
                            );

                            // make new object of updated object.
                            const updatedObj = {
                              ...fileUpload[objIndex],
                              issuer: e.target.value,
                            };

                            // make final new array of objects by combining updated object.
                            const updatedProjects = [
                              ...fileUpload.slice(0, objIndex),
                              updatedObj,
                              ...fileUpload.slice(objIndex + 1),
                            ];

                            setFileUpload(updatedProjects);
                          }}
                          size="small"
                        />
                      </td>
                      <td
                        style={{
                          border: "1px solid #dddddd",
                          textAlign: "left",
                          padding: "8px",
                        }}
                      >
                        {/* {moment(item.valid_until).format("MM/DD/YYYY")} */}
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                          <DatePicker
                            disabled={fileUpload[index].disable}
                            label="Valid Until"
                            value={fileUpload[index].valid_until}
                            onChange={(newValue) => {
                              //find the index of object from array that you want to update
                              const objIndex = fileUpload.findIndex(
                                (obj) => obj.id === idDoc
                              );

                              // make new object of updated object.
                              const updatedObj = {
                                ...fileUpload[objIndex],
                                valid_until: newValue,
                              };

                              // make final new array of objects by combining updated object.
                              const updatedProjects = [
                                ...fileUpload.slice(0, objIndex),
                                updatedObj,
                                ...fileUpload.slice(objIndex + 1),
                              ];

                              setFileUpload(updatedProjects);
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                margin="normal"
                                required
                                fullWidth
                                disabled={disable}
                                size="small"
                              />
                            )}
                          />
                        </LocalizationProvider>
                        <FormControlLabel
                          control={
                            <Checkbox
                              size="small"
                              checked={fileUpload[index].disable}
                              onChange={() => {
                                const objIndex = fileUpload.findIndex(
                                  (obj) => obj.id === idDoc
                                );

                                // make new object of updated object.
                                const updatedObj = {
                                  ...fileUpload[objIndex],
                                  disable: !fileUpload[index].disable,
                                };

                                // make final new array of objects by combining updated object.
                                const updatedProjects = [
                                  ...fileUpload.slice(0, objIndex),
                                  updatedObj,
                                  ...fileUpload.slice(objIndex + 1),
                                ];

                                setFileUpload(updatedProjects);
                              }}
                            />
                          }
                          label={
                            <p style={{ fontSize: 12 }}>
                              This document has no expiration date
                            </p>
                          }
                        />
                        {fileUpload[index].disable === false &&
                          fileUpload[index].valid_until === null && (
                            <p style={{ fontSize: 10, color: "red" }}>
                              * Valid until is required{" "}
                            </p>
                          )}
                      </td>
                    </tr>
                  );
                })}
            </table>

            {((!visibleUpload && fileUpload.length < 1) ||
              (!visibleUpload && fileUpload.length > 0)) && (
              <div
                style={{
                  width: "100%",
                  padding: 10,
                  textAlign: "center",
                  border: "1px solid #dddddd",
                }}
              >
                There is no file
              </div>
            )}
          </div>
        </DialogContent>
        <DialogActions>
          {/* <Button autoFocus onClick={handleClose}>
              Save changes
            </Button> */}
          <Button
            onClick={metadataMultipleUpload}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            disabled={!visibleSaveUpload}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>

      <BootstrapDialog
        onClose={handleCloseRevise}
        // aria-labelledby="customized-dialog-title"
        open={openRevise}
        maxWidth="xl"
        fullWidth
      >
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleCloseRevise}
        >
          Attachment Details
          {openRevise ? (
            <IconButton
              aria-label="close"
              onClick={handleCloseRevise}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent
          style={{ marginLeft: 15, marginRight: 15, overflow: "hidden" }}
          dividers
        >
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Card
                style={{
                  height: "60%",
                  overflow: "auto",
                  textAlign: "center",
                  boxShadow: "none",
                }}
              >
                {fileShow && fileType === "pdf" && (
                  <PDFViewer
                    navbarOnTop={true}
                    document={{
                      url: `https://dev-apigp.bki.co.id/v1/documents/fetch?path=${fileShow}&act=view&key=${key}`,
                    }}
                  />
                )}
                {fileShow &&
                  (fileType === "jpg" ||
                    fileType === "jpeg" ||
                    fileType === "png") && (
                    <img
                      src={`https://dev-apigp.bki.co.id/v1/documents/fetch?path=${fileShow}&act=view&key=${key}`}
                      loading="lazy"
                      style={{
                        justifyContent: "center",
                        alignSelf: "center",
                      }}
                    />
                  )}

                {fileShow &&
                  (fileType === "doc" ||
                    fileType === "docx" ||
                    fileType === "xls" ||
                    fileType === "xlsx" ||
                    fileType === "ppt" ||
                    fileType === "pptx") && (
                    <div
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        display: "flex",
                        height: "100%",
                        width: "100%",
                      }}
                    >
                      <img
                        src={
                          fileType === "doc" || fileType === "docx"
                            ? WORD
                            : fileType === "xls" || fileType === "xlsx"
                            ? EXCEL
                            : PPT
                        }
                        // alt={item.title}
                        loading="lazy"
                        // style="max-height: 100px; max-width: 100px;"
                        style={{
                          maxHeight: 300,
                          maxWidth: 300,
                          justifyContent: "center",
                          alignSelf: "center",
                        }}
                      />
                    </div>
                  )}
              </Card>
            </Grid>
            <Grid
              item
              xs={3}
              style={{
                border: "1",
                borderStyle: "none none none solid",
                borderColor: "#E5E8EB",
              }}
            >
              <Card
                style={{
                  padding: 5,
                  maxHeight: "40%",
                  overflow: "auto",
                  border: "none",
                  boxShadow: "none",
                  fontSize: 12,
                }}
              >
                {/* <hr style={{ marginTop: 0, marginBottom: 20 }} /> */}
                <p>
                  <b>Uploaded on:</b>{" "}
                  <span style={{ color: "#637381" }}>{created_at}</span>
                </p>
                <p>
                  <b>Uploaded by:</b>{" "}
                  <span style={{ color: "#637381" }}>{created_by}</span>
                </p>
                <p>
                  <b>File name:</b>{" "}
                  <span style={{ color: "#637381" }}>{doc_title}</span>
                </p>
                <p>
                  <b>File type:</b>{" "}
                  <span style={{ color: "#637381" }}>{fileType}</span>
                </p>
                <p>
                  <b>File size:</b>{" "}
                  <span style={{ color: "#637381" }}>{size + " Bytes"}</span>
                </p>

                <hr style={{ marginTop: 20, marginBottom: 20 }} />

                <TextField
                  required
                  autoFocus
                  margin="normal"
                  id="document_name"
                  label="Document Name"
                  type="text"
                  fullWidth
                  variant="outlined"
                  disabled={disable}
                  value={doc_title}
                  onChange={(e) => setDoc_title(e.target.value)}
                  size="small"
                />

                <TextField
                  required
                  autoFocus
                  margin="normal"
                  id="issuer"
                  label="Issuer"
                  type="text"
                  fullWidth
                  variant="outlined"
                  disabled={disable}
                  value={issuer}
                  onChange={(e) => setIssuer(e.target.value)}
                  size="small"
                />
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DatePicker
                    inputFormat="yyyy/MM/dd"
                    disabled={dataDisable}
                    label="Valid Until"
                    value={valid_until}
                    onChange={(newValue) => {
                      setValid_until(newValue);
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        margin="normal"
                        required
                        fullWidth
                        // disabled={disable}
                        size="small"
                      />
                    )}
                  />
                </LocalizationProvider>
                <FormControlLabel
                  control={
                    <Checkbox
                      size="small"
                      checked={dataDisable}
                      onChange={() => {
                        if (dataDisable) {
                          setDataDisable(false);
                        } else {
                          setDataDisable(true);
                        }
                      }}
                    />
                  }
                  label={
                    <p style={{ fontSize: 12 }}>
                      This document has no expiration date
                    </p>
                  }
                />

                <Autocomplete
                  disabled={disable}
                  value={valueAuto}
                  onChange={(event, newValue) => {
                    setValueAuto(newValue);
                  }}
                  inputValue={inputValueAuto}
                  onInputChange={(event, newInputValue) => {
                    setInputValueAuto(newInputValue);
                  }}
                  id="controllable-states-demo"
                  options={options}
                  // sx={{ width: 300 }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Type"
                      margin="normal"
                      required
                      fullWidth
                    />
                  )}
                  size="small"
                />

                <hr style={{ marginTop: 20, marginBottom: 20 }} />

                <Button
                  variant={visibleRevise ? "outlined" : "contained"}
                  color={visibleRevise ? "error" : "primary"}
                  onClick={() => setvisibleRevise(!visibleRevise)}
                >
                  {visibleRevise ? "Cancel" : "Update Document"}
                </Button>

                {visibleRevise && (
                  <>
                    <p style={{ textAlign: "center", marginTop: 20 }}>
                      <b>FORM DOCUMENT REVISION</b>
                    </p>

                    <TextField
                      required
                      autoFocus
                      margin="normal"
                      id="description"
                      label="Description"
                      type="text"
                      fullWidth
                      variant="outlined"
                      disabled={disable}
                      value={descRevise}
                      onChange={(e) => setDescRevise(e.target.value)}
                    />

                    <div style={{ marginTop: 15 }}>
                      <label
                        htmlFor="upload-photo"
                        style={{ color: "#667480" }}
                      >
                        Revision Documents
                      </label>
                      <br />
                      <input
                        type="file"
                        style={{ color: "#667480", marginTop: 10 }}
                        onChange={(e) => onFileChangeRevise(e)}
                      />
                    </div>
                    <Button
                      style={{ marginTop: 30, width: "100%" }}
                      variant="contained"
                      onClick={UploadRevise}
                    >
                      Upload Revision
                    </Button>
                  </>
                )}

                <fieldset
                  style={{
                    border: "1px solid silver",
                    padding: "5px",
                    overflow: "auto",
                    maxHeight: "320px",
                    marginTop: 30,
                  }}
                >
                  <legend style={{ color: "black", fontSize: 17 }}>
                    Revision History
                  </legend>
                  <div>
                    <Paper style={{ maxHeight: 220, overflow: "auto" }}>
                      <List
                        style={{
                          height: "70vh",
                          overflowY: "auto",
                          fontSize: "5px",
                        }}
                      >
                        {dataHistory.map((value) => {
                          return (
                            <ListItem
                              style={{
                                width: "100%",
                                color: "#1948A5",
                                cursor: "pointer",
                              }}
                            >
                              <ListItemText
                                onClick={() =>
                                  window.open(
                                    `https://dev-apigp.bki.co.id/v1/documents/fetch?path=${value.path}&act=download&key=${key}`
                                  )
                                }
                                // primary="filename_new2.pdf"
                                primary={
                                  <Typography
                                    variant="h6"
                                    gutterBottom
                                    component="div"
                                    style={{
                                      textAlign: "left",
                                      color: "#00AF5B",
                                      fontSize: "14px",
                                    }}
                                  >
                                    {`Revision ${value.revision}`}
                                  </Typography>
                                }
                                secondary={
                                  <Typography
                                    variant="p"
                                    gutterBottom
                                    component="div"
                                    style={{
                                      textAlign: "left",
                                      color: "silver",
                                      fontSize: "12px",
                                      marginTop: -10,
                                    }}
                                  >
                                    {`Issued Date: ${moment(
                                      value.created_at
                                    ).format("MM/DD/YYYY")}`}
                                  </Typography>
                                }
                                style={{
                                  color: "#1948A5",
                                  cursor: "pointer",
                                }}
                              />
                            </ListItem>
                          );
                        })}
                      </List>
                    </Paper>
                  </div>
                </fieldset>
              </Card>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions style={{ justifyContent: "space-between" }}>
          {/* <Button autoFocus onClick={handleClose}>
              Save changes
            </Button> */}
          <Button
            onClick={() =>
              window.open(
                `https://dev-apigp.bki.co.id/v1/documents/fetch?path=${fileShow}&act=download&key=${key}`
              )
            }
            variant="contained"
            color="info"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="bi:download" />}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginLeft: 20,
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            Download File
          </Button>
          <Button
            onClick={metadataSingleUpload}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>

      <Typography
        variant="h4"
        gutterBottom
        style={{ margin: 10, textAlign: "center" }}
      >
        {criteria}
      </Typography>
      {/* <hr style={{ margin: "-5px" }}></hr> */}
      <Typography
        variant="h6"
        style={{ margin: 10, color: "#00AF5B", textAlign: "center" }}
      >
        Aspek: {aspect}
      </Typography>
      <TableContainer component={Paper} style={{ borderRadius: 0 }}>
        <Table aria-label="collapsible table">
          <TableHead style={{ backgroundColor: "#254A7C" }}>
            <TableRow>
              <TableCell />
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Kode Kriteria
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Sub Kriteria
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Porsi
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Tolak Ukur
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Pemenuhan
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Catatan
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Nilai
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Persentase
              </TableCell>
              {/* <TableCell /> */}
            </TableRow>
          </TableHead>
          <TableBody>
            {/* {rows.map((row) => (
              <Row key={row.name} row={row} />
            ))} */}
            {data
              // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                const idDoc = data[index].id;
                return (
                  // <Row
                  //   key={row.name}
                  //   row={row}
                  //   keys={key}
                  //   // funct={GetAllAssessment()}
                  // />
                  <>
                    <TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
                      <TableCell>
                        <IconButton
                          aria-label="expand row"
                          size="small"
                          onClick={() => handleCollapse(row.id)}
                        >
                          {collapse.includes(row.id) ? (
                            <KeyboardArrowUpIcon />
                          ) : (
                            <KeyboardArrowDownIcon />
                          )}
                        </IconButton>
                      </TableCell>
                      <TableCell
                        component="th"
                        scope="row"
                        align="center"
                        style={{
                          color: "#00AF5B",
                          cursor: "pointer",
                          verticalAlign: "top",
                        }}
                      >
                        {row.code}
                      </TableCell>
                      <TableCell style={{ verticalAlign: "top" }}>
                        {row.criteria}
                      </TableCell>
                      <TableCell
                        style={{ textAlign: "center", verticalAlign: "top" }}
                      >
                        {row.master_weight}
                      </TableCell>
                      <TableCell style={{ verticalAlign: "top" }}>
                        {row.master_name}
                      </TableCell>
                      <TableCell
                        style={{ textAlign: "center", verticalAlign: "top" }}
                      >
                        {/* {row.compliance === null ? "N/a" : row.compliance} */}
                        <FormControl
                          // sx={{ m: 1, minWidth: 100 }}
                          size="small"
                          disabled={row.released === 1}
                        >
                          <Select
                            labelId="demo-simple-select-standard-label"
                            id="demo-simple-select-standard"
                            placeholder="-- Select --"
                            // style={{ width: '100px' }}
                            // label="Age"
                            value={data[index].compliance}
                            onChange={(e) => {
                              console.log(e.target.value);
                              const objIndex = data.findIndex(
                                (obj) => obj.id === idDoc
                              );

                              // make new object of updated object.
                              const updatedObj = {
                                ...data[objIndex],
                                compliance: e.target.value,
                              };

                              // make final new array of objects by combining updated object.
                              const updatedProjects = [
                                ...data.slice(0, objIndex),
                                updatedObj,
                                ...data.slice(objIndex + 1),
                              ];

                              setData(updatedProjects);
                              UpdateChecklist(e.target.value, row.id);
                            }}
                          >
                            {complience.map((item, index) => {
                              return (
                                <MenuItem value={item} title={item}>
                                  {item}
                                </MenuItem>
                              );
                            })}
                          </Select>
                        </FormControl>
                      </TableCell>
                      <TableCell
                        style={{
                          textAlign: "left",
                          width: "250px",
                          verticalAlign: "top",
                        }}
                      >
                        <TextField
                          style={{ marginTop: 0 }}
                          name="description"
                          margin="normal"
                          id="outlined-multiline-static"
                          // label="Description"
                          multiline
                          disabled={row.released === 1}
                          rows={3}
                          fullWidth
                          value={data[index].assessor_note}
                          onChange={(e) => {
                            const objIndex = data.findIndex(
                              (obj) => obj.id === idDoc
                            );

                            console.log(data[index].id + " || " + idDoc);

                            // make new object of updated object.
                            const updatedObj = {
                              ...data[objIndex],
                              assessor_note: e.target.value,
                            };

                            // make final new array of objects by combining updated object.
                            const updatedProjects = [
                              ...data.slice(0, objIndex),
                              updatedObj,
                              ...data.slice(objIndex + 1),
                            ];

                            setData(updatedProjects);
                          }}
                        />
                        <Button
                          variant="contained"
                          disabled={row.released === 1}
                          onClick={() =>
                            UpdateCatatan(data[index].assessor_note, row.id)
                          }
                          style={{ float: "right", width: 10 }}
                        >
                          <p style={{ fontSize: 10 }}>Update</p>
                        </Button>
                      </TableCell>
                      <TableCell
                        style={{ textAlign: "center", verticalAlign: "top" }}
                      >
                        {row.value === null ? "N/a" : row.value}
                      </TableCell>
                      <TableCell
                        style={{ textAlign: "center", verticalAlign: "top" }}
                      >
                        {row.percentage === null
                          ? "N/a"
                          : Number(row.percentage).toFixed(2)}
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell
                        style={{ paddingBottom: 0, paddingTop: 0 }}
                        colSpan={12}
                      >
                        <Collapse
                          in={collapse.includes(row.id)}
                          timeout="auto"
                          unmountOnExit
                        >
                          <Box sx={{ margin: 1 }}>
                            <Grid container spacing={3}>
                              <Grid item xs={6}>
                                <Card style={{ padding: 15, Height: "350px" }}>
                                  <Typography
                                    variant="h6"
                                    gutterBottom
                                    component="div"
                                    style={{
                                      textAlign: "center",
                                      color: "#00AF5B",
                                      fontSize: "14px",
                                    }}
                                  >
                                    Remark
                                  </Typography>
                                  <TextareaAutosize
                                    aria-label="minimum height"
                                    minRows={14}
                                    disabled
                                    placeholder="Narative Description"
                                    style={{
                                      width: "100%",
                                      padding: 5,
                                      borderColor: "silver",
                                      borderRadius: 5,
                                      resize: "none",
                                    }}
                                    value={data[index].note}
                                    onChange={(e) => {
                                      setRemark(e.target.value);
                                      data[index].note = e.target.value;
                                    }}
                                  />
                                </Card>
                              </Grid>
                              <Grid item xs={6}>
                                <Card
                                  style={{ padding: 15, maxHeight: "350px" }}
                                >
                                  <Typography
                                    variant="h6"
                                    gutterBottom
                                    component="div"
                                    style={{
                                      textAlign: "center",
                                      color: "#00AF5B",
                                      fontSize: "14px",
                                    }}
                                  >
                                    Comment
                                  </Typography>

                                  <Paper
                                    style={{ height: 242, overflow: "auto" }}
                                  >
                                    <div className={classes.imessage}>
                                      {row.comment !== [] &&
                                        row.comment.map((value, index) => (
                                          <>
                                            <p
                                              className={
                                                value.from === "int"
                                                  ? classes.from_me
                                                  : classes.from_them
                                              }
                                            >
                                              {value.comment}
                                              <br />
                                              <small
                                                style={{
                                                  float: "right",
                                                  color: "#5b5b5b",
                                                  // color:
                                                  //   value.from === "int"
                                                  //     ? "#86c5fc"
                                                  //     : "#5b5b5b",
                                                }}
                                              >
                                                {`${moment(
                                                  value.created_at
                                                ).format(
                                                  "DD MMMM YY hh:mm"
                                                )} | ${value.name}`}
                                              </small>
                                            </p>
                                          </>
                                        ))}
                                    </div>
                                    <hr style={{ marginBottom: 50 }} />
                                    <Grid
                                      container
                                      style={{
                                        padding: "20px",
                                        bottom: 0,
                                        left: 0,
                                        position: "absolute",
                                        backgroundColor: "white",
                                        // overflow: "scroll"
                                      }}
                                    >
                                      <Grid item xs={10}>
                                        <TextField
                                          id="outlined-basic-email"
                                          label="Send message"
                                          fullWidth
                                          size="small"
                                          disabled={row.released === 1}
                                          value={data[index].chat}
                                          onChange={(e) => {
                                            setChat(e.target.value);
                                            data[index].chat = e.target.value;
                                          }}
                                        />
                                      </Grid>
                                      <Grid
                                        xs={2}
                                        align="right"
                                        style={{
                                          // backgroundColor: "green",
                                          alignSelf: "center",
                                        }}
                                      >
                                        <Button
                                          disabled={row.released === 1}
                                          variant="contained"
                                          onClick={() => CommentChat(row.id)}
                                        >
                                          <Iconify
                                            icon={"fe:paper-plane"}
                                            sx={{
                                              width: 35,
                                              height: 20,
                                            }}
                                            color="#ffffff"
                                          />
                                        </Button>
                                      </Grid>
                                    </Grid>
                                  </Paper>
                                </Card>
                              </Grid>
                              <Grid item xs={12}>
                                <Card
                                  style={{
                                    padding: 15,
                                    maxHeight: "300px",
                                  }}
                                >
                                  <Stack
                                    direction="row"
                                    alignItems="center"
                                    justifyContent="space-between"
                                    mb={1}
                                  >
                                    <Typography
                                      variant="h6"
                                      gutterBottom
                                      component="div"
                                      style={{
                                        textAlign: "left",
                                        color: "#00AF5B",
                                        fontSize: "14px",
                                        marginTop: "-30px",
                                        marginBottom: "-20px",
                                      }}
                                    >
                                      List Evidence
                                    </Typography>
                                    {/* <Button
                                      variant="contained"
                                      onClick={() => {
                                        setOpen(true);
                                        setIdChecklist(row.id);
                                      }}
                                      style={{ float: "right" }}
                                      size="small"
                                    >
                                      Add New Evidence
                                    </Button> */}
                                  </Stack>
                                  <hr />
                                  <div
                                    style={{
                                      maxHeight: "200px",
                                      overflow: "auto",
                                    }}
                                  >
                                    <Table
                                      size="small"
                                      aria-label="purchases"
                                      style={{ fontSize: "12px" }}
                                    >
                                      <TableHead
                                        style={{
                                          backgroundColor: "grey",
                                          color: "#ffffff",
                                        }}
                                      >
                                        <TableRow>
                                          <TableCell
                                            style={{
                                              textAlign: "center",
                                              color: "#ffffff",
                                            }}
                                          >
                                            Document Name
                                          </TableCell>
                                          <TableCell
                                            style={{
                                              textAlign: "center",
                                              color: "#ffffff",
                                            }}
                                          >
                                            Issuer
                                          </TableCell>
                                          <TableCell
                                            style={{
                                              textAlign: "center",
                                              color: "#ffffff",
                                            }}
                                          >
                                            Valid Until
                                          </TableCell>
                                          <TableCell
                                            style={{
                                              textAlign: "center",
                                              color: "#ffffff",
                                            }}
                                          >
                                            Revision
                                          </TableCell>
                                          {/* <TableCell
                                            style={{
                                              textAlign: "center",
                                              color: "#ffffff"
                                            }}
                                          >
                                            Action
                                          </TableCell> */}
                                        </TableRow>
                                      </TableHead>
                                      <TableBody>
                                        {row.docs.map((historyRow) => (
                                          <TableRow key={historyRow.id}>
                                            <TableCell
                                              component="th"
                                              scope="row"
                                              style={{
                                                textAlign: "center",
                                                color: "blue",
                                                cursor: "pointer",
                                              }}
                                              onClick={() =>
                                                window.open(
                                                  `${
                                                    process.env
                                                      .REACT_APP_BASURL_TRX
                                                  }documents/fetch?path=${
                                                    historyRow.path
                                                  }&act=download&key=${GetCookie(
                                                    "key"
                                                  )}`
                                                )
                                              }
                                            >
                                              {`${historyRow.doc_title}.${historyRow.filetype}`}
                                            </TableCell>
                                            <TableCell
                                              style={{ textAlign: "center" }}
                                            >
                                              {historyRow.issuer === null
                                                ? "-"
                                                : historyRow.issuer}
                                            </TableCell>
                                            <TableCell
                                              style={{ textAlign: "center" }}
                                            >
                                              {historyRow.valid_until === null
                                                ? "-"
                                                : moment(
                                                    historyRow.valid_until
                                                  ).format("MM/DD/YYYY")}
                                            </TableCell>
                                            <TableCell
                                              style={{ textAlign: "center" }}
                                            >
                                              {historyRow.latest_revision}
                                            </TableCell>
                                            {/* <TableCell
                                              style={{
                                                textAlign: "center",
                                                flexDirection: "row"
                                              }}
                                            >
                                              <Button
                                                variant="contained"
                                                onClick={() =>
                                                  GetDocumentByID(historyRow)
                                                }
                                                color="warning"
                                                style={{ marginRight: 5 }}
                                              >
                                                <Iconify icon="ant-design:edit-outlined" />
                                              </Button>
                                              <Button
                                                variant="contained"
                                                onClick={() => alert("haha")}
                                                color="error"
                                                style={{ marginLeft: 5 }}
                                              >
                                                <Iconify icon="eva:checkmark-fill" />
                                              </Button>
                                            </TableCell> */}
                                          </TableRow>
                                        ))}
                                      </TableBody>
                                    </Table>
                                    {row.docs.length < 1 && (
                                      <div
                                        style={{
                                          padding: 10,
                                          width: "100%",
                                          textAlign: "center",
                                          border: "1px solid #dddddd",
                                        }}
                                      >
                                        There is no file
                                      </div>
                                    )}
                                  </div>
                                </Card>
                              </Grid>
                            </Grid>
                          </Box>
                        </Collapse>
                      </TableCell>
                    </TableRow>
                  </>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      {/* <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      /> */}
    </Card>
  );
}
