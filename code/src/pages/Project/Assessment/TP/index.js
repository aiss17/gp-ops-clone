import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { Link as RouterLink } from "react-router-dom";
import { useEffect, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { styled } from "@mui/material/styles";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  DialogTitle,
  IconButton,
  DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  DialogActions,
  Radio,
  Dialog,
  Collapse,
  Box
} from "@mui/material";
import Iconify from "../../../../components/Iconify";
import UserMoreMenuTE from "src/sections/@dashboard/user/UserMoreMenuTE";

function createData(kd, sk, porsi, tk, pemenuhan, nilai, presentase) {
  // const density = population / size;
  return {
    kd,
    sk,
    porsi,
    tk,
    pemenuhan,
    nilai,
    presentase,
    history: [
      {
        evidence: "filename.pdf",
        issuedDate: "05-01-2020",
        revision: 3,
        comment: "Lorem ipsum sastra widoyo",
        commentDate: "2020-01-05",
        commentBy: "username"
      },
      {
        evidence: "filename.pdf",
        issuedDate: "05-01-2020",
        revision: 1,
        comment: "Lorem ipsum sastra widoyo",
        commentDate: "2020-01-05",
        commentBy: "username"
      }
    ]
  };
}

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

const rows = [
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port States",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port Kingdom",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  ),
  createData(
    "KK-01",
    "Rencana pengembangan green port",
    0.2,
    "Memiliki kebijakan tertulis green port",
    "kurang",
    1,
    "2.0%"
  )
];

export default function StickyHeadTable() {
  const [page, setPage] = React.useState(0);

  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = useState(false);
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    setDisable(false);
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Card>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="sm"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Assessment Form
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500]
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={5}>
            <Grid item xs={12}>
              <TextField
                required
                autoFocus
                margin="normal"
                id="name"
                label="Requirement"
                type="text"
                fullWidth
                variant="outlined"
                defaultValue={"Terdapat penjual kaki lima disekitar port ?"}
                disabled
              />
            </Grid>
          </Grid>
          <Grid container spacing={5}>
            <Grid item xs={6}>
              <FormControl margin="normal">
                <FormLabel id="demo-controlled-radio-buttons-group">
                  Compliance
                </FormLabel>
                <RadioGroup
                  row
                  aria-labelledby="demo-controlled-radio-buttons-group"
                  name="controlled-radio-buttons-group"
                  value={valueRadio}
                  onChange={handleChange}
                >
                  <FormControlLabel
                    value="female"
                    control={<Radio />}
                    label="Yes"
                  />
                  <FormControlLabel
                    value="male"
                    control={<Radio />}
                    label="No"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={6}>
              <div style={{ marginTop: 15 }}>
                <label htmlFor="upload-photo" style={{ color: "#667480" }}>
                  Upload Evidence
                </label>
                <br />
                <input
                  type="file"
                  style={{ color: "#667480", marginTop: 10 }}
                />
              </div>
            </Grid>
          </Grid>

          <Grid container spacing={5}>
            <Grid item xs={6}>
              <TextField
                required
                name="port_address"
                margin="normal"
                id="outlined-multiline-static"
                label="Point"
                fullWidth
                disabled
                defaultValue={0}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          {/* <Button autoFocus onClick={handleClose}>
              Save changes
            </Button> */}
          <Button
            onClick={handleClose}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>

      <Typography variant="h4" gutterBottom style={{ margin: 20 }}>
        KOMITMEN DAN KEBIJAKAN PENERAPAN GREEN PORT
      </Typography>
      <hr></hr>
      <Typography variant="h6" style={{ margin: 20, color: "green" }}>
        Aspek: Manajemen
      </Typography>
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell align="center">Kode Kriteria</TableCell>
              <TableCell align="center">Sub Kriteria</TableCell>
              <TableCell align="center">Porsi</TableCell>
              <TableCell align="center">Tolak Ukur</TableCell>
              <TableCell align="center">Pemenuhan</TableCell>
              <TableCell align="center">Nilai</TableCell>
              <TableCell align="center">Persentase</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <Row key={row.name} row={row} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Card>
  );
}

function Row(props) {
  const { row } = props;
  const [openRow, setOpenRow] = useState(false);
  const [open, setOpen] = useState(false);
  const [disable, setDisable] = useState(false);

  const handleClose = () => {
    setOpen(false);
    setDisable(false);
  };

  return (
    <React.Fragment>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="sm"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Reply
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500]
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={5}>
            <Grid item xs={12}>
              <TextField
                required
                autoFocus
                margin="normal"
                id="name"
                label="Comment"
                type="text"
                fullWidth
                variant="outlined"
                disabled={disable}
                value={"Evidence Anda masih kurang"}
              />
            </Grid>
          </Grid>

          <Grid container spacing={5}>
            <Grid item xs={12}>
              <TextField
                required
                name="port_address"
                margin="normal"
                id="outlined-multiline-static"
                label="Reply Comment"
                multiline
                rows={4}
                fullWidth
              />
            </Grid>
          </Grid>

          {!disable && (
            <Grid item xs={6}>
              <div style={{ marginTop: 5 }}>
                <label
                  htmlFor="upload-photo"
                  style={{ color: "#667480", fontSize: 17 }}
                >
                  Upload Documents
                </label>
                <br />
                <input
                  type="file"
                  style={{ color: "#667480", marginTop: 10 }}
                />
              </div>
              {/* <TextField
                  required
                  autoFocus
                  name="upload-photo"
                  type="file"
                  margin="normal"
                  label="Upload Documents"
                  fullWidth
                  variant="outlined"
                /> */}
            </Grid>
          )}
        </DialogContent>
        <DialogActions>
          {/* <Button autoFocus onClick={handleClose}>
              Save changes
            </Button> */}
          <Button
            onClick={handleClose}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10
            }}
          >
            Submit
          </Button>
        </DialogActions>
      </BootstrapDialog>
      <TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpenRow(!openRow)}
          >
            {openRow ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row" align="center">
          {row.kd}
        </TableCell>
        <TableCell align="center">{row.sk}</TableCell>
        <TableCell align="center">{row.porsi}</TableCell>
        <TableCell align="center">{row.tk}</TableCell>
        <TableCell align="center">{row.pemenuhan}</TableCell>
        <TableCell align="center">{row.nilai}</TableCell>
        <TableCell align="center">{row.presentase}</TableCell>
        <TableCell>
          <UserMoreMenuTE
            onClickView={() => {
              setOpen(true);
              setDisable(true);
            }}
          />
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={12}>
          <Collapse in={openRow} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              {/* <Typography
                variant="h6"
                gutterBottom
                component="div"
                style={{ backgroundColor: "red", textAlign: "center" }}
              >
                History
              </Typography> */}
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell align="center">Evidence</TableCell>
                    <TableCell align="center">Issued Date</TableCell>
                    <TableCell align="center">Revision</TableCell>
                    <TableCell align="center">Comment</TableCell>
                    <TableCell align="center">Comment Date</TableCell>
                    <TableCell align="center">Comment By</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.history.map((historyRow) => (
                    <TableRow key={historyRow.issuedDate}>
                      <TableCell component="th" scope="row" align="center">
                        {historyRow.evidence}
                      </TableCell>
                      <TableCell component="th" scope="row" align="center">
                        {historyRow.issuedDate}
                      </TableCell>
                      <TableCell>{historyRow.revision}</TableCell>
                      <TableCell align="center">{historyRow.comment}</TableCell>
                      <TableCell align="center">
                        {historyRow.commentDate}
                      </TableCell>
                      <TableCell align="center">
                        {historyRow.commentBy}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}
