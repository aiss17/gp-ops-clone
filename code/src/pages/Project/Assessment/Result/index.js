import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { Link as RouterLink, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { styled } from "@mui/material/styles";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import DatePicker from "@mui/lab/DatePicker";
import TextareaAutosize from "@mui/material/TextareaAutosize";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  DialogTitle,
  IconButton,
  DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  DialogActions,
  Radio,
  Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab,
  Autocomplete,
  Select,
  MenuItem,
} from "@mui/material";
import Iconify from "../../../../components/Iconify";
import UserMoreMenuTE from "src/sections/@dashboard/user/UserMoreMenuTE";
// import filepdf from "../../../../assets/PDF.pdf";
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import moment from "moment";
import { LocalizationProvider } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import PDFViewer from "pdf-viewer-reactjs";
import {
  BMP,
  EXCEL,
  JPEG,
  JPG,
  PDF,
  PNG,
  PPT,
  WORD,
} from "../../../../assets/images";
import { LoadingData } from "src/pages";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  chatSection: {
    width: "100%",
    height: "80vh",
  },
  headBG: {
    backgroundColor: "#e0e0e0",
  },
  borderRight500: {
    borderRight: "1px solid #e0e0e0",
  },
  messageArea: {
    height: "70vh",
    overflowY: "auto",
    fontSize: "5px",
  },
  imessage: {
    backgroundColor: "#fff",
    // border: "1px solid #e5e5ea",
    // borderRadius: "0.25rem",
    display: "flex",
    flexDirection: "column",
    fontSize: "12px",
    margin: "0 auto 1rem",
    maxWidth: "600px",
    padding: "0.5rem 1.5rem",
  },
  imessage_p: {
    borderRadius: "1.15rem",
    lineHeight: "1.25",
    maxWidth: "75%",
    padding: "0.5rem .875rem",
    position: "relative",
    wordWrap: "break-word",
  },

  from_me: {
    alignSelf: "flex-end",
    backgroundColor: "#248bf5",
    color: "#fff",
    margin: 5,
    padding: 10,
    borderRadius: 10,
  },

  from_them: {
    alignItems: "flex-start",
    backgroundColor: "#e5e5ea",
    color: "#3a3a3a",
    margin: 5,
    padding: 10,
    borderRadius: 10,
    maxWidth: "70%",
  },
});

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

export default function Result(props) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [key, setKey] = useState("");
  const [data, setData] = useState([]);
  const [properties, setProperties] = useState(null);
  const [loading, setLoading] = useState(false);
  const [statusRelease, setStatusRelease] = useState(null);

  const classes = useStyles();
  const { id } = useParams();

  useEffect(() => {
    console.log("ini props nya loohhh => ", props.id_project);
    GetNCOFI();
    return () => {};
  }, []);

  const GetNCOFI = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetNCOFI,
      { project_id: id },
      "GET"
    );
    setKey(GetCookie("key"));
    console.log("dapat data order", response.finalResponse.data);
    setData(response.finalResponse.data.nc);
    if (response.finalResponse.data.nc.length > 0) {
      setStatusRelease(response.finalResponse.data.nc[0].released);
    }
    if (response.finalResponse.data.approval.length > 0) {
      setProperties(response.finalResponse.data.approval[0]);
    }
    setLoading(false);
  };

  const ReleaseNC = async () => {
    setLoading(true);
    const payload = {
      project: id,
      released: statusRelease === 0 ? 1 : 0,
    };

    const response = await callApi(ENDPOINT.ReleaseNC, payload, "POST");

    console.log("Coba deh => ", response.finalResponse);

    GetNCOFI();
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Card>
      <TableContainer component={Paper} style={{ borderRadius: 0 }}>
        <Table aria-label="collapsible table">
          <TableHead style={{ backgroundColor: "#254A7C" }}>
            <TableRow>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Kriteria
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Kode Kriteria
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Sub Kriteria
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Nilai
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Persentase
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Status
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Keterangan
              </TableCell>
              {/* <TableCell /> */}
            </TableRow>
          </TableHead>
          <TableBody>
            {data
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                const idDoc = data[index].id;
                return (
                  <>
                    <TableRow
                      sx={{ "& > *": { borderBottom: "unset" } }}
                      key={index}
                    >
                      <TableCell style={{ textAlign: "left" }}>
                        {row.criteria}
                      </TableCell>
                      <TableCell style={{ textAlign: "center" }}>
                        {row.code}
                      </TableCell>
                      <TableCell>{row.sub_criteria}</TableCell>
                      <TableCell style={{ textAlign: "center" }}>
                        {row.master_weight}
                      </TableCell>
                      <TableCell style={{ textAlign: "center" }}>
                        {Number(row.percentage * 100).toFixed(2)} %
                      </TableCell>
                      <TableCell style={{ textAlign: "center" }}>
                        {row.status}
                      </TableCell>
                      <TableCell style={{ textAlign: "center" }}>
                        {row.note === null ? "-" : row.note}
                      </TableCell>
                    </TableRow>
                  </>
                );
              })}
          </TableBody>

          {data.length < 1 && (
            <TableBody>
              <TableRow>
                <TableCell
                  style={{
                    padding: 10,
                    textAlign: "center",
                    border: "1px solid #dddddd",
                  }}
                  colSpan={7}
                >
                  There is no data
                </TableCell>
              </TableRow>
            </TableBody>
          )}
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      <hr />

      <table width="100%">
        <tr>
          <td width="3%">{/* <Checkbox justifyContent="flex-start" /> */}</td>
          <td
            width="77%"
            style={{
              padding: 10,
              fontSize: "14px",
              color: properties === null ? "red" : "green",
            }}
          >
            {properties === null
              ? "Customer has not approved the results of the assessment carried out by the IDSurvey team."
              : `${properties.approved_by} from ${
                  properties.company_name
                } on ${moment(properties.created_at).format(
                  "DD/MM/yyyy"
                )} stated that it has agreed with the results of the assessment carried out by the ID Survey team`}
          </td>
          <td width="3%">{/* <Checkbox justifyContent="flex-start" /> */}</td>
        </tr>
        <tr>
          <td width="3%">{/* <Checkbox justifyContent="flex-start" /> */}</td>
          <td
            colspan="2"
            width="15%"
            style={{ padding: 10, textAlign: "left" }}
          >
            <Button
              disabled={properties !== null}
              variant="contained"
              startIcon={<Iconify icon="bxs:rocket" />}
              style={{
                boxShadow: "none",
                marginBottom: 20,
                backgroundColor: "#c32443",
              }}
              size="small"
              onClick={() => {
                // GenerateFileReport();
                if (
                  window.confirm(
                    `Are you sure you want to ${
                      statusRelease === 0 ? "release" : "unrelease"
                    } this report? This report will be overwritten`
                  )
                ) {
                  ReleaseNC();
                  console.log("success");
                } else {
                  // cancel
                  console.log("cancel");
                }
              }}
            >
              {statusRelease === 0 ? "Release" : "Unrelease"}
            </Button>
          </td>
          <td width="3%">{/* <Checkbox justifyContent="flex-start" /> */}</td>
        </tr>
      </table>
    </Card>
  );
}
