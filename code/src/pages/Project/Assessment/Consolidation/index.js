import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { Link as RouterLink, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { styled } from "@mui/material/styles";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import DatePicker from "@mui/lab/DatePicker";
import TextareaAutosize from "@mui/material/TextareaAutosize";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  DialogTitle,
  IconButton,
  DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  DialogActions,
  Radio,
  Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab,
  Autocomplete,
  Select,
  MenuItem
} from "@mui/material";
import Iconify from "../../../../components/Iconify";
import UserMoreMenuTE from "src/sections/@dashboard/user/UserMoreMenuTE";
// import filepdf from "../../../../assets/PDF.pdf";
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import moment from "moment";
import { LocalizationProvider } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import PDFViewer from "pdf-viewer-reactjs";
import {
  BMP,
  EXCEL,
  JPEG,
  JPG,
  PDF,
  PNG,
  PPT,
  WORD
} from "../../../../assets/images";
import { LoadingData } from "src/pages";

const useStyles = makeStyles({
  table: {
    minWidth: 650
  },
  chatSection: {
    width: "100%",
    height: "80vh"
  },
  headBG: {
    backgroundColor: "#e0e0e0"
  },
  borderRight500: {
    borderRight: "1px solid #e0e0e0"
  },
  messageArea: {
    height: "70vh",
    overflowY: "auto",
    fontSize: "5px"
  },
  imessage: {
    backgroundColor: "#fff",
    // border: "1px solid #e5e5ea",
    // borderRadius: "0.25rem",
    display: "flex",
    flexDirection: "column",
    fontSize: "12px",
    margin: "0 auto 1rem",
    maxWidth: "600px",
    padding: "0.5rem 1.5rem"
  },
  imessage_p: {
    borderRadius: "1.15rem",
    lineHeight: "1.25",
    maxWidth: "75%",
    padding: "0.5rem .875rem",
    position: "relative",
    wordWrap: "break-word"
  },

  from_me: {
    alignSelf: "flex-end",
    backgroundColor: "#248bf5",
    color: "#fff",
    margin: 5,
    padding: 10,
    borderRadius: 10
  },

  from_them: {
    alignItems: "flex-start",
    backgroundColor: "#e5e5ea",
    color: "#3a3a3a",
    margin: 5,
    padding: 10,
    borderRadius: 10,
    maxWidth: "70%"
  }
});

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

export default function StickyHeadTable(props) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [hasilRating, setHasilRating] = useState("");
  const [key, setKey] = useState("");
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const classes = useStyles();
  const { id } = useParams();

  useEffect(() => {
    console.log("ini props nya loohhh => ", props.id_project);
    GetConsolidation();
    return () => {};
  }, []);

  const GetConsolidation = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetConsolidation,
      { project_id: id },
      "GET"
    );
    setKey(GetCookie("key"));
    //console.log("dapat data order",response.finalResponse.data);
    setHasilRating(response.finalResponse.data.final_score);
    setData(response.finalResponse.data.consolidation);
    setLoading(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Card>
      <Typography
        variant="h6"
        gutterBottom
        style={{ marginLeft: 25, marginTop: 20, fontSize: "16px" }}
      >
        Hasil Rating : <span style={{ color: "green" }}>{Number(hasilRating * 100).toFixed(2)} %</span>
      </Typography>
      {/* <hr /> */}
      <TableContainer component={Paper} style={{ borderRadius: 0 }}>
        <Table aria-label="collapsible table">
          <TableHead style={{ backgroundColor: "#254A7C" }}>
            <TableRow>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                No
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Kategori
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Kode Kriteria
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Nama Kriteria
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Pembobotan (%)
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Hasil Bobot (%)
              </TableCell>
              <TableCell style={{ color: "#ffffff", textAlign: "center" }}>
                Level Pencapaian (%)
              </TableCell>
              {/* <TableCell /> */}
            </TableRow>
          </TableHead>
          <TableBody>
            {data
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                const idDoc = data[index].id;
                return (
                  <>
                    <TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
                      <TableCell
                        // style={{ color: "#00AF5B", cursor: "pointer" }}
                        style={{ textAlign: "center" }}
                      >
                        {index + 1}
                      </TableCell>
                      <TableCell>{row.aspect}</TableCell>
                      <TableCell>{row.criteria_code}</TableCell>
                      <TableCell>
                        {/* {row.value === null ? "N/a" : row.value} */}
                        {row.criteria_name}
                      </TableCell>
                      <TableCell style={{ textAlign: "center" }}>
                        {row.weight}
                      </TableCell>
                      <TableCell style={{ textAlign: "center" }}>
                        {row.weighted_score}
                      </TableCell>
                      <TableCell style={{ textAlign: "center" }}>
                        {row.score}
                      </TableCell>
                    </TableRow>
                  </>
                );
              })}
          </TableBody>

          {data.length < 1 && (
            <TableBody>
              <TableRow>
                <TableCell
                  style={{
                    padding: 10,
                    textAlign: "center",
                    border: "1px solid #dddddd"
                  }}
                  colSpan={7}
                >
                  There is no data
                </TableCell>
              </TableRow>
            </TableBody>
          )}
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Card>
  );
}
