import { Button, List, ListItem } from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Editor from "src/components/Editor";
import Page from "src/components/Page";
import { LoadingData } from "src/pages";
import { ENDPOINT } from "src/services/Constants";
import { callApi, GetCookie } from "src/services/Functions";
import { underconstruction } from "../../../../assets/images";

export default function ReportPreview() {
  const [visibleOnlyOffice, setVisibleOnlyOffice] = useState(false);
  const [urlReport, setUrlReport] = useState(null);
  const [loading, setLoading] = useState(false);

  const params = useParams();

  useEffect(() => {
    GetFileReport();

    return () => {};
  }, []);

  const GenerateFileReport = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GenerateFileReport,
      { project: params.id },
      "GET"
    );

    console.log("Coba deh => ", response.finalResponse.status);

    if (response.finalResponse.status === "success") {
      GetFileReport();
    } else {
      setLoading(false);
    }
  };

  const GenerateToPDF = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GenerateToPDF,
      { project: params.id },
      "GET"
    );
    setLoading(false);
  };

  const GetFileReport = async () => {
    setLoading(true);
    const response = await callApi(
      ENDPOINT.GetFileReport,
      { project: params.id, extension: "docx" },
      "GET"
    );

    console.log("Check response get file => ", response.finalResponse);
    if (response.finalResponse.status !== "empty") {
      setUrlReport(
        `${process.env.REACT_APP_BASURL_TRX}project/report/fetch?project=${
          params.id
        }&key=${GetCookie("key")}&extension=docx`
      );

      setVisibleOnlyOffice(false);

      setTimeout(() => {
        setLoading(false);
        setVisibleOnlyOffice(true);
      }, 500);
    } else {
      setLoading(false);
      setVisibleOnlyOffice(false);
    }
  };

  if (loading) {
    return <LoadingData />;
  }

  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      {/* <List style={{ marginTop: `auto` }}>
        <ListItem>
          <img
            src={underconstruction}
            style={{ width: "50%", marginLeft: "25%" }}
            alt="logo"
          />
        </ListItem>
      </List> */}
      <Button
        variant="contained"
        // startIcon={<Iconify icon="mdi:check-bold" />}
        style={{
          boxShadow: "none",
          marginBottom: 20,
          backgroundColor: "#254A7C"
        }}
        size="small"
        onClick={() => {
          // GenerateFileReport();
          if (
            window.confirm(
              "Are you sure you want to generate this report? This report will be overwritten"
            )
          ) {
            GenerateFileReport();
            console.log("success");
          } else {
            // cancel
            console.log("cancel");
          }
        }}
      >
        Generate Report
      </Button>
      <Button
        variant="contained"
        // startIcon={<Iconify icon="mdi:check-bold" />}
        style={{
          boxShadow: "none",
          marginBottom: 20,
          marginLeft: 20,
          backgroundColor: "red"
        }}
        size="small"
        onClick={() => {
          // GenerateFileReport();
          if (
            window.confirm(
              "Are you sure you want to generate this report to PDF?"
            )
          ) {
            GenerateToPDF();
            console.log("success");
          } else {
            // cancel
            console.log("cancel");
          }
        }}
      >
        Generate to PDF
      </Button>

      {visibleOnlyOffice && (
        <div style={{ height: window.innerHeight }}>
          <Editor
            fileType={"docx"}
            title={"Test"}
            url={urlReport}
            userData={params.id}
          />
          {/* hahaha */}
        </div>
      )}
    </Page>
  );
}
