import { List, ListItem } from "@mui/material";
import Page from "src/components/Page";
import { underconstruction } from "../../../../assets/images";

export default function Consolidation() {
  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      <List style={{ marginTop: `auto` }}>
        <ListItem>
          <img
            src={underconstruction}
            style={{ cursor: "pointer", width: "100%" }}
            alt="logo"
          />
        </ListItem>
      </List>
    </Page>
  );
}
