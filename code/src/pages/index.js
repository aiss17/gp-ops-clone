import Dashboard from "./Dashboard";
import Order from "./Order";
import {
  Assessment,
  Document,
  DetailSurvey,
  Meeting,
  Overview,
  ProjectList,
  RemoteSurvey,
  Report,
  TabProject,
  TechnicalEnquiry,
  VisitDetails
} from "./Project";
// import Certificate from "./Certificate";
import {
  Checklist,
  Customer,
  Client,
  ClientDetail,
  CustomerDetails
} from "./Masterdata";
import AuthLoading from "./AuthLoading";
import Certificate from "./Certificate";
// import {
//   Assessment,
//   Document,
//   Meeting,
//   TechnicalEnquiry,
//   RemoteSurvey,
//   ReviewApprove,
//   SetupPlan,
//   SetupTeam,
// } from "./Project 2";
import LoadingData from "./Loading";
import SelfAssessmentManagement from "./SelfAssessManagement";
import SelfAssessmentDetail from "./SelfAssessManagement/selfAssessDetail";
import PreviewCertificate from "./PreviewCertificate";

export {
  Dashboard,
  Order,
  DetailSurvey,
  Overview,
  ProjectList,
  Report,
  TabProject,
  VisitDetails,
  Checklist,
  Customer,
  Client,
  Assessment,
  Document,
  Meeting,
  TechnicalEnquiry,
  RemoteSurvey,
  ClientDetail,
  AuthLoading,
  Certificate,
  CustomerDetails,
  LoadingData,
  SelfAssessmentManagement,
  SelfAssessmentDetail,
  PreviewCertificate
};
