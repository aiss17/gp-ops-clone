import { ThreeBounce } from "better-react-spinkit";
import React, { useEffect, useState } from "react";

const Loading = () => {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        // justifyContent: "center",
        top: "50%",
        left: "50%",
        textAlign: "center"
      }}
    >
      <ThreeBounce size={15} color="blue" />
    </div>
  );
};

export default Loading;
