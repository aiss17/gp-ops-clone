import { useEffect, useState } from "react";
import { Link as RouterLink, useNavigate, useLocation } from "react-router-dom";
import { callApi } from "src/services/Functions";

export default function AuthLoading() {
  const navigate = useNavigate();

  useEffect(() => {
    navigate("login", { replace: true });

    return () => {};
  }, []);

  return <div></div>;
}
