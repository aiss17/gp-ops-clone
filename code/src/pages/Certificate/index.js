import { filter } from "lodash";
import { sentenceCase } from "change-case";
import { useEffect, useState } from "react";
import { Link, Link as RouterLink, useNavigate } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  // TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid,
  Input,
  InputAdornment,
  FormControl,
  InputLabel,
  OutlinedInput
} from "@mui/material";
import moment from "moment";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

// components
import Page from "../../components/Page";
import Label from "../../components/Label";
import Scrollbar from "../../components/Scrollbar";
import Iconify from "../../components/Iconify";
import SearchNotFound from "../../components/SearchNotFound";
import {
  MoreMenu,
  UserListHead,
  UserListToolbar,
  UserMoreMenu
} from "../../sections/@dashboard/user";

// table
import TableHead from "@mui/material/TableHead";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";

// api
import {
  callApi,
  GetCookie,
  refreshTokenApi,
  SetCookie
} from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { LoadingData } from "src/pages";

// ----------------------------------------------------------------------

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff",
    textAlign: "center"
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}));

const TABLE_HEAD = [
  { id: "no", label: "No. ", alignRight: false },
  { id: "name", label: "Document Name", alignRight: false },
  { id: "type", label: "Document Type", alignRight: false },
  { id: "category", label: "Document Category", alignRight: false },
  { id: "file", label: "File", alignRight: false },
  { id: "startDate", label: "Created Date", alignRight: false },
  { id: "" }
];

// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.company.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Certificate() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("company");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [open, setOpen] = useState(false);
  // const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [data, setData] = useState([]);
  const [detailuser, setListuser] = useState([]);
  const [user_id, setuseridsingle] = useState("");
  const [nama_user, setNamauser] = useState("");
  const [detaiOrder, setdetailOrder] = useState([]);
  const [order_number, setOrderNo] = useState("");
  const [order_loc, setOrderLoc] = useState("");
  const [loading, setLoading] = useState(false);

  // default set variabel input
  const [key, setKey] = useState("");

  // api
  const [orderID, setOrderID] = useState("");
  const [projectName, setProjectName] = useState("");
  const [projectValue, setProjectValue] = useState("");
  const [contractNo, setContractNo] = useState("");
  const [startDate, setStartDate] = useState(null);
  const [dueDate, setDueDate] = useState(null);
  const [description, setDescription] = useState("");

  // kiriman
  const [certificateNumber, setCertificateNumber] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [rating, setRating] = useState("");
  const [code, setCode] = useState("");
  const [type, setType] = useState("");
  const [signedBy1, setSignedBy1] = useState("");
  const [signedBy2, setSignedBy2] = useState("");
  const [issuedAt, setIssuedAt] = useState("");
  const [issuedBy, setIssuedBy] = useState("");
  const [city, setCity] = useState("");
  const [docs, setDocs] = useState(null);
  const [docTitle, setDocTitle] = useState("");

  useEffect(() => {
    GetAllCertificate();
    return () => {};
  }, []);

  const GetAllCertificate = async () => {
    setLoading(true);

    const response = await callApi(ENDPOINT.GetAllCertificate, null, "GET");
    setKey(GetCookie("key"));
    //console.log("dapat data project",response.finalResponse.data);
    setData(response.finalResponse.data);

    setLoading(false);
  };

  const InsertCertificate = async () => {
    // const formData = new FormData();
    setKey(GetCookie("key"));
    const payload = new FormData();

    payload.append("no_cert", certificateNumber);
    payload.append("company", companyName);
    payload.append("rating", rating);
    payload.append("code", code);
    payload.append("type", type);
    payload.append("signed_by1", signedBy1);
    payload.append("signed_by2", signedBy2);
    payload.append("issued_at", issuedAt);
    payload.append("issued_by", issuedBy);
    payload.append("city", city);
    payload.append("docs", docs);
    payload.append("doc_title", docTitle);

    const response = await callApi(
      ENDPOINT.InsertCertificate,
      payload,
      "UPLOAD"
    );

    //setOrderID("");
    initiateState();

    GetAllCertificate();
  };

  const initiateState = () => {
    setCertificateNumber("");
    setCompanyName("");
    setRating("");
    setCode("");
    setType("");
    setSignedBy1("");
    setSignedBy2("");
    setIssuedAt("");
    setIssuedBy("");
    setCity("");
    setDocs(null);
    setDocTitle("");
  };

  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setDisable(false);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.company);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    console.log(selectedIndex);
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const navigate = useNavigate();

  return (
    <Page title="Project Management | My Green Port" style={{ margin: 10 }}>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={1}
      >
        <Typography variant="h4" gutterBottom>
          List Certificate
        </Typography>
        <Button
          variant="contained"
          component={RouterLink}
          to="#"
          startIcon={<Iconify icon="eva:plus-fill" />}
          // onClick={handleClickOpen}
          onClick={function (event) {
            handleClickOpen();
          }}
          style={{
            backgroundColor: "#254A7C",
            boxShadow: "none"
            // "&:hover": { color: "red" },
          }}
        >
          New Project
        </Button>
      </Stack>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="lg"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          New Certificate
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500]
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <h4 style={{ marginBottom: "-15px" }}>No. Certificate</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="Certificate number"
                onChange={(e) => setCertificateNumber(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>Document Title</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="Document title"
                onChange={(e) => setDocTitle(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>Company Name</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="Company name"
                onChange={(e) => setCompanyName(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>Rating</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="Rating"
                onChange={(e) => setRating(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>Code</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="Code"
                onChange={(e) => setCode(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>Type</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="Type"
                onChange={(e) => setType(e.target.value)}
              />
            </Grid>
            <Grid item xs={6}>
              <h4 style={{ marginBottom: "-15px" }}>Signed By 1</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="Signed by 1"
                onChange={(e) => setSignedBy1(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>Signed By 2</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="Signed by 2"
                onChange={(e) => setSignedBy2(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>Issued Date</h4>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  disabled={disable}
                  // label="Requested Date"
                  value={issuedAt}
                  onChange={(newValue) => {
                    let text = newValue.toLocaleDateString("en-US");
                    setIssuedAt(text);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      margin="normal"
                      required
                      fullWidth
                      size="small"
                    />
                  )}
                />
              </LocalizationProvider>

              <h4 style={{ marginBottom: "-15px" }}>Issued By</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="Issued by"
                onChange={(e) => setIssuedBy(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>City</h4>
              <TextField
                margin="normal"
                id="name"
                type="text"
                fullWidth
                variant="outlined"
                size="small"
                placeholder="City"
                onChange={(e) => setCity(e.target.value)}
              />

              <h4 style={{ marginBottom: "-15px" }}>Upload Certificate</h4>
              <TextField
                margin="normal"
                id="name"
                type="file"
                fullWidth
                variant="outlined"
                size="small"
                onChange={(e) => setDocs(e.target.files[0])}
                inputProps={{ accept: "application/pdf" }}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={function (event) {
              handleClose();
              InsertCertificate();
            }}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
              backgroundColor: "#254A7C",
              boxShadow: "none"
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>
      <Card>
        <UserListToolbar
          numSelected={selected.length}
          filterName={filterName}
          onFilterName={handleFilterByName}
        />

        <Scrollbar>
          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <StyledTableCell>No. Certificate</StyledTableCell>
                  <StyledTableCell>Company Name</StyledTableCell>
                  <StyledTableCell>City</StyledTableCell>
                  <StyledTableCell>Rating</StyledTableCell>
                  <StyledTableCell>Action</StyledTableCell>
                </TableRow>
              </TableHead>
              {loading ? (
                <TableBody>
                  <TableRow>
                    <TableCell align="center" colSpan={9} sx={{ py: 3 }}>
                      <LoadingData />
                    </TableCell>
                  </TableRow>
                </TableBody>
              ) : (
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      //   const { id, name, role, status, company, avatarUrl, isVerified } = row;
                      //   const isItemSelected = selected.indexOf(name) !== -1;
                      const { id, no_cert, company, city, rating } = row;
                      const isItemSelected = selected.indexOf(orderID) !== -1;

                      return (
                        <TableRow>
                          <TableCell style={{ textAlign: "center" }}>
                            {no_cert !== "" ? no_cert.toUpperCase() : "-"}
                          </TableCell>
                          <TableCell style={{ textAlign: "center" }}>
                            {company !== "" ? company.toUpperCase() : "-"}
                          </TableCell>
                          <TableCell style={{ textAlign: "center" }}>
                            {city !== "" ? city.toUpperCase() : "-"}
                          </TableCell>
                          <TableCell style={{ textAlign: "center" }}>
                            {rating}
                          </TableCell>
                          <TableCell style={{ textAlign: "center" }}>
                            <Button
                              variant="contained"
                              // component={RouterLink}
                              // to={`/view`}
                              startIcon={<Iconify icon="bx:detail" />}
                              style={{
                                backgroundColor: "#00AF5B",
                                boxShadow: "none"
                                // "&:hover": { color: "red" },
                              }}
                              onClick={() =>
                                navigate("/gp-ops/view", {
                                  state: { id }
                                })
                              }
                            >
                              Detail
                            </Button>
                          </TableCell>
                        </TableRow>
                      );
                    })}

                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}

                  {isUserNotFound && (
                    <TableRow>
                      <TableCell align="center" colSpan={9} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  )}
                </TableBody>
              )}
            </Table>
          </TableContainer>
        </Scrollbar>

        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Page>
  );
}
