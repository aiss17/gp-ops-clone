import { filter } from "lodash";
import { sentenceCase } from "change-case";
import { useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid,
} from "@mui/material";
import moment from "moment";

import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
// components
import Page from "../../../components/Page";
import Label from "../../../components/Label";
import Scrollbar from "../../../components/Scrollbar";
import Iconify from "../../../components/Iconify";
import SearchNotFound from "../../../components/SearchNotFound";
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from "../../../sections/@dashboard/user";
//
import { ORDERS } from "../../../_mocks_/user";
// table
import TableHead from "@mui/material/TableHead";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import { callApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { sha512 } from "js-sha512";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const columns = [
  { id: "username", label: "Username", width: "120px", align: "center" },
  { id: "fullname", label: "Full Name", minWidth: "150px" },
  {
    id: "email",
    label: "Email",
    minWidth: "80px",
    align: "right",
  },
  {
    id: "company",
    label: "Company",
    minWidth: "80px",
    align: "right",
  },
  {
    id: "telephone",
    label: "Telephone",
    minWidth: "80px",
    align: "right",
  },
  {
    id: "hp",
    label: "HP / WA",
    minWidth: "80px",
    align: "right",
  },
];

function createData(username, fullname, email, company, telephone, hp) {
  return { username, fullname, email, company, telephone, hp };
}

const rows = [
  createData(
    "Abhasses",
    "Test Full Name",
    "abhasses@duasatu.com",
    "PT XYZ DESSJU GROVE",
    "021 - 5674984",
    "081992809120"
  ),
  createData(
    "Abhasses",
    "Test Full Name",
    "abhasses@duasatu.com",
    "PT XYZ DESSJU GROVE",
    "021 - 5674984",
    "081992809120"
  ),
  createData(
    "Abhasses",
    "Test Full Name",
    "abhasses@duasatu.com",
    "PT XYZ DESSJU GROVE",
    "021 - 5674984",
    "081992809120"
  ),
  createData(
    "Abhasses",
    "Test Full Name",
    "abhasses@duasatu.com",
    "PT XYZ DESSJU GROVE",
    "021 - 5674984",
    "081992809120"
  ),
  createData(
    "Abhasses",
    "Test Full Name",
    "abhasses@duasatu.com",
    "PT XYZ DESSJU GROVE",
    "021 - 5674984",
    "081992809120"
  ),
  createData(
    "Abhasses",
    "Test Full Name",
    "abhasses@duasatu.com",
    "PT XYZ DESSJU GROVE",
    "021 - 5674984",
    "081992809120"
  ),
  createData(
    "Abhasses",
    "Test Full Name",
    "abhasses@duasatu.com",
    "PT XYZ DESSJU GROVE",
    "021 - 5674984",
    "081992809120"
  ),
  createData(
    "Abhasses",
    "Test Full Name",
    "abhasses@duasatu.com",
    "PT XYZ DESSJU GROVE",
    "021 - 5674984",
    "081992809120"
  ),
];
// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user.username.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Order() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("username");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [open, setOpen] = useState(false);
  const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [valueRadio, setValueRadio] = useState("");
  const [data, setData] = useState([]);
  const [dataCompany, setDataCompany] = useState([]);
  const [companyNames, setCompanyNames] = useState([]);
  const [companyName, setCompanyName] = useState("");

  // payload
  const [nama, setNama] = useState("");
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");
  const [email, setEmail] = useState("");
  const [company_phone, setCompany_phone] = useState("");
  const [mobile_phone, setMobile_phone] = useState("");
  //const [app, setApp] = useState("29ac790b5fdb251a682c3db72fadcc1f");
  const [company_id, setCompany_id] = useState("");

  useEffect(() => {
    console.log(ORDERS);
    GetAllUsers();

    return () => {};
  }, []);

  const GetAllUsers = async () => {
    const payload = {
      app: process.env.REACT_APP_KEY_COMPANY,
    };

    const response = await callApi(ENDPOINT.GetClientMyGp, payload, "GET");
    //console.log("sapi",response);
    setData(response.finalResponse.data);
    setNama("");
    setUsername("");
    setPass("");
    setEmail("");
    setCompany_phone("");
    setMobile_phone("");
    setCompany_id("");
    setCompanyName("");
  };

  const InsertClient = async () => {
    const payload = {
      app: process.env.REACT_APP_KEY_COMPANY,
      nama: nama,
      username: username,
      pass: sha512(pass),
      email: email,
      company_phone: company_phone,
      mobile_phone: mobile_phone,
      company_id: Number(company_id),
    };

    console.log(payload);

    const response = await callApi(ENDPOINT.InsertClient, payload, "POST");
    //console.log("sapi",response);
    GetAllUsers();
    setOpen(false);
  };

  const GetCompanyProfile = async () => {
    let companyNames = [];
    const payload = {
      app: process.env.REACT_APP_KEY_COMPANY,
    };

    const response = await callApi(ENDPOINT.GetCompanyProfile, payload, "GET");
    //console.log("dapat data order",response.finalResponse.data);
    setDataCompany(response.finalResponse.data);
    if (response.finalResponse.data.length > 0) {
      for (let i = 0; i < response.finalResponse.data.length; i++) {
        companyNames.push(response.finalResponse.data[i].legal_name);
      }

      setCompanyNames(companyNames);
    }
  };

  const handleClickOpen = () => {
    setOpen(true);
    GetCompanyProfile();
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.username);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const order_cat = ["Assessment", "Surveillance", "Consultation"];

  const top100Films = [
    { title: "The Shawshank Redemption", year: 1994 },
    { title: "The Godfather", year: 1972 },
    { title: "The Godfather: Part II", year: 1974 },
    { title: "The Dark Knight", year: 2008 },
    { title: "12 Angry Men", year: 1957 },
    { title: "Schindler's List", year: 1993 },
    { title: "Pulp Fiction", year: 1994 },
  ];

  const handleCompanyName = (value) => {
    let obj = dataCompany.find((o) => o.legal_name === value);

    console.log(obj);
    setCompany_id(obj.id);
    setCompanyName(value);
  };

  return (
    <Page title="Orders | Green Port Ops">
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Masterdata Client
        </Typography>
        <Button
          variant="contained"
          component={RouterLink}
          to="#"
          startIcon={<Iconify icon="eva:plus-fill" />}
          onClick={handleClickOpen}
          style={{ backgroundColor: "#164477", boxShadow: "none" }}
        >
          New Client
        </Button>
      </Stack>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="md"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          New Client
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={3}>
            {/* left side */}
            <Grid item xs={6}>
              <h4>Username</h4>
              <TextField
                required
                autoFocus
                id="username"
                // label="Username"
                type="text"
                fullWidth
                variant="outlined"
                style={{ marginBottom: "15px" }}
                value={username}
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              />
              <h4>Full Name</h4>
              <TextField
                required
                autoFocus
                id="fullname"
                // label="Username"
                type="text"
                fullWidth
                variant="outlined"
                style={{ marginBottom: "15px" }}
                value={nama}
                onChange={(e) => {
                  setNama(e.target.value);
                }}
              />
              <h4>Email</h4>
              <TextField
                required
                autoFocus
                id="username"
                // label="Username"
                type="email"
                fullWidth
                variant="outlined"
                style={{ marginBottom: "15px" }}
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
              <h4>Password</h4>
              <TextField
                required
                autoFocus
                id="fullname"
                // label="Username"
                type="password"
                fullWidth
                variant="outlined"
                style={{ marginBottom: "15px" }}
                value={pass}
                onChange={(e) => {
                  setPass(e.target.value);
                }}
              />
            </Grid>
            {/* right side */}
            <Grid item xs={6}>
              <h4>Company</h4>
              {/* <TextField
                required
                autoFocus
                id="username"
                // label="Username"
                type="text"
                fullWidth
                variant="outlined"
                style={{ marginBottom: "15px" }}
                value={username}
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              /> */}
              <Autocomplete
                id="controllable-states-demo"
                options={companyNames}
                value={companyName}
                onChange={(e, val) => {
                  console.log(val);
                  handleCompanyName(val);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    required
                    size="small"
                    placeholder="-- Select Client --"
                    style={{ fontSize: 12, paddingTop: 20 }}
                  />
                )}
                // onSelect={() => console.log(JSON.stringify(row))}
                style={{
                  marginTop: "-30px",
                  fontSize: 12,
                }}
              />
              <h4>Office Phone</h4>
              <TextField
                required
                autoFocus
                id="fullname"
                // label="Username"
                type="number"
                fullWidth
                variant="outlined"
                style={{ marginBottom: "15px" }}
                value={company_phone}
                onChange={(e) => {
                  setCompany_phone(e.target.value);
                }}
              />
              <h4>HP / WA</h4>
              <TextField
                required
                autoFocus
                id="fullname"
                // label="Username"
                type="number"
                fullWidth
                variant="outlined"
                style={{ marginBottom: "15px" }}
                value={mobile_phone}
                onChange={(e) => {
                  setMobile_phone(e.target.value);
                }}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={InsertClient}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>
      <Card>
        <UserListToolbar
          numSelected={selected.length}
          filterName={filterName}
          onFilterName={handleFilterByName}
        />

        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <StyledTableCell>No.</StyledTableCell>
                <StyledTableCell>Username</StyledTableCell>
                <StyledTableCell>Fullname</StyledTableCell>
                <StyledTableCell>Email</StyledTableCell>
                <StyledTableCell>Company</StyledTableCell>
                <StyledTableCell>Telephone</StyledTableCell>
                <StyledTableCell>HP/WA</StyledTableCell>
                {/* <StyledTableCell>Action</StyledTableCell> */}
              </TableRow>
            </TableHead>

            <TableBody>
              {filteredUsers
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                    >
                      <TableCell
                        style={{
                          textAlign: "center",
                        }}
                      >
                        {index + 1}.
                      </TableCell>
                      <TableCell
                        style={{
                          textAlign: "center",
                        }}
                      >
                        {row.username}
                      </TableCell>
                      <TableCell
                        style={{
                          textAlign: "center",
                        }}
                      >
                        {row.nama}
                      </TableCell>
                      <TableCell
                        style={{
                          textAlign: "center",
                        }}
                      >
                        {row.email}
                      </TableCell>
                      <TableCell
                        style={{
                          textAlign: "center",
                        }}
                      >
                        belum ada field nya
                      </TableCell>
                      <TableCell
                        style={{
                          textAlign: "center",
                        }}
                      >
                        {row.company_phone === null || row.company_phone === ""
                          ? "-"
                          : row.company_phone}
                      </TableCell>
                      <TableCell
                        style={{
                          textAlign: "center",
                        }}
                      >
                        {row.mobile_phone === null || row.mobile_phone === ""
                          ? "-"
                          : row.mobile_phone}
                      </TableCell>
                      {/* <TableCell
                        style={{ width: "100px", textAlign: "center" }}
                      >
                        <RouterLink to={`clientdetail/${row.id_user}`} state={row}>
                          Details
                        </RouterLink>
                      </TableCell> */}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Page>
  );
}
