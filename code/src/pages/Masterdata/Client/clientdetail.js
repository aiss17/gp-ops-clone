import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
// Autocomplete
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
// textArea
import TextareaAutosize from "@mui/material/TextareaAutosize";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  DialogTitle,
  IconButton,
  DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  DialogActions,
  Radio,
  Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab
} from "@mui/material";
import { useLocation } from "react-router-dom";

const actionOpt = ["Publish", "Not Publish"];
const projectList = [
  "Project A",
  "Project B",
  "Project C",
  "Project D",
  "Project E"
];
const groupList = ["Group A", "Group B", "Group C", "Group D", "Group E"];
const collaboratorList = [
  "Collaborator A",
  "Collaborator B",
  "Collaborator C",
  "Collaborator D",
  "Collaborator E"
];

// header project asosiated
const columns = [
  { id: "id_client", label: "ID Clinet" },
  { id: "project", label: "Project" },
  { id: "start_date", label: "Start Date", align: "right" },
  { id: "due_date", label: "Due Date", align: "right" },
  { id: "progress", label: "Progress" },
  { id: "contract", label: "Contract No." },
  { id: "klas", label: "Class" },
  { id: "group", label: "Group" },
  { id: "status", label: "Status" }
];

function createData(
  id_client,
  project,
  start_date,
  due_date,
  progress,
  contract,
  klas,
  group,
  status
) {
  return {
    id_client,
    project,
    start_date,
    due_date,
    progress,
    contract,
    klas,
    group,
    status
  };
}

const rows = [
  createData(
    "01",
    "Project A",
    "02-12-2020",
    "01-02-2021",
    "100%",
    "1224TR",
    "Class A",
    "Grosup B",
    "Finished"
  )
];

// header log client
const headerLog = [
  { id: "date", label: "Date" },
  { id: "doing", label: "Doing" },
  { id: "act", label: "Act", align: "right" },
  { id: "description", label: "Description", align: "right" }
];

function createDataLog(date, doing, act, description) {
  return { date, doing, act, description };
}

const rowsLog = [
  createDataLog(
    "01-02-2021",
    "Update",
    "Update Progress",
    "Progress becam 100%"
  )
];

export default function StickyHeadTable() {
  const [page, setPage] = React.useState(0);
  const [arrayIdDoc, setArrayIdDoc] = useState([]);

  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = useState(false);
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [fileUpload, setFileUpload] = useState([]);

  // Data
  const [nama, setNama] = useState("");
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");
  const [email, setEmail] = useState("");
  const [company_phone, setCompany_phone] = useState("");
  const [mobile_phone, setMobile_phone] = useState("");
  const [app, setApp] = useState("29ac790b5fdb251a682c3db72fadcc1f");
  const [company_id, setCompany_id] = useState("");

  const location = useLocation();

  useEffect(() => {
    console.log(location.state);

    return () => {};
  }, []);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  // Autocomplete (ComboBox)
  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;

  const criteria = [
    "Komitmen dan kebijakan penerapan green port",
    "Promosi green port",
    "Tata kelola kawasan pelabuhan",
    "Tata kelola transportasi pendukung",
    "Pengelolaan kualitas udara",
    "etc."
  ];
  const subcriteria = [
    "Rencana pengembangan green port",
    "Pembiayaan green port",
    "Laporan tahunan green port",
    "Organisasi",
    "Sistem manajemen pendukung green port",
    "Penegakan kebijakan green port",
    "etc."
  ];

  return (
    <Card>
      {/* manajemen kriteria */}
      <Box
        sx={{
          width: "auto",
          maxWidth: "100%",
          margin: 2
        }}
      >
        <Grid container spacing={3}>
          {/* Detail Client */}
          <Grid item xs={12}>
            <Typography variant="h5" gutterBottom>
              Client Detail
            </Typography>
            <hr style={{ marginTop: "0px", marginBottom: "-10px" }} />
          </Grid>
          {/* left side */}
          <Grid item xs={6}>
            <h4>Username</h4>
            <TextField
              disabled
              required
              autoFocus
              id="username"
              value={location.state.username}
              type="text"
              fullWidth
              variant="outlined"
              style={{ marginBottom: "15px" }}
              size="small"
            />
            <h4>Full Name</h4>
            <TextField
              disabled
              required
              autoFocus
              id="fullname"
              value={location.state.nama}
              type="text"
              fullWidth
              variant="outlined"
              style={{ marginBottom: "15px" }}
              size="small"
            />
            <h4>Email</h4>
            <TextField
              disabled
              required
              autoFocus
              id="username"
              value={location.state.email}
              type="email"
              fullWidth
              variant="outlined"
              style={{ marginBottom: "15px" }}
              size="small"
            />
            <h4>Password</h4>
            <TextField
              disabled
              required
              autoFocus
              id="fullname"
              value={location.state.pin}
              type="password"
              fullWidth
              variant="outlined"
              style={{ marginBottom: "15px" }}
              size="small"
            />
          </Grid>
          {/* right side */}
          <Grid item xs={6}>
            <h4>Company</h4>
            <TextField
              disabled
              required
              autoFocus
              id="username"
              value={location.state.nama}
              type="text"
              fullWidth
              variant="outlined"
              style={{ marginBottom: "15px" }}
              size="small"
            />
            <h4>Office Phone</h4>
            <TextField
              disabled
              required
              autoFocus
              id="fullname"
              value={location.state.company_phone}
              type="text"
              fullWidth
              variant="outlined"
              style={{ marginBottom: "15px" }}
              size="small"
            />
            <h4>HP / WA</h4>
            <TextField
              disabled
              required
              autoFocus
              id="fullname"
              value={location.state.mobile_phone}
              type="text"
              fullWidth
              variant="outlined"
              style={{ marginBottom: "15px" }}
              size="small"
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              disabled
              variant="contained"
              style={{
                backgroundColor: "#248bf5",
                boxShadow: "none",
                marginBottom: 10,
                float: "right"
              }}
            >
              Update
            </Button>
          </Grid>
          {/* Project Asosiated */}
          {/* <Grid item xs={12}>
            <hr style={{ marginTop: 5 }} />
            <Typography variant="h5" gutterBottom>
              Project Asosiated
            </Typography>
            <Grid container spacing={3}>
              <Grid item xs={4}>
                <h5>Add Project</h5>
                <Autocomplete
                  id="controllable-states-demo"
                  options={projectList}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      margin="normal"
                      required
                      size="small"
                      placeholder="-- Select Project --"
                      style={{ fontSize: 12, paddingTop: 20 }}
                      sixe="small"
                    />
                  )}
                  // onSelect={() => console.log(JSON.stringify(row))}
                  style={{
                    marginTop: "-30px",
                    marginBottom: 10,
                    fontSize: 12,
                  }}
                />
              </Grid>
              <Grid item xs={3}>
                <h4>Group</h4>
                <Autocomplete
                  id="controllable-states-demo"
                  options={groupList}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      margin="normal"
                      required
                      size="small"
                      placeholder="-- Select Group --"
                      style={{ fontSize: 12, paddingTop: 20 }}
                      sixe="small"
                    />
                  )}
                  // onSelect={() => console.log(JSON.stringify(row))}
                  style={{
                    marginTop: "-30px",
                    marginBottom: 10,
                    fontSize: 12,
                  }}
                />
              </Grid>
              <Grid item xs={3}>
                <h4>Collaborator</h4>
                <Autocomplete
                  id="controllable-states-demo"
                  options={collaboratorList}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      // label="Type"
                      margin="normal"
                      // required
                      size="small"
                      placeholder="Select Status"
                      style={{ fontSize: 12, paddingTop: 20 }}
                      sixe="small"
                    />
                  )}
                  // onSelect={() => console.log(JSON.stringify(row))}
                  style={{
                    marginTop: "-30px",
                    marginBottom: 10,
                    fontSize: 12,
                  }}
                />
              </Grid>
              <Grid item xs={2}>
                <h1>&nbsp;</h1>
                <Button
                  variant="contained"
                  style={{
                    backgroundColor: "#248bf5",
                    boxShadow: "none",
                    marginBottom: 10,
                    marginTop: 10,
                  }}
                  fullWidth
                >
                  Update
                </Button>
              </Grid>
            </Grid>
            <TableContainer sx={{ maxHeight: 440 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{
                          minWidth: column.minWidth,
                          backgroundColor: "#254A7C",
                          color: "#ffffff",
                        }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                        >
                          {columns.map((column) => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Grid> */}
          {/* Log Client */}
          <Grid item xs={12}>
            <hr style={{ marginTop: 5 }} />

            <Typography variant="h5" gutterBottom>
              Log Client
            </Typography>
            <TableContainer sx={{ maxHeight: 440 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {headerLog.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{
                          minWidth: column.minWidth,
                          backgroundColor: "#254A7C",
                          color: "#ffffff"
                        }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rowsLog
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                        >
                          {headerLog.map((column) => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={rowsLog.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
}
