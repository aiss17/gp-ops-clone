import { filter } from "lodash";
import { sentenceCase } from "change-case";
import { useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  Container,
  Typography,
  TableContainer,
  TablePagination,
  Grid,
} from "@mui/material";
import moment from "moment";

import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
// components
import Page from "../../../components/Page";
import Label from "../../../components/Label";
import Scrollbar from "../../../components/Scrollbar";
import Iconify from "../../../components/Iconify";
import SearchNotFound from "../../../components/SearchNotFound";
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from "../../../sections/@dashboard/user";
//
import { ORDERS } from "../../../_mocks_/user";
// table
import TableHead from "@mui/material/TableHead";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
// api
import { callApi, GetCookie, refreshTokenApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

// ----------------------------------------------------------------------

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user.clientName.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Customer() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("clientName");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [open, setOpen] = useState(false);
  const [valueDate, setValueDate] = useState(null);
  const [valueAuto, setValueAuto] = useState("");
  const [inputValueAuto, setInputValueAuto] = useState("");
  const [valueRadio, setValueRadio] = useState("");
  const [disable, setDisable] = useState(false);

  // default set variabel input
  const [key, setKey] = useState("");
  const [data, setData] = useState([]);

  // set variabel input
  const [id, setID] = useState("");
  const [companyname, setCompanyname] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [province, setProvince] = useState([]);
  const [provinceid, setProvinceID] = useState(null);
  const [country, setCountry] = useState([]);
  const [countryid, setCountryID] = useState(null);
  const [postalcode, setPostalcode] = useState("");
  const [phone, setPhone] = useState("");
  const [fax, setFax] = useState("");
  const [website, setWebsite] = useState("");
  const [email, setEmail] = useState("");
  const [taxnumber, setTaxnumber] = useState("");
  const [taxaddress, setTaxaddress] = useState("");
  const [ownership, setOwnership] = useState("");
  const [established, setEstablished] = useState(null);
  const [group, setGroup] = useState("");

  useEffect(() => {
    // console.log(data);
    GetCompanyProfile();
    return () => {};
  }, []);

  const GetCompanyProfile = async () => {
    const payload = {
      app: process.env.REACT_APP_KEY_COMPANY,
    };

    const response = await callApi(ENDPOINT.GetCompanyProfile, payload, "GET");
    setKey(GetCookie("key"));
    //console.log("dapat data order",response.finalResponse.data);
    setData(response.finalResponse.data);
  };

  const GetAllProvince = async () => {
    const payload = {
      app: process.env.REACT_APP_KEY_COMPANY,
    };

    const response = await callApi(ENDPOINT.GetAllProvince, payload, "GET");
    //console.log("sapi",response);
    setProvince(response.finalResponse.data);
  };

  const GetAllCountry = async () => {
    const payload = {
      app: process.env.REACT_APP_KEY_COMPANY,
    };

    const response = await callApi(ENDPOINT.GetAllCountry, payload, "GET");
    //console.log("sapi",response);
    setCountry(response.finalResponse.data);
  };

  const InsertCompany = async () => {
    setKey(GetCookie("key"));

    const payload = {
      app: process.env.REACT_APP_KEY_COMPANY,
      // key: key,
      userid: GetCookie("id_user"),

      legal_name: companyname,
      address: address,
      city: city,
      province: provinceid,
      country: countryid,
      postal_code: postalcode,
      phone: phone,
      fax: fax,
      website: website,
      email: email,
      tax_number: taxnumber,
      tax_address: taxaddress,
      ownership: ownership,
      established: established,
      group: group,
    };
    console.log(payload);

    const response = await callApi(ENDPOINT.GetCompanyProfile, payload, "POST");

    setCompanyname("");
    setAddress("");
    setCity("");
    setProvinceID(null);
    setCountryID(null);
    setPostalcode("");
    setPhone("");
    setFax("");
    setWebsite("");
    setEmail("");
    setTaxnumber("");
    setTaxaddress("");
    setOwnership("");
    setEstablished(null);
    setGroup("");

    handleClose();
    GetCompanyProfile();
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n.clientName);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const filteredUsers = applySortFilter(
    data,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  return (
    <Page title="Orders | Green Port Ops">
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Masterdata Customer
        </Typography>
        <Button
          variant="contained"
          component={RouterLink}
          to="#"
          startIcon={<Iconify icon="eva:plus-fill" />}
          onClick={function (event) {
            GetAllProvince();
            GetAllCountry();
            handleClickOpen();
          }}
          style={{ backgroundColor: "#164477", boxShadow: "none" }}
        >
          Add Company
        </Button>
      </Stack>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="md"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          New Company
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={3}>
            {/* left side */}
            <Grid item xs={6}>
              <TextField
                required
                type="text"
                label="Company Name"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setCompanyname(e.target.value)}
              />
              <TextField
                required
                type="text"
                label="Address"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setAddress(e.target.value)}
              />
              <TextField
                required
                type="text"
                label="City"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: "-1px" }}
                onChange={(e) => setCity(e.target.value)}
              />

              <Autocomplete
                id="controllable-states-demo"
                options={province}
                getOptionLabel={(option) => option.province}
                onChange={(e, val) => {
                  setProvinceID(val.id);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    required
                    label="Province"
                    fullWidth
                  />
                )}
                style={{ marginBottom: 6 }}
                size="small"
              />

              <Autocomplete
                id="controllable-states-demo"
                options={country}
                getOptionLabel={(option) => option.name}
                onChange={(e, val) => {
                  setCountryID(val.id);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    margin="normal"
                    required
                    label="Country"
                    fullWidth
                  />
                )}
                style={{ marginBottom: 6, marginTop: "-15px" }}
                size="small"
              />
              <TextField
                required
                type="number"
                label="Postal Code"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setPostalcode(e.target.value)}
              />
              <TextField
                required
                type="number"
                label="Telephone"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setPhone(e.target.value)}
              />
              <TextField
                required
                type="number"
                label="Fax"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setFax(e.target.value)}
              />
            </Grid>
            {/* right side */}
            <Grid item xs={6}>
              <TextField
                required
                type="text"
                label="Website"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setWebsite(e.target.value)}
              />
              <TextField
                required
                type="text"
                label="Email"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                required
                type="number"
                label="Tax Number"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setTaxnumber(e.target.value)}
              />
              <TextField
                required
                type="text"
                label="Tax Address"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setTaxaddress(e.target.value)}
              />
              <TextField
                required
                type="text"
                label="Ownership"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setOwnership(e.target.value)}
              />
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  disabled={disable}
                  // label="Requested Date"
                  value={established}
                  onChange={(newValue) => {
                    setEstablished(newValue);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      margin="normal"
                      required
                      fullWidth
                      size="small"
                    />
                  )}
                />
              </LocalizationProvider>
              {/* <TextField
                required
                type="number"
                label="Established"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setEstablished(e.target.value)}
              /> */}
              <TextField
                required
                type="text"
                label="Group"
                fullWidth
                variant="outlined"
                size="small"
                style={{ marginBottom: 15 }}
                onChange={(e) => setGroup(e.target.value)}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={function (event) {
              InsertCompany();
            }}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>
      <Card>
        <UserListToolbar
          numSelected={selected.length}
          filterName={filterName}
          onFilterName={handleFilterByName}
        />

        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <StyledTableCell>No.</StyledTableCell>
                <StyledTableCell>Company ID</StyledTableCell>
                <StyledTableCell>Company Name</StyledTableCell>
                <StyledTableCell>Address</StyledTableCell>
                <StyledTableCell>Province</StyledTableCell>
                <StyledTableCell>Telephone</StyledTableCell>
                <StyledTableCell>Email</StyledTableCell>
                <StyledTableCell>Action</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredUsers
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const {
                    id,
                    legal_name,
                    address,
                    city,
                    province,
                    phone,
                    email,
                  } = row;

                  return (
                    <TableRow>
                      <TableCell align="left">{index + 1}</TableCell>
                      <TableCell align="left">{id}</TableCell>
                      <TableCell align="left">{legal_name}</TableCell>
                      <TableCell align="left">
                        {address} - {city}
                      </TableCell>
                      <TableCell align="left">{province}</TableCell>
                      <TableCell align="left">{phone}</TableCell>
                      <TableCell align="left">{email}</TableCell>
                      <TableCell align="right">
                        <Button
                          variant="contained"
                          component={RouterLink}
                          to={
                            window.location.pathname + `/customerdetails/${id}`
                          }
                          startIcon={<Iconify icon="bx:detail" />}
                          style={{
                            backgroundColor: "#254A7C",
                            boxShadow: "none",
                            // "&:hover": { color: "red" },
                          }}
                        >
                          Detail
                        </Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            {isUserNotFound && (
              <TableBody>
                <TableRow>
                  <TableCell align="center" colSpan={8} sx={{ py: 3 }}>
                    <SearchNotFound searchQuery={filterName} />
                  </TableCell>
                </TableRow>
              </TableBody>
            )}
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Card>
    </Page>
  );
}
