import { Link as RouterLink, useNavigate } from "react-router-dom";

import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";
import * as React from "react";
import Tabs, { tabsClasses } from "@mui/material/Tabs";
import { useEffect, useState } from "react";
import { ENDPOINT } from "src/services/Constants";
// api
import {
  callApi,
  GetCookie,
  refreshTokenApi,
  SetCookie,
} from "src/services/Functions";

import { useParams } from "react-router-dom";
import {
  Button,
  Grid,
  Stack,
  Table,
  TableBody,
  TableCell,
  tableCellClasses,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from "@mui/material";
import { styled } from "@material-ui/styles";

import Iconify from "src/components/Iconify";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

export default function CustDetail() {
  const { id } = useParams();
  const [value, setValue] = useState(0);
  const [dataproject, setDataproject] = useState([]);
  const [dataDocument, setDataDocument] = useState([]);

  // default set variabel input
  const [key, setKey] = useState("");
  const [data, setData] = useState([]);

  // set variabel input
  const [companyname, setCompanyname] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [province, setProvince] = useState("");
  const [country, setCountry] = useState("");
  const [postalcode, setPostalcode] = useState("");
  const [phone, setPhone] = useState("");
  const [fax, setFax] = useState("");
  const [website, setWebsite] = useState("");
  const [email, setEmail] = useState("");
  const [taxnumber, setTaxnumber] = useState("");
  const [taxaddress, setTaxaddress] = useState("");
  const [ownership, setOwnership] = useState("");
  const [established, setEstablished] = useState("");
  const [group, setGroup] = useState("");

  useEffect(() => {
    console.log("ini state ewrfwer => ", id);
    GetCompanyByID(id);
    GetAllDocument();
    return () => {};
  }, []);

  const GetCompanyByID = async () => {
    const payload = {
      app: process.env.REACT_APP_KEY_COMPANY,
    };

    const response = await callApi(
      ENDPOINT.GetCompanyByID + `${id}`,
      payload,
      "GET"
    );
    setKey(GetCookie("key"));
    //console.log("dapat data order",response.finalResponse.data);
    setData(response.finalResponse.data);

    setCompanyname(response.finalResponse.data[0].legal_name);
    setAddress(response.finalResponse.data[0].address);
    setCity(response.finalResponse.data[0].city);
    setProvince(response.finalResponse.data[0].province);
    setCountry(response.finalResponse.data[0].country);
    setPostalcode(response.finalResponse.data[0].postal_code);
    setPhone(response.finalResponse.data[0].phone);
    setFax(response.finalResponse.data[0].fax);
    setWebsite(response.finalResponse.data[0].website);
    setEmail(response.finalResponse.data[0].email);
    setTaxnumber(response.finalResponse.data[0].tax_number);
    setTaxaddress(response.finalResponse.data[0].tax_address);
    setOwnership(response.finalResponse.data[0].ownership);
    setEstablished(response.finalResponse.data[0].established);
    setGroup(response.finalResponse.data[0].group);
  };

  const GetAllDocument = async () => {
    const response = await callApi(
      ENDPOINT.GetAllDocument,
      { category: "company", link: id },
      "GET"
    );
    setKey(GetCookie("key"));
    console.log("dapat data order", response.finalResponse.data);
    setDataDocument(response.finalResponse.data);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const navigate = useNavigate();
  return (
    <Box>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        mb={2}
      >
        <Typography variant="h4" gutterBottom>
          Detail Company
        </Typography>
        <Button
          variant="contained"
          // component={RouterLink}
          // to={window.location.pathname - `/customerdetails/${id}`}
          onClick={() => {
            navigate(-1);
            // console.log(window.location.pathname);
          }}
          startIcon={<Iconify icon="eva:arrow-ios-back-outline" />}
          style={{
            backgroundColor: "#D6D6D6",
            boxShadow: "none",
            color: "#000000",
            // "&:hover": { color: "red" },
          }}
        >
          Back
        </Button>
      </Stack>
      <hr style={{ marginTop: 0 }} />
      <Grid container spacing={3}>
        {/* left side */}
        <Grid item xs={6}>
          <TextField
            required
            type="text"
            label="Company Name"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={companyname}
            onChange={(e) => setCompanyname(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Address"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={address}
            onChange={(e) => setAddress(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="City"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={city}
            onChange={(e) => setCity(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Province"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={province}
            onChange={(e) => setProvince(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Country"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={country}
            onChange={(e) => setCountry(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Postal Code"
            fullWidth
            variant="outlined"
            size="small"
            value={postalcode}
            style={{ marginBottom: 15 }}
            onChange={(e) => setPostalcode(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Telephone"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Fax"
            fullWidth
            variant="outlined"
            size="small"
            value={fax}
            style={{ marginBottom: 15 }}
            onChange={(e) => setFax(e.target.value)}
            disabled
          />
        </Grid>
        {/* right side */}
        <Grid item xs={6}>
          <TextField
            required
            type="text"
            label="Website"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={website}
            onChange={(e) => setWebsite(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Email"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Tax Number"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={taxnumber}
            onChange={(e) => setTaxnumber(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Tax Address"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={taxaddress}
            onChange={(e) => setTaxaddress(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Ownership"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={ownership}
            onChange={(e) => setOwnership(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Established"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={established}
            onChange={(e) => setEstablished(e.target.value)}
            disabled
          />
          <TextField
            required
            type="text"
            label="Group"
            fullWidth
            variant="outlined"
            size="small"
            style={{ marginBottom: 15 }}
            value={group}
            onChange={(e) => setGroup(e.target.value)}
            disabled
          />
        </Grid>
        <Grid item xs={12} style={{ marginTop: "0px" }}>
          <hr style={{ marginTop: "-20px", marginBottom: "10px" }} />
          <Typography variant="h5" gutterBottom>
            Legal Document
          </Typography>
          <TableContainer sx={{ maxHeight: 440 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>No.</StyledTableCell>
                  <StyledTableCell>Document Name</StyledTableCell>
                  <StyledTableCell>Document Type</StyledTableCell>
                  <StyledTableCell>Action</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {dataDocument.length > 0 &&
                  dataDocument.map((value, index) => {
                    return (
                      <TableRow>
                        <TableCell align="left">{index + 1}</TableCell>
                        <TableCell align="left">{value.doc_title}</TableCell>
                        <TableCell style={{ textAlign: "center " }}>
                          <Button
                            onClick={() =>
                              window.open(
                                `${
                                  process.env.REACT_APP_BASURL_TRX
                                }documents/fetch?path=${
                                  value.path
                                }&act=download&key=${GetCookie("key")}`
                              )
                            }
                            variant="contained"
                            color="info"
                            component={RouterLink}
                            to="#"
                            startIcon={<Iconify icon="bi:download" />}
                            style={{
                              // backgroundColor: "#164477",
                              boxShadow: "none",
                              marginTop: 10,
                              marginBottom: 10,
                            }}
                          >
                            Download File
                          </Button>
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          {/* <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={rowsLog.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          /> */}
        </Grid>
      </Grid>
    </Box>
  );
}
