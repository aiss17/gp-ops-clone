import * as React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Article from "@mui/icons-material/Article";
import VideoCameraFrontIcon from "@mui/icons-material/VideoCameraFront";
import SummarizeIcon from "@mui/icons-material/Summarize";
import QuizIcon from "@mui/icons-material/Quiz";
import FactCheckIcon from "@mui/icons-material/FactCheck";

// pages
import { Assessment, Report, TechnicalEnquiry } from "../index";

// pages
import ManCriteria from "./ManageCriteria";
import ManChecklist from "./ManageChecklist";
import PrevChecklist from "./PreviewChecklist";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box style={{ clear: "both" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
          variant="scrollable"
          scrollButtons={false}
          style={{ marginTop: "-15px" }}
        >
          <Tab label="Manage Criteria" {...a11yProps(0)} />
          <Tab label="Manage Checklist" {...a11yProps(1)} />
          <Tab label="Preview Checklist" {...a11yProps(2)} />
          {/* <Tab label="TP" {...a11yProps(3)} />
          <Tab label="KU" {...a11yProps(4)} /> */}
        </Tabs>
      </Box>
      <TabPanel
        value={value}
        index={0}
        style={{ marginLeft: "-25px", marginRight: "-25px" }}
      >
        <ManCriteria />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <ManChecklist />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <PrevChecklist />
      </TabPanel>
      {/* <TabPanel value={value} index={3}>
        <TP />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <KU />
      </TabPanel> */}
    </Box>
  );
}
