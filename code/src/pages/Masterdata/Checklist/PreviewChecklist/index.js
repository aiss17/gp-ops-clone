import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
// Autocomplete
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
// textArea
import TextareaAutosize from "@mui/material/TextareaAutosize";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  DialogTitle,
  IconButton,
  DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  DialogActions,
  Radio,
  Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab
} from "@mui/material";
import Iconify from "../../../../components/Iconify";
import UserMoreMenuTE from "src/sections/@dashboard/user/UserMoreMenuTE";
import filepdf from "../../../../assets/PDF.pdf";
import { callApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";

const columns = [
  { id: "subcriteria_name", label: "Sub-Kriteria", minWidth: "100px" },
  { id: "portion", label: "Bobot Sub-Kriteria", minWidth: "100px" },
  { id: "step", label: "Langkah Penerapan", minWidth: "100px" },
  {
    id: "benchmark",
    label: "Tolok Ukur",
    minWidth: "100px",
    align: "right"
  },
  {
    id: "evidence",
    label: "Bukti Pendukung",
    minWidth: "100px",
    align: "right"
  },
  { id: "mater_weight", label: "Bobot", minWidth: "50px" },
  { id: "reference", label: "Acuan Referensi", minWidth: "100px" },
  { id: "keterangan", label: "Keterangan", minWidth: "100px" },
  { id: "sifat", label: "Sifat", minWidth: "50px" }
];

export default function StickyHeadTable() {
  const [page, setPage] = React.useState(0);
  const [arrayIdDoc, setArrayIdDoc] = useState([]);

  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = useState(false);
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [fileUpload, setFileUpload] = useState([]);
  const [data, setData] = useState([]);
  const [optionCriteria, setOptionCriteria] = useState([]);
  const [criteriaId, setCriteriaId] = useState(null);

  useEffect(() => {
    GetAllMasterChecklist();

    //GetAllMasterChecklist().then(GetAllCriteria); 
    setTimeout(() => { GetAllCriteria(); }, 3000);
   

    return () => {};
  }, []);

  const GetAllMasterChecklist = async () => {
    setData([]);
    const response = await callApi(
      ENDPOINT.GetAllMasterChecklist,
      { criteria: criteriaId },
      "GET"
    );

    setData(response.finalResponse.data);
  };

  const GetAllCriteria = async () => {
    const response = await callApi(
      ENDPOINT.GetAllCriteria,
      { status: 1 },
      "GET"
    );

    setOptionCriteria(response.finalResponse.data);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Card>
      {/* manajemen kriteria */}
      <Box
        sx={{
          width: "auto",
          maxWidth: "100%",
          margin: 2
        }}
      >
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography variant="h5" gutterBottom>
              Preview Checklist
            </Typography>
            <hr style={{ marginTop: "0px", marginBottom: "-10px" }} />
          </Grid>
          <Grid item xs={10}>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Criteria</span>
            <Autocomplete
              id="controllable-states-demo"
              options={optionCriteria}
              getOptionLabel={(option) =>
                `${option.code} - ${option.criteria_name}`
              }
              onChange={(e, val) => {
                console.log("Test value => ", val.id);
                setCriteriaId(val.id);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  // label="Aspek"
                  margin="normal"
                  required
                  fullWidth
                  size="small"
                  placeholder="--Select Criteria--"
                />
              )}
              style={{ marginTop: "-15px", marginBottom: 10 }}
            />
          </Grid>
          <Grid item xs={2}>
            <span
              style={{
                fontSize: "18px",
                fontWeight: 700
              }}
            >
              &nbsp;
            </span>
            <Button
              variant="contained"
              style={{
                backgroundColor: "#248bf5",
                boxShadow: "none",
                marginBottom: 10,
                float: "right",
                width: "100%"
              }}
              onClick={GetAllMasterChecklist}
            >
              Search
            </Button>
          </Grid>

          <Grid item xs={12}>
            <TableContainer sx={{ maxHeight: 440 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{
                          minWidth: column.minWidth,
                          backgroundColor: "#254A7C",
                          color: "#ffffff"
                        }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                        >
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.code}
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.portion}
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.application_steps}
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.master_name}
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.evidence}
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.reference}
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.master_weight}
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.remark}
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.compliance_type}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
}
