import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { Link as RouterLink } from "react-router-dom";
import { useEffect, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import { styled } from "@mui/material/styles";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import DatePicker from "@mui/lab/DatePicker";
import TextareaAutosize from "@mui/material/TextareaAutosize";
// Autocomplete
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  DialogTitle,
  IconButton,
  DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  DialogActions,
  Radio,
  Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab,
  Select,
  MenuItem
} from "@mui/material";
import Iconify from "../../../../components/Iconify";
import UserMoreMenuTE from "src/sections/@dashboard/user/UserMoreMenuTE";
import filepdf from "../../../../assets/PDF.pdf";
import { callApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";

const useStyles = makeStyles({
  table: {
    minWidth: 650
  },
  chatSection: {
    width: "100%",
    height: "80vh"
  },
  headBG: {
    backgroundColor: "#e0e0e0"
  },
  borderRight500: {
    borderRight: "1px solid #e0e0e0"
  },
  messageArea: {
    height: "70vh",
    overflowY: "auto",
    fontSize: "5px"
  }
});

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2)
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1)
  }
}));

const columns = [
  { id: "code", label: "Criteria Code", width: 30 },
  { id: "name", label: "Criteria Name", minWidth: 120 },
  {
    id: "bobot",
    label: "Weight (%)",
    minWidth: 50,
    align: "right",
    format: (value) => value.toLocaleString("en-US")
  },
  { id: "aspek", label: "Aspect", minWidth: 80 }
];

const actionOpt = ["Publish", "Not Publish"];

const colSubKriteria = [
  { id: "subcode", label: "Sub Criteria Code", minWidth: "30px" },
  { id: "name", label: "Sub Criteria", minWidth: "80px" },
  {
    id: "porsi",
    label: "Portion",
    minWidth: "30px",
    align: "center",
    format: (value) => value.toLocaleString("en-US")
  }
];

function createDataKriteria(subcode, name, porsi) {
  return { subcode, name, porsi };
}

const rowsKriteria = [
  createDataKriteria("KK-01", "Rencana pengembangan green port", 0.2),
  createDataKriteria("KK-02", "Pembiayaan green port", 0.2),
  createDataKriteria("KK-03", "Laporan tahunan green port", 0.2),
  createDataKriteria("KK-04", "Organisasi ", 0.2),
  createDataKriteria("KK-05", "Sistem manajemen pendukung green port", 0.1)
];

export default function StickyHeadTable() {
  const [page, setPage] = React.useState(0);
  const [arrayIdDoc, setArrayIdDoc] = useState([]);
  const [open, setOpen] = useState(false);
  const [openSub, setOpenSub] = useState(false);

  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  // Update Sub Criteria
  const [idSubCriteria, setIdSubCriteria] = useState(null);
  const [numberSubCriteriaUpdate, setNumberSubCriteriaUpdate] = useState("");
  const [nameSubCriteriaUpdate, setNameSubCriteriaUpdate] = useState("");
  const [portionSubCriteriaUpdate, setPortionSubCriteriaUpdate] = useState("");

  // Update
  const [idCriteriaUpdate, setIdCriteriaUpdate] = useState(null);
  const [codeCriteriaUpdate, setCodeCriteriaUpdate] = useState("");
  const [nameCriteriaUpdate, setNameCriteriaUpdate] = useState("");
  const [weightCriteriaUpdate, setWeightCriteriaUpdate] = useState(null);
  const [aspectCriteriaUpdate, setAspectCriteriaUpdate] = useState(null);

  // Criteria
  const [dataCriteria, setDataCriteria] = useState([]);
  const [codeCriteria, setCodeCriteria] = useState("");
  const [nameCriteria, setNameCriteria] = useState("");
  const [weightCriteria, setWeightCriteria] = useState(null);
  const [aspectCriteria, setAspectCriteria] = useState("");
  const [optionCriteria, setOptionCriteria] = useState([]);

  const [dataSubCriteria, setDataSubCriteria] = useState([]);
  const [idCriteria, setIdCriteria] = useState(null);
  const [selectOption, setSelectOption] = useState("Select criteria");
  const [numberSubCriteria, setNumberSubCriteria] = useState("");
  const [nameSubCriteria, setNameSubCriteria] = useState("");
  const [portionSubCriteria, setPortionSubCriteria] = useState("");

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  useEffect(() => {
    GetAllCriteria();
    setTimeout(() => {
      GetAllSubCriteria();
    }, 2000);
    setTimeout(() => {
      OptionCriteria();
    }, 3000);

    return () => {};
  }, []);

  const GetAllCriteria = async () => {
    const response = await callApi(ENDPOINT.GetAllCriteria, null, "GET");

    setDataCriteria(response.finalResponse.data);
  };

  const OptionCriteria = async () => {
    const response = await callApi(
      ENDPOINT.GetAllCriteria,
      { status: 1 },
      "GET"
    );
    setOptionCriteria(response.finalResponse.data);
  };

  const GetAllSubCriteria = async () => {
    const response = await callApi(ENDPOINT.GetAllSubCriteria, null, "GET");

    setDataSubCriteria(response.finalResponse.data);
  };

  const PostCriteria = async () => {
    const payload = {
      code: codeCriteria,
      weight_factor: weightCriteria,
      aspect: aspectCriteria,
      criteria_name: nameCriteria,
      status: 1
    };

    const response = await callApi(ENDPOINT.GetAllCriteria, payload, "POST");

    setCodeCriteria("");
    setNameCriteria("");
    setAspectCriteria("");
    setWeightCriteria(0);

    GetAllCriteria();
    OptionCriteria();
  };

  const PostSubCriteria = async () => {
    const payload = {
      criteria_id: idCriteria,
      number: Number(numberSubCriteria),
      subcriteria_name: nameSubCriteria,
      portion: Number(portionSubCriteria),
      status: 1
    };

    const response = await callApi(ENDPOINT.GetAllSubCriteria, payload, "POST");

    //setIdCriteria(null); ga perlu di null kan karena user ga perlu ngeclick lagi
    setNumberSubCriteria("");
    setNameSubCriteria("");
    setPortionSubCriteria("");

    GetAllSubCriteria();
  };

  const UpdateCriteria = async (status, item, type) => {
    let payload;
    if (type === "update") {
      payload = {
        code: codeCriteriaUpdate,
        weight_factor: weightCriteriaUpdate,
        aspect: aspectCriteriaUpdate,
        criteria_name: nameCriteriaUpdate
      };
    } else {
      payload = {
        status: status
      };
    }

    const response = await callApi(
      ENDPOINT.UpdateCriteria + `/${item}`,
      payload,
      "POST"
    );

    GetAllCriteria();
    OptionCriteria();
  };

  const UpdateSubCriteria = async (status, item, type) => {
    let payload;
    if (type === "update") {
      payload = {
        criteria_id: idCriteriaUpdate,
        number: Number(numberSubCriteriaUpdate),
        subcriteria_name: nameSubCriteriaUpdate,
        portion: Number(portionSubCriteriaUpdate)
      };
    } else {
      payload = {
        status: status
      };
    }

    const response = await callApi(
      ENDPOINT.UpdateSubCriteria + `/${item}`,
      payload,
      "POST"
    );

    GetAllSubCriteria();
  };

  const aspek = ["Technical", "Management", "Digitalization"];

  const handleClose = () => {
    setOpen(false);
    setOpenSub(false);
    // setDisable(false);
  };

  return (
    <Card>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="sm"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Edit Criteria
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500]
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <h4 style={{ marginBottom: "-15px" }}>Criteria Code</h4>
              <TextField
                required
                autoFocus
                margin="normal"
                id="name"
                // label="Project Name"
                type="text"
                fullWidth
                variant="outlined"
                placeholder={"Masukkan Kode Kriteria"}
                size="small"
                style={{ marginBottom: 10 }}
                value={codeCriteriaUpdate}
                onChange={(e) => {
                  setCodeCriteriaUpdate(e.target.value);
                }}
              />
              <h4 style={{ marginBottom: "-15px" }}>Criteria Name</h4>
              <TextField
                required
                autoFocus
                margin="normal"
                id="name"
                // label="Project Name"
                type="text"
                fullWidth
                variant="outlined"
                placeholder={"Masukkan Nama Kriteria"}
                size="small"
                style={{ marginBottom: 10 }}
                value={nameCriteriaUpdate}
                onChange={(e) => {
                  setNameCriteriaUpdate(e.target.value);
                }}
              />
              <h4 style={{ marginBottom: "-15px" }}>Weight (%)</h4>
              <TextField
                required
                autoFocus
                margin="normal"
                id="name"
                // label="Contract No"
                type="number"
                fullWidth
                variant="outlined"
                placeholder={"Insert Weight Value"}
                size="small"
                style={{ marginBottom: 10 }}
                value={weightCriteriaUpdate}
                onChange={(e) => {
                  setWeightCriteriaUpdate(e.target.value);
                }}
              />
              <h4 style={{ marginBottom: "-15px" }}>Aspect</h4>
              <Autocomplete
                id="controllable-states-demo"
                options={aspek}
                value={aspectCriteriaUpdate}
                onSelect={(e) => setAspectCriteriaUpdate(e.target.value)}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    // label="Aspek"
                    margin="normal"
                    required
                    fullWidth
                    size="small"
                  />
                )}
                style={{ marginBottom: 10 }}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={function (event) {
              handleClose();
              UpdateCriteria(null, idCriteriaUpdate, "update");
            }}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
              backgroundColor: "#254A7C",
              boxShadow: "none"
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>

      {/* Edit Sub Criteria */}
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openSub}
        maxWidth="sm"
        fullWidth
      >
        {/* <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
            Modal title
          </BootstrapDialogTitle> */}
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Edit Sub Criteria
          {openSub ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500]
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <h4 style={{ marginBottom: "-15px" }}>Criteria Code</h4>
              <Autocomplete
                id="controllable-states-demo"
                options={optionCriteria}
                // getOptionLabel={(option) => nameCriteriaUpdate}
                value={nameCriteriaUpdate}
                onChange={(e, val) => {
                  setNameCriteriaUpdate(val.code);
                  setIdCriteriaUpdate(val.id);
                  //console.log(val.id);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    // label="Aspek"
                    margin="normal"
                    required
                    value={nameCriteriaUpdate}
                    fullWidth
                    placeholder="Select Criteria"
                    size="small"
                  />
                )}
                style={{ marginBottom: 10 }}
              />
              <h4 style={{ marginBottom: "-15px" }}>Number of Sub Criteria</h4>
              <TextField
                required
                autoFocus
                margin="normal"
                id="name"
                // label="Project Name"
                type="number"
                fullWidth
                variant="outlined"
                placeholder={"Sub Criteria Number"}
                size="small"
                style={{ marginBottom: 10 }}
                value={numberSubCriteriaUpdate}
                onChange={(e) => {
                  setNumberSubCriteriaUpdate(e.target.value);
                }}
              />
              <h4 style={{ marginBottom: "-15px" }}>Sub Criteria Name</h4>
              <TextField
                required
                autoFocus
                margin="normal"
                id="name"
                // label="Project Name"
                type="text"
                fullWidth
                variant="outlined"
                placeholder={"Insert Sub Criteria"}
                size="small"
                style={{ marginBottom: 10 }}
                value={nameSubCriteriaUpdate}
                onChange={(e) => {
                  setNameSubCriteriaUpdate(e.target.value);
                }}
              />
              <h4 style={{ marginBottom: "-15px" }}>Portion</h4>
              <TextField
                required
                autoFocus
                margin="normal"
                id="name"
                // label="Contract No"
                type="number"
                fullWidth
                variant="outlined"
                placeholder={"Contoh: 0,2"}
                size="small"
                style={{ marginBottom: 10 }}
                value={portionSubCriteriaUpdate}
                onChange={(e) => {
                  setPortionSubCriteriaUpdate(e.target.value);
                }}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={function (event) {
              handleClose();
              UpdateSubCriteria(null, idCriteriaUpdate, "update");
            }}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              // backgroundColor: "#164477",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
              backgroundColor: "#254A7C",
              boxShadow: "none"
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>
      {/* manajemen kriteria */}
      <Box
        sx={{
          width: "auto",
          maxWidth: "100%",
          margin: 2
        }}
      >
        <Grid container spacing={3}>
          {/* Bagian Kiri */}
          <Grid item xs={4}>
            <Typography variant="h5" gutterBottom>
              Criteria Management
            </Typography>
            <hr style={{ marginTop: "0px", marginBottom: 10 }} />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Criteria Code
            </span>
            <TextField
              required
              autoFocus
              margin="normal"
              id="name"
              // label="Project Name"
              type="text"
              fullWidth
              variant="outlined"
              placeholder={"Masukkan Kode Kriteria"}
              size="small"
              style={{ marginTop: 0, marginBottom: 10 }}
              value={codeCriteria}
              onChange={(e) => {
                setCodeCriteria(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Criteria Name
            </span>
            <TextField
              required
              autoFocus
              margin="normal"
              id="name"
              // label="Project Value"
              type="text"
              fullWidth
              variant="outlined"
              placeholder={"Masukkan Nama Kriteria"}
              size="small"
              style={{ marginTop: 0, marginBottom: 10 }}
              value={nameCriteria}
              onChange={(e) => {
                setNameCriteria(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Weight (%)
            </span>
            <TextField
              required
              autoFocus
              margin="normal"
              id="name"
              // label="Contract No"
              type="number"
              fullWidth
              variant="outlined"
              placeholder={"Insert Weight Value"}
              size="small"
              style={{ marginTop: 0, marginBottom: 10 }}
              value={weightCriteria}
              onChange={(e) => {
                setWeightCriteria(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Aspect</span>
            <Autocomplete
              id="controllable-states-demo"
              options={aspek}
              value={aspectCriteria}
              onSelect={(e) => setAspectCriteria(e.target.value)}
              renderInput={(params) => (
                <TextField
                  {...params}
                  // label="Aspek"
                  margin="normal"
                  required
                  fullWidth
                  size="small"
                />
              )}
              style={{ marginTop: "-15px", marginBottom: 10 }}
            />
            <Button
              variant="contained"
              style={{
                backgroundColor: "#248bf5",
                boxShadow: "none",
                marginBottom: 10
              }}
              onClick={PostCriteria}
            >
              Save
            </Button>
          </Grid>

          {/* Bagian Kanan */}
          <Grid item xs={8}>
            <TableContainer sx={{ maxHeight: 440 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    <TableCell
                      style={{
                        backgroundColor: "#254A7C",
                        color: "#ffffff"
                      }}
                    >
                      No.
                    </TableCell>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{
                          minWidth: column.minWidth,
                          backgroundColor: "#254A7C",
                          color: "#ffffff"
                        }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                    <TableCell
                      style={{
                        backgroundColor: "#254A7C",
                        color: "#ffffff",
                        textAlign: "center"
                      }}
                    >
                      Action
                    </TableCell>
                    <TableCell
                      style={{
                        backgroundColor: "#254A7C",
                        color: "#ffffff"
                      }}
                    ></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {dataCriteria
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      const idDoc = dataCriteria[index].id;
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                        >
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {index + 1}.
                          </TableCell>
                          {/* {columns.map((column) => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })} */}
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.code}
                          </TableCell>
                          <TableCell>{row.criteria_name}</TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.weight_factor}
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.aspect}
                          </TableCell>
                          <TableCell
                            style={{ width: "100px", justifyContent: "center" }}
                          >
                            <FormControl
                              sx={{ m: 1, minWidth: 100 }}
                              size="small"
                            >
                              <Select
                                labelId="demo-simple-select-standard-label"
                                id="demo-simple-select-standard"
                                placeholder="-- Select --"
                                // label="Age"
                                value={dataCriteria[index].status}
                                onChange={(e) => {
                                  const objIndex = dataCriteria.findIndex(
                                    (obj) => obj.id === idDoc
                                  );

                                  // make new object of updated object.
                                  const updatedObj = {
                                    ...dataCriteria[objIndex],
                                    status: e.target.value
                                  };

                                  // make final new array of objects by combining updated object.
                                  const updatedProjects = [
                                    ...dataCriteria.slice(0, objIndex),
                                    updatedObj,
                                    ...dataCriteria.slice(objIndex + 1)
                                  ];

                                  setDataCriteria(updatedProjects);
                                  UpdateCriteria(e.target.value, row.id);
                                }}
                              >
                                <MenuItem
                                  value={1}
                                  title="Tolak ukur terpenuhi secara lengkap (bukti dokumen/laporan, dokumentasi, legalitas, persyaratan regulasi dll)"
                                >
                                  Publish
                                </MenuItem>
                                <MenuItem
                                  value={0}
                                  title="Tolak ukur terpenuhi sebagian"
                                >
                                  Not Publish
                                </MenuItem>
                              </Select>
                            </FormControl>
                          </TableCell>
                          <TableCell
                            style={{ width: "20px", justifyContent: "center" }}
                          >
                            <Button
                              variant="contained"
                              component={RouterLink}
                              to={"#"}
                              // state={{ companyName: company }}
                              // startIcon={<Iconify icon="eva:edit-fill" />}
                              style={{
                                backgroundColor: "#254A7C",
                                boxShadow: "none"
                                // "&:hover": { color: "red" },
                              }}
                              onClick={() => {
                                setIdCriteriaUpdate(row.id);
                                setCodeCriteriaUpdate(row.code);
                                setNameCriteriaUpdate(row.criteria_name);
                                setWeightCriteriaUpdate(row.weight_factor);
                                setAspectCriteriaUpdate(row.aspect);
                                setOpen(true);
                              }}
                            >
                              <Iconify
                                icon="eva:edit-fill"
                                width={24}
                                height={24}
                              />
                            </Button>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
              {dataCriteria.length < 1 && (
                <div
                  style={{
                    padding: 10,
                    width: "100%",
                    textAlign: "center",
                    border: "1px solid #dddddd"
                  }}
                >
                  There is no data
                </div>
              )}
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={dataCriteria.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Grid>
        </Grid>
      </Box>
      <hr
        style={{
          marginTop: "0px",
          // marginBottom: 10,
          borderColor: "red",
          borderWidth: 2
        }}
      />
      {/* manajemen subkriteria */}
      <Box
        sx={{
          width: "auto",
          maxWidth: "100%",
          margin: 2
        }}
      >
        <Grid container spacing={3}>
          {/* Bagian Kiri */}
          <Grid item xs={4}>
            <Typography variant="h5" gutterBottom>
              Sub Criteria Management
            </Typography>
            <hr style={{ marginTop: "0px", marginBottom: 10 }} />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Criteria</span>
            <Autocomplete
              id="controllable-states-demo"
              options={optionCriteria}
              getOptionLabel={(option) => option.code}
              onChange={(e, val) => {
                setIdCriteria(val.id);
                //console.log(val.id);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  // label="Aspek"
                  margin="normal"
                  required
                  fullWidth
                  placeholder="Select Criteria"
                  size="small"
                />
              )}
              style={{ marginTop: "-15px", marginBottom: 5 }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Number of Sub Criteria
            </span>
            <TextField
              required
              autoFocus
              margin="normal"
              id="name"
              // label="Project Name"
              type="number"
              fullWidth
              variant="outlined"
              placeholder={"Sub Criteria Number"}
              size="small"
              style={{ marginTop: 0, marginBottom: 10 }}
              value={numberSubCriteria}
              onChange={(e) => {
                setNumberSubCriteria(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Sub Criteria Name
            </span>
            <TextField
              required
              autoFocus
              margin="normal"
              id="name"
              // label="Project Name"
              type="text"
              fullWidth
              variant="outlined"
              placeholder={"Insert Sub Criteria"}
              size="small"
              style={{ marginTop: 0, marginBottom: 10 }}
              value={nameSubCriteria}
              onChange={(e) => {
                setNameSubCriteria(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Portion</span>
            <TextField
              required
              autoFocus
              margin="normal"
              id="name"
              // label="Contract No"
              type="number"
              fullWidth
              variant="outlined"
              placeholder={"Contoh: 0,2"}
              size="small"
              style={{ marginTop: 0, marginBottom: 10 }}
              value={portionSubCriteria}
              onChange={(e) => {
                setPortionSubCriteria(e.target.value);
              }}
            />

            <Button
              variant="contained"
              style={{
                backgroundColor: "#248bf5",
                boxShadow: "none",
                marginBottom: 10
              }}
              onClick={PostSubCriteria}
            >
              Save
            </Button>
          </Grid>

          {/* Bagian Kanan */}
          <Grid item xs={8}>
            <TableContainer sx={{ maxHeight: 600 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    <TableCell
                      style={{
                        backgroundColor: "#254A7C",
                        color: "#ffffff"
                      }}
                    >
                      No.
                    </TableCell>
                    {colSubKriteria.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{
                          minWidth: column.minWidth,
                          backgroundColor: "#254A7C",
                          color: "#ffffff"
                        }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                    <TableCell
                      style={{
                        backgroundColor: "#254A7C",
                        color: "#ffffff"
                      }}
                    >
                      Action
                    </TableCell>
                    <TableCell
                      style={{
                        backgroundColor: "#254A7C",
                        color: "#ffffff"
                      }}
                    ></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {dataSubCriteria
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      const idDoc = dataSubCriteria[index].id;
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                        >
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {index + 1}.
                          </TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.criteria_code}
                          </TableCell>
                          <TableCell>{row.subcriteria_name}</TableCell>
                          <TableCell
                            style={{
                              textAlign: "center"
                            }}
                          >
                            {row.portion}
                          </TableCell>
                          <TableCell
                            style={{ width: "190px", justifyContent: "center" }}
                          >
                            <FormControl
                              sx={{ m: 1, minWidth: 100 }}
                              size="small"
                            >
                              <Select
                                labelId="demo-simple-select-standard-label"
                                id="demo-simple-select-standard"
                                placeholder="-- Select --"
                                // label="Age"
                                value={dataSubCriteria[index].status}
                                onChange={(e) => {
                                  const objIndex = dataSubCriteria.findIndex(
                                    (obj) => obj.id === idDoc
                                  );

                                  // make new object of updated object.
                                  const updatedObj = {
                                    ...dataSubCriteria[objIndex],
                                    status: e.target.value
                                  };

                                  // make final new array of objects by combining updated object.
                                  const updatedProjects = [
                                    ...dataSubCriteria.slice(0, objIndex),
                                    updatedObj,
                                    ...dataSubCriteria.slice(objIndex + 1)
                                  ];

                                  setDataSubCriteria(updatedProjects);
                                  UpdateSubCriteria(e.target.value, row.id);
                                }}
                              >
                                <MenuItem
                                  value={1}
                                  title="Tolak ukur terpenuhi secara lengkap (bukti dokumen/laporan, dokumentasi, legalitas, persyaratan regulasi dll)"
                                >
                                  Publish
                                </MenuItem>
                                <MenuItem
                                  value={0}
                                  title="Tolak ukur terpenuhi sebagian"
                                >
                                  Not Publish
                                </MenuItem>
                              </Select>
                            </FormControl>
                          </TableCell>
                          <TableCell
                            style={{ width: "20px", justifyContent: "center" }}
                          >
                            <Button
                              variant="contained"
                              component={RouterLink}
                              to={"#"}
                              // state={{ companyName: company }}
                              // startIcon={<Iconify icon="eva:edit-fill" />}
                              style={{
                                backgroundColor: "#254A7C",
                                boxShadow: "none"
                                // "&:hover": { color: "red" },
                              }}
                              onClick={() => {
                                const objIndex = optionCriteria.findIndex(
                                  (obj) => obj.id === row.criteria_id
                                );

                                console.log("test => ", optionCriteria[objIndex].code);
                                setNameCriteriaUpdate(
                                  optionCriteria[objIndex].code
                                );
                                setIdSubCriteria(row.id);
                                setIdCriteriaUpdate(row.criteria_id);
                                setNumberSubCriteriaUpdate(row.number);
                                setNameSubCriteriaUpdate(row.subcriteria_name);
                                setPortionSubCriteriaUpdate(row.portion);
                                setOpenSub(true);
                              }}
                            >
                              <Iconify
                                icon="eva:edit-fill"
                                width={24}
                                height={24}
                              />
                            </Button>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
              {dataSubCriteria.length < 1 && (
                <div
                  style={{
                    padding: 10,
                    width: "100%",
                    textAlign: "center",
                    border: "1px solid #dddddd"
                  }}
                >
                  There is no file
                </div>
              )}
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={dataSubCriteria.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
}
