import * as React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
// Autocomplete
import Autocomplete from "@mui/material/Autocomplete";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
// textArea
import TextareaAutosize from "@mui/material/TextareaAutosize";

import {
  Card,
  Stack,
  Button,
  Checkbox,
  Container,
  Typography,
  Grid,
  DialogTitle,
  IconButton,
  DialogContent,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  DialogActions,
  Radio,
  Dialog,
  Collapse,
  Box,
  List,
  ListItem,
  ListItemText,
  Divider,
  Fab,
  Select,
  MenuItem,
} from "@mui/material";
import Iconify from "../../../../components/Iconify";
import UserMoreMenuTE from "src/sections/@dashboard/user/UserMoreMenuTE";
import filepdf from "../../../../assets/PDF.pdf";
import CloseIcon from "@mui/icons-material/Close";
import { Link as RouterLink } from "react-router-dom";
import { callApi } from "src/services/Functions";
import { ENDPOINT } from "src/services/Constants";
import { SearchColumn, UserListToolbar } from "src/sections/@dashboard/user";
import Scrollbar from "src/components/Scrollbar";
import { filter } from "lodash";
import SearchNotFound from "src/components/SearchNotFound";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.white,
    backgroundColor: "#254A7C",
    color: "#ffffff",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(
  array,
  queryKode,
  queryLangkahPenerapan,
  queryTolakUkur,
  queryBuktiPendukung,
  queryAcuanReferensi,
  queryKeterangan,
  querySifat
) {
  let stabilizedThis = array.map((el, index) => [el, index]);

  // if (queryKode || queryLangkahPenerapan || queryTolakUkur) {
  //   let result = filter(
  //     array,
  //     (_user) =>
  //       _user.code.toLowerCase().indexOf(queryKode.toLowerCase()) !== -1
  //   );

  //   result = filter(
  //     array,
  //     (_user) =>
  //       _user.application_steps.toLowerCase().indexOf(queryLangkahPenerapan.toLowerCase()) !== -1
  //   );

  //   result = filter(
  //     array,
  //     (_user) =>
  //       _user.master_name.toLowerCase().indexOf(queryTolakUkur.toLowerCase()) !== -1
  //   );

  //   return array;
  // }

  if (queryKode) {
    return filter(
      array,
      (_user) =>
        _user.code.toLowerCase().indexOf(queryKode.toLowerCase()) !== -1
    );
  }

  if (queryLangkahPenerapan) {
    return filter(
      array,
      (_user) =>
        _user.application_steps
          .toLowerCase()
          .indexOf(queryLangkahPenerapan.toLowerCase()) !== -1
    );
  }

  if (queryTolakUkur) {
    return filter(
      array,
      (_user) =>
        _user.master_name
          .toLowerCase()
          .indexOf(queryTolakUkur.toLowerCase()) !== -1
    );
  }

  if (queryBuktiPendukung) {
    return filter(
      array,
      (_user) =>
        _user.evidence
          .toLowerCase()
          .indexOf(queryBuktiPendukung.toLowerCase()) !== -1
    );
  }

  if (queryAcuanReferensi) {
    console.log("Keluar ga ya => ", queryAcuanReferensi);
    return filter(
      array,
      (_user) =>
        String(_user.reference)
          .toLowerCase()
          .indexOf(queryAcuanReferensi.toLowerCase()) !== -1
    );
  }

  if (queryKeterangan) {
    console.log("Haha");
    return filter(
      array,
      (_user) =>
        String(_user.remark)
          .toLowerCase()
          .indexOf(queryKeterangan.toLowerCase()) !== -1
    );
  }

  if (querySifat) {
    return filter(
      array,
      (_user) =>
        _user.compliance_type
          .toLowerCase()
          .indexOf(querySifat.toLowerCase()) !== -1
    );
  }

  return stabilizedThis.map((el) => el[0]);
}

export default function StickyHeadTable() {
  const [page, setPage] = React.useState(0);
  const [arrayIdDoc, setArrayIdDoc] = useState([]);
  const [selected, setSelected] = useState([]);
  const [filterName, setFilterName] = useState("");
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("code");

  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [open, setOpen] = useState(false);
  const [disable, setDisable] = useState(false);
  const [valueRadio, setValueRadio] = useState("");
  const [fileUpload, setFileUpload] = useState([]);
  const [data, setData] = useState([]);
  const [optionSubCriteria, setOptionSubCriteria] = useState([]);
  const [idMasterChecklist, setIdMasterChecklist] = useState(null);

  // payload
  const [criteriaId, setCriteriaId] = useState(null);
  const [subCriteriaId, setSubCriteriaId] = useState(null);
  const [masterName, setMasterName] = useState("");
  const [evidence, setEvidence] = useState("");
  const [applicationStep, setApplicationStep] = useState("");
  const [reference, setReference] = useState("");
  const [complianceType, setComplianceType] = useState("");
  const [remark, setRemark] = useState("");

  // update payload
  const [criteriaIdUpdate, setCriteriaIdUpdate] = useState(null);
  const [subCriteriaIdUpdate, setSubCriteriaIdUpdate] = useState(null);
  const [masterNameUpdate, setMasterNameUpdate] = useState("");
  const [evidenceUpdate, setEvidenceUpdate] = useState("");
  const [applicationStepUpdate, setApplicationStepUpdate] = useState("");
  const [referenceUpdate, setReferenceUpdate] = useState("");
  const [complianceTypeUpdate, setComplianceTypeUpdate] = useState("");
  const [remarkUpdate, setRemarkUpdate] = useState("");
  const [portionCheklis, setPortionChecklist] = useState("");

  // filter state
  const [filterKode, setFilterKode] = useState("");
  const [filterLangkahPenerapan, setFilterLangkahPenerapan] = useState("");
  const [filterTolakUkur, setFilterTolakUkur] = useState("");
  const [filterBuktiPendukung, setFilterBuktiPendukung] = useState("");
  const [filterAcuanReferensi, setFilterAcuanReferensi] = useState("");
  const [filterKeterangan, setFilterKeterangan] = useState("");
  const [filterSifat, setFilterSifat] = useState("");

  useEffect(() => {
    GetAllMasterChecklist();
    // GetAllMasterChecklist().then(GetAllSubCriteria);

    setTimeout(() => {
      GetAllSubCriteria();
    }, 3000);

    return () => {};
  }, []);

  const GetAllMasterChecklist = async () => {
    const response = await callApi(ENDPOINT.GetAllMasterChecklist, null, "GET");

    setData(response.finalResponse.data);
  };

  const GetMasterChecklistById = async (item) => {
    const response = await callApi(
      ENDPOINT.GetAllMasterChecklist + `/${item}`,
      null,
      "GET"
    );

    setIdMasterChecklist(item);
    setMasterNameUpdate(response.finalResponse.data[0].master_name);
    setEvidenceUpdate(response.finalResponse.data[0].evidence);
    setApplicationStepUpdate(response.finalResponse.data[0].application_steps);
    setReferenceUpdate(response.finalResponse.data[0].reference);
    setComplianceTypeUpdate(response.finalResponse.data[0].compliance_type);
    setRemarkUpdate(response.finalResponse.data[0].remark);
    setOpen(true);
  };

  const GetAllSubCriteria = async () => {
    const response = await callApi(
      ENDPOINT.GetAllSubCriteria,
      { status: 1 },
      "GET"
    );

    setOptionSubCriteria(response.finalResponse.data);
  };

  const PostMaster = async () => {
    const payload = {
      //criteria_id: criteriaId, ga perlu cukukp sub criteria yg dikirim
      sub_criteria_id: subCriteriaId,
      master_name: masterName,
      evidence: evidence,
      application_steps: applicationStep,
      reference: reference,
      compliance_type: complianceType,
      remark: remark,
      self_assessment: 0,
      status: 1,
      master_weight: portionCheklis,
    };

    const response = await callApi(
      ENDPOINT.GetAllMasterChecklist,
      payload,
      "POST"
    );

    //setCriteriaId(null); ga perlu di null bikin error aja
    setSubCriteriaId(null);
    setMasterName("");
    setEvidence("");
    setApplicationStep("");
    setReference("");
    setRemark("");
    //setComplianceType(""); ga perlu
    setPortionChecklist("");
    GetAllMasterChecklist();
    GetAllSubCriteria();
  };

  const UpdateStatus = async (status, item) => {
    const payload = {
      status: status,
    };

    const response = await callApi(
      ENDPOINT.UpdateMaster + `/${item}`,
      payload,
      "POST"
    );

    GetAllMasterChecklist();
    GetAllSubCriteria();
  };

  const UpdateInclude = async (include, item) => {
    const payload = {
      self_assessment: include,
    };

    const response = await callApi(
      ENDPOINT.UpdateMaster + `/${item}`,
      payload,
      "POST"
    );

    GetAllMasterChecklist();
    GetAllSubCriteria();
  };

  const UpdateMaster = async () => {
    const payload = {
      master_name: masterNameUpdate,
      evidence: evidenceUpdate,
      application_steps: applicationStepUpdate,
      reference: referenceUpdate,
      compliance_type: complianceTypeUpdate,
      remark: remarkUpdate,
    };

    const response = await callApi(
      ENDPOINT.UpdateMaster + `/${idMasterChecklist}`,
      payload,
      "POST"
    );

    setMasterNameUpdate("");
    setEvidenceUpdate("");
    setApplicationStepUpdate("");
    setReferenceUpdate("");
    setRemarkUpdate("");
    setComplianceTypeUpdate("");
    setOpen(false);

    GetAllMasterChecklist();
    GetAllSubCriteria();
  };

  const handleClose = () => {
    setOpen(false);
    setDisable(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChange = (event) => {
    setValueRadio(event.target.value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const sifat = ["Wajib", "Inisiatif"];
  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const handleFilterByKode = (event) => {
    setFilterKode(event.target.value);
  };

  const handleFilterByTolakUkur = (event) => {
    setFilterTolakUkur(event.target.value);
  };
  const handleFilterByLangkahPenerapan = (event) => {
    setFilterLangkahPenerapan(event.target.value);
  };

  const handleFilter = (event, type) => {
    if (type === "KODE") {
      setFilterKode(event.target.value);
    } else if (type === "TU") {
      setFilterTolakUkur(event.target.value);
    } else if (type === "LP") {
      setFilterLangkahPenerapan(event.target.value);
    } else if (type === "BP") {
      setFilterBuktiPendukung(event.target.value);
    } else if (type === "AR") {
      console.log(event.target.value);
      setFilterAcuanReferensi(event.target.value);
    } else if (type === "KET") {
      console.log(event.target.value);
      setFilterKeterangan(event.target.value);
    } else {
      setFilterSifat(event.target.value);
    }
  };
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  // const filteredUsers = applySortFilter(
  //   data,
  //   // getComparator(order, orderBy),
  //   filterKode,
  //   filterLangkahPenerapan,
  //   filterTolakUkur,
  //   filterBuktiPendukung,
  //   filterAcuanReferensi,
  //   filterKeterangan,
  //   filterSifat
  // );
  const filteredUsers = data.filter((post) => {
    if (filterKode === "") {
      //if query is empty
      return post;
    } else if (
      post.code.toLowerCase().includes(filterKode.toLowerCase()) &&
      post.application_steps
        .toLowerCase()
        .includes(filterLangkahPenerapan.toLowerCase()) &&
      post.master_name.toLowerCase().includes(filterTolakUkur.toLowerCase()) &&
      post.evidence
        .toLowerCase()
        .includes(filterBuktiPendukung.toLowerCase()) &&
      String(post.reference)
        .toLowerCase()
        .includes(filterAcuanReferensi.toLowerCase()) &&
      String(post.remark)
        .toLowerCase()
        .includes(filterKeterangan.toLowerCase()) &&
      post.compliance_type.toLowerCase().includes(filterSifat.toLowerCase())
    ) {
      //returns filtered array
      return post;
    }
  });

  const isUserNotFound = filteredUsers.length === 0;
  return (
    <Card>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="md"
        fullWidth
      >
        <DialogTitle
          sx={{ m: 0, p: 2 }}
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Update Kriteria
          {open ? (
            <IconButton
              aria-label="close"
              onClick={handleClose}
              sx={{
                position: "absolute",
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent style={{ marginLeft: 15, marginRight: 15 }} dividers>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <span style={{ fontSize: "14px", fontWeight: 700 }}>
                Langkah Penerapan
              </span>
              <TextareaAutosize
                aria-label="minimum height"
                minRows={5}
                placeholder="Deskripsi langkah penerapan ..."
                style={{
                  width: "100%",
                  padding: 5,
                  borderColor: "silver",
                  borderRadius: 5,
                  resize: "none",
                  fontSize: "14px",
                }}
                value={applicationStepUpdate}
                onChange={(e) => {
                  setApplicationStepUpdate(e.target.value);
                }}
              />
              <span style={{ fontSize: "14px", fontWeight: 700 }}>
                Tolok Ukur
              </span>
              <TextareaAutosize
                aria-label="minimum height"
                minRows={5}
                placeholder="Deskripsi tolok ukur ..."
                style={{
                  width: "100%",
                  padding: 5,
                  borderColor: "silver",
                  borderRadius: 5,
                  resize: "none",
                  fontSize: "14px",
                }}
                value={masterNameUpdate}
                onChange={(e) => {
                  setMasterNameUpdate(e.target.value);
                }}
              />
              <span style={{ fontSize: "14px", fontWeight: 700 }}>
                Bukti Pendukung
              </span>
              <TextareaAutosize
                aria-label="minimum height"
                minRows={5}
                placeholder="Deskripsi bukti pendukung ..."
                style={{
                  width: "100%",
                  padding: 5,
                  borderColor: "silver",
                  borderRadius: 5,
                  resize: "none",
                  fontSize: "14px",
                }}
                value={evidenceUpdate}
                onChange={(e) => {
                  setEvidenceUpdate(e.target.value);
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <span style={{ fontSize: "14px", fontWeight: 700 }}>
                Acuan Referensi
              </span>
              <TextareaAutosize
                aria-label="minimum height"
                minRows={5}
                placeholder="Deskripsi acuan refrensi"
                style={{
                  width: "100%",
                  padding: 5,
                  borderColor: "silver",
                  borderRadius: 5,
                  resize: "none",
                  fontSize: "14px",
                }}
                value={referenceUpdate}
                onChange={(e) => {
                  setReferenceUpdate(e.target.value);
                }}
              />
              <span style={{ fontSize: "14px", fontWeight: 700 }}>
                Keterangan
              </span>
              <TextareaAutosize
                aria-label="minimum height"
                minRows={5}
                placeholder="Keterangan ..."
                style={{
                  width: "100%",
                  padding: 5,
                  borderColor: "silver",
                  borderRadius: 5,
                  resize: "none",
                  fontSize: "14px",
                }}
                value={remarkUpdate}
                onChange={(e) => {
                  setRemarkUpdate(e.target.value);
                }}
              />
              <span style={{ fontSize: "14px", fontWeight: 700 }}>Sifat</span>
              <Autocomplete
                id="controllable-states-demo"
                options={sifat}
                value={complianceTypeUpdate}
                onChange={(e, val) => setComplianceTypeUpdate(val)}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    // label="Aspek"
                    margin="normal"
                    required
                    fullWidth
                    size="small"
                    placeholder="--Pilih Sifat Kriteria--"
                  />
                )}
                style={{ marginTop: "-15px", marginBottom: 10 }}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={UpdateMaster}
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Iconify icon="eva:save-fill" />}
            style={{
              backgroundColor: "#254A7C",
              boxShadow: "none",
              marginRight: 20,
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>

      {/* manajemen kriteria */}
      <Box
        sx={{
          width: "auto",
          maxWidth: "100%",
          margin: 2,
        }}
      >
        <Grid container spacing={3}>
          {/* Bagian Kiri */}
          <Grid item xs={12}>
            <Typography variant="h5" gutterBottom>
              Manajemen Kriteria
            </Typography>
            <hr style={{ marginTop: "0px", marginBottom: "-10px" }} />
          </Grid>
          <Grid item xs={6}>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Sub Kriteria
            </span>
            <Autocomplete
              id="controllable-states-demo"
              options={optionSubCriteria}
              getOptionLabel={(option) =>
                `${option.criteria_code} | ${option.subcriteria_name}`
              }
              onChange={(e, val) => {
                setCriteriaId(val.id);
                setSubCriteriaId(val.id);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  // label="Aspek"
                  margin="normal"
                  required
                  fullWidth
                  placeholder="Select Criteria"
                  size="small"
                />
              )}
              style={{ marginTop: "-15px", marginBottom: 5 }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Langkah Penerapan
            </span>
            <TextareaAutosize
              aria-label="minimum height"
              minRows={5}
              placeholder="Deskripsi langkah penerapan ..."
              style={{
                width: "100%",
                padding: 5,
                borderColor: "silver",
                borderRadius: 5,
                resize: "none",
                fontSize: "14px",
              }}
              value={applicationStep}
              onChange={(e) => {
                setApplicationStep(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Tolok Ukur
            </span>
            <TextareaAutosize
              aria-label="minimum height"
              minRows={5}
              placeholder="Deskripsi tolok ukur ..."
              style={{
                width: "100%",
                padding: 5,
                borderColor: "silver",
                borderRadius: 5,
                resize: "none",
                fontSize: "14px",
              }}
              value={masterName}
              onChange={(e) => {
                setMasterName(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Bukti Pendukung
            </span>
            <TextareaAutosize
              aria-label="minimum height"
              minRows={5}
              placeholder="Deskripsi bukti pendukung ..."
              style={{
                width: "100%",
                padding: 5,
                borderColor: "silver",
                borderRadius: 5,
                resize: "none",
                fontSize: "14px",
              }}
              value={evidence}
              onChange={(e) => {
                setEvidence(e.target.value);
              }}
            />
          </Grid>
          <Grid item xs={6}>
            <span style={{ fontSize: "14px", fontWeight: 700 }}>bobot</span>
            <TextField
              required
              autoFocus
              margin="normal"
              id="name"
              // label="Contract No"
              type="number"
              fullWidth
              variant="outlined"
              placeholder={"Contoh: 0.2"}
              size="small"
              style={{ marginTop: 0, marginBottom: 10 }}
              value={portionCheklis}
              onChange={(e) => {
                setPortionChecklist(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Acuan Referensi
            </span>
            <TextareaAutosize
              aria-label="minimum height"
              minRows={5}
              placeholder="Deskripsi acuan refrensi"
              style={{
                width: "100%",
                padding: 5,
                borderColor: "silver",
                borderRadius: 5,
                resize: "none",
                fontSize: "14px",
              }}
              value={reference}
              onChange={(e) => {
                setReference(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>
              Keterangan
            </span>
            <TextareaAutosize
              aria-label="minimum height"
              minRows={5}
              placeholder="Keterangan ..."
              style={{
                width: "100%",
                padding: 5,
                borderColor: "silver",
                borderRadius: 5,
                resize: "none",
                fontSize: "14px",
              }}
              value={remark}
              onChange={(e) => {
                setRemark(e.target.value);
              }}
            />
            <span style={{ fontSize: "14px", fontWeight: 700 }}>Sifat</span>
            <Autocomplete
              id="controllable-states-demo"
              options={sifat}
              onChange={(e, val) => setComplianceType(val)}
              renderInput={(params) => (
                <TextField
                  {...params}
                  // label="Aspek"
                  margin="normal"
                  required
                  fullWidth
                  size="small"
                  placeholder="--Pilih Sifat Kriteria--"
                />
              )}
              style={{ marginTop: "-15px", marginBottom: 10 }}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="contained"
              style={{
                backgroundColor: "#248bf5",
                boxShadow: "none",
                marginBottom: 10,
                float: "right",
              }}
              onClick={PostMaster}
            >
              Save
            </Button>
          </Grid>
          <Grid item xs={12}>
            {/* <UserListToolbar
              numSelected={selected.length}
              filterName={filterName}
              onFilterName={handleFilterByName}
            /> */}

            <Scrollbar>
              <TableContainer sx={{ minWidth: 800 }}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <StyledTableCell>No.</StyledTableCell>
                      <StyledTableCell style={{ minWidth: "50px" }}>
                        Kode
                      </StyledTableCell>
                      <StyledTableCell style={{ minWidth: "150px" }}>
                        Langkah Penerapan
                      </StyledTableCell>
                      <StyledTableCell style={{ minWidth: "250px" }}>
                        Tolak Ukur
                      </StyledTableCell>
                      <StyledTableCell style={{ minWidth: "200px" }}>
                        Bukti Pendukung
                      </StyledTableCell>
                      <StyledTableCell style={{ minWidth: "350px" }}>
                        Acuan Referensi
                      </StyledTableCell>
                      <StyledTableCell style={{ minWidth: "800px" }}>
                        Keterangan
                      </StyledTableCell>
                      <StyledTableCell style={{ minWidth: "50px" }}>
                        Sifat
                      </StyledTableCell>
                      <StyledTableCell style={{ minWidth: "50px" }}>
                        Bobot
                      </StyledTableCell>
                      <StyledTableCell>Action</StyledTableCell>
                      <StyledTableCell>Self Assessment</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {filteredUsers
                      // .slice(
                      //   page * rowsPerPage,
                      //   page * rowsPerPage + rowsPerPage
                      // )
                      .map((row, index) => {
                        const {
                          code,
                          application_steps,
                          master_name,
                          evidence,
                          reference,
                          remark,
                          compliance_type,
                          master_weight,
                        } = row;

                        const idDoc = data[index].id;
                        return (
                          <TableRow key={index}>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              {index + 1}
                            </TableCell>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              {code}
                            </TableCell>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              {application_steps}
                            </TableCell>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              {master_name}
                            </TableCell>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              {evidence}
                            </TableCell>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              {reference}
                            </TableCell>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              {remark}
                            </TableCell>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              {compliance_type}
                            </TableCell>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              {master_weight}
                            </TableCell>
                            <TableCell
                              style={{
                                textAlign: "left",
                                verticalAlign: "top",
                              }}
                            >
                              <FormControl
                                // sx={{ m: 1, minWidth: 100 }}
                                size="small"
                              >
                                <Select
                                  labelId="demo-simple-select-standard-label"
                                  id="demo-simple-select-standard"
                                  placeholder="-- Select --"
                                  // label="Age"
                                  value={data[index].status}
                                  onChange={(e) => {
                                    if (e.target.value === "Update") {
                                      GetMasterChecklistById(row.id);
                                    } else {
                                      const objIndex = data.findIndex(
                                        (obj) => obj.id === idDoc
                                      );

                                      // make new object of updated object.
                                      const updatedObj = {
                                        ...data[objIndex],
                                        status: e.target.value,
                                      };

                                      // make final new array of objects by combining updated object.
                                      const updatedProjects = [
                                        ...data.slice(0, objIndex),
                                        updatedObj,
                                        ...data.slice(objIndex + 1),
                                      ];

                                      setData(updatedProjects);
                                      UpdateStatus(e.target.value, row.id);
                                    }
                                  }}
                                >
                                  <MenuItem
                                    value={1}
                                    title="Tolak ukur terpenuhi secara lengkap (bukti dokumen/laporan, dokumentasi, legalitas, persyaratan regulasi dll)"
                                  >
                                    Publish
                                  </MenuItem>
                                  <MenuItem
                                    value={0}
                                    title="Tolak ukur terpenuhi sebagian"
                                  >
                                    Not Publish
                                  </MenuItem>
                                  <MenuItem
                                    value={"Update"}
                                    title="Tolak ukur terpenuhi sebagian"
                                  >
                                    Update
                                  </MenuItem>
                                </Select>
                              </FormControl>
                            </TableCell>
                            <TableCell
                              style={{
                                width: "50px",
                                textAlign: "center",
                                verticalAlign: "top",
                              }}
                            >
                              <FormControlLabel
                                control={
                                  <Checkbox
                                    size="small"
                                    checked={
                                      data[index].self_assessment === 1
                                        ? true
                                        : false
                                    }
                                    onChange={() => {
                                      const objIndex = data.findIndex(
                                        (obj) => obj.id === idDoc
                                      );

                                      // make new object of updated object.
                                      const updatedObj = {
                                        ...data[objIndex],
                                        self_assessment:
                                          data[index].self_assessment === 1
                                            ? 0
                                            : 1,
                                      };

                                      // make final new array of objects by combining updated object.
                                      const updatedProjects = [
                                        ...data.slice(0, objIndex),
                                        updatedObj,
                                        ...data.slice(objIndex + 1),
                                      ];

                                      const bool =
                                        data[index].self_assessment === 1
                                          ? 0
                                          : 1;

                                      setData(updatedProjects);
                                      UpdateInclude(bool, row.id);
                                    }}
                                  />
                                }
                                label="Included"
                                style={{ textAlign: "12px" }}
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    {emptyRows > 0 && (
                      <TableRow style={{ height: 53 * emptyRows }}>
                        <TableCell colSpan={11} />
                      </TableRow>
                    )}
                  </TableBody>
                  {isUserNotFound && (
                    <TableBody>
                      <TableRow>
                        <TableCell align="center" colSpan={11} sx={{ py: 3 }}>
                          <SearchNotFound searchQuery={filterName} />
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  )}
                  {/* <TableBody>
                    <TableRow>
                      <TableCell colSpan={1} />
                      <TableCell align="center" colSpan={1} sx={{ py: 3 }}>
                        <SearchColumn
                          numSelected={selected.length}
                          filterName={filterKode}
                          onFilterName={(e) => handleFilter(e, "KODE")}
                          // placeholder={"By kode..."}
                        />
                      </TableCell>
                      <TableCell align="center" colSpan={1} sx={{ py: 3 }}>
                        <SearchColumn
                          numSelected={selected.length}
                          filterName={filterLangkahPenerapan}
                          onFilterName={(e) => handleFilter(e, "LP")}
                          // placeholder={"By langkah penerapan..."}
                        />
                      </TableCell>
                      <TableCell align="center" colSpan={1} sx={{ py: 3 }}>
                        <SearchColumn
                          numSelected={selected.length}
                          filterName={filterTolakUkur}
                          onFilterName={(e) => handleFilter(e, "TU")}
                          // placeholder={"By langkah penerapan..."}
                        />
                      </TableCell>
                      <TableCell align="center" colSpan={1} sx={{ py: 3 }}>
                        <SearchColumn
                          numSelected={selected.length}
                          filterName={filterBuktiPendukung}
                          onFilterName={(e) => handleFilter(e, "BP")}
                          // placeholder={"By langkah penerapan..."}
                        />
                      </TableCell>
                      <TableCell align="center" colSpan={1} sx={{ py: 3 }}>
                        <SearchColumn
                          numSelected={selected.length}
                          filterName={filterAcuanReferensi}
                          onFilterName={(e) => handleFilter(e, "AR")}
                          // placeholder={"By langkah penerapan..."}
                        />
                      </TableCell>
                      <TableCell align="center" colSpan={1} sx={{ py: 3 }}>
                        <SearchColumn
                          numSelected={selected.length}
                          filterName={filterKeterangan}
                          onFilterName={(e) => handleFilter(e, "KET")}
                          // placeholder={"By langkah penerapan..."}
                        />
                      </TableCell>
                      <TableCell align="center" colSpan={1} sx={{ py: 3 }}>
                        <SearchColumn
                          numSelected={selected.length}
                          filterName={filterSifat}
                          onFilterName={(e) => handleFilter(e, "SIFAT")}
                          // placeholder={"By langkah penerapan..."}
                        />
                      </TableCell>
                    </TableRow>
                  </TableBody> */}
                </Table>
              </TableContainer>
              {/* <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={data.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              /> */}
            </Scrollbar>
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
}
